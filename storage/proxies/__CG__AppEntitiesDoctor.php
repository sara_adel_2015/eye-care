<?php

namespace DoctrineProxies\__CG__\App\Entities;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Doctor extends \App\Entities\Doctor implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', 'id', 'doctorName', 'email', 'speciality', 'address', 'city', 'state', 'zip', 'latitude', 'longitude', 'weblink', 'iphoneImage', 'status', 'dateAdded', 'strorder', 'phone', 'api_token', 'password', 'patients', 'eyeTests', 'paymentCards', 'currentPaymentCardId', 'nextRecurringDate', 'rememberToken'];
        }

        return ['__isInitialized__', 'id', 'doctorName', 'email', 'speciality', 'address', 'city', 'state', 'zip', 'latitude', 'longitude', 'weblink', 'iphoneImage', 'status', 'dateAdded', 'strorder', 'phone', 'api_token', 'password', 'patients', 'eyeTests', 'paymentCards', 'currentPaymentCardId', 'nextRecurringDate', 'rememberToken'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Doctor $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function prePersist(\Doctrine\ORM\Event\LifecycleEventArgs $eventArgs)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'prePersist', [$eventArgs]);

        return parent::prePersist($eventArgs);
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setId($id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setId', [$id]);

        return parent::setId($id);
    }

    /**
     * {@inheritDoc}
     */
    public function getDoctorName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDoctorName', []);

        return parent::getDoctorName();
    }

    /**
     * {@inheritDoc}
     */
    public function setDoctorName($doctorName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDoctorName', [$doctorName]);

        return parent::setDoctorName($doctorName);
    }

    /**
     * {@inheritDoc}
     */
    public function getSpeciality()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSpeciality', []);

        return parent::getSpeciality();
    }

    /**
     * {@inheritDoc}
     */
    public function setSpeciality($speciality)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSpeciality', [$speciality]);

        return parent::setSpeciality($speciality);
    }

    /**
     * {@inheritDoc}
     */
    public function getAddress()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAddress', []);

        return parent::getAddress();
    }

    /**
     * {@inheritDoc}
     */
    public function setAddress($address)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAddress', [$address]);

        return parent::setAddress($address);
    }

    /**
     * {@inheritDoc}
     */
    public function getCity()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCity', []);

        return parent::getCity();
    }

    /**
     * {@inheritDoc}
     */
    public function setCity($city)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCity', [$city]);

        return parent::setCity($city);
    }

    /**
     * {@inheritDoc}
     */
    public function getState()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getState', []);

        return parent::getState();
    }

    /**
     * {@inheritDoc}
     */
    public function setState($state)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setState', [$state]);

        return parent::setState($state);
    }

    /**
     * {@inheritDoc}
     */
    public function getZip()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getZip', []);

        return parent::getZip();
    }

    /**
     * {@inheritDoc}
     */
    public function setZip($zip)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setZip', [$zip]);

        return parent::setZip($zip);
    }

    /**
     * {@inheritDoc}
     */
    public function getLatitude()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLatitude', []);

        return parent::getLatitude();
    }

    /**
     * {@inheritDoc}
     */
    public function setLatitude($latitude)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLatitude', [$latitude]);

        return parent::setLatitude($latitude);
    }

    /**
     * {@inheritDoc}
     */
    public function getLongitude()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLongitude', []);

        return parent::getLongitude();
    }

    /**
     * {@inheritDoc}
     */
    public function setLongitude($longitude)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLongitude', [$longitude]);

        return parent::setLongitude($longitude);
    }

    /**
     * {@inheritDoc}
     */
    public function getWeblink()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getWeblink', []);

        return parent::getWeblink();
    }

    /**
     * {@inheritDoc}
     */
    public function setWeblink($weblink)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setWeblink', [$weblink]);

        return parent::setWeblink($weblink);
    }

    /**
     * {@inheritDoc}
     */
    public function getIphoneImage()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIphoneImage', []);

        return parent::getIphoneImage();
    }

    /**
     * {@inheritDoc}
     */
    public function setIphoneImage($iphoneImage)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIphoneImage', [$iphoneImage]);

        return parent::setIphoneImage($iphoneImage);
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStatus', []);

        return parent::getStatus();
    }

    /**
     * {@inheritDoc}
     */
    public function setStatus($status)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStatus', [$status]);

        return parent::setStatus($status);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateAdded()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateAdded', []);

        return parent::getDateAdded();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateAdded($dateAdded)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateAdded', [$dateAdded]);

        return parent::setDateAdded($dateAdded);
    }

    /**
     * {@inheritDoc}
     */
    public function getStrorder()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStrorder', []);

        return parent::getStrorder();
    }

    /**
     * {@inheritDoc}
     */
    public function setStrorder($strorder)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStrorder', [$strorder]);

        return parent::setStrorder($strorder);
    }

    /**
     * {@inheritDoc}
     */
    public function getPhone()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPhone', []);

        return parent::getPhone();
    }

    /**
     * {@inheritDoc}
     */
    public function setPhone($phone)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPhone', [$phone]);

        return parent::setPhone($phone);
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEmail', []);

        return parent::getEmail();
    }

    /**
     * {@inheritDoc}
     */
    public function setEmail($email)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEmail', [$email]);

        return parent::setEmail($email);
    }

    /**
     * {@inheritDoc}
     */
    public function getApiToken()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getApiToken', []);

        return parent::getApiToken();
    }

    /**
     * {@inheritDoc}
     */
    public function setApiToken($api_token)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setApiToken', [$api_token]);

        return parent::setApiToken($api_token);
    }

    /**
     * {@inheritDoc}
     */
    public function getPatients()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPatients', []);

        return parent::getPatients();
    }

    /**
     * {@inheritDoc}
     */
    public function setPatients($patients)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPatients', [$patients]);

        return parent::setPatients($patients);
    }

    /**
     * {@inheritDoc}
     */
    public function getPaymentCards()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPaymentCards', []);

        return parent::getPaymentCards();
    }

    /**
     * {@inheritDoc}
     */
    public function setPaymentCards($paymentCards)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPaymentCards', [$paymentCards]);

        return parent::setPaymentCards($paymentCards);
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrentPaymentCardId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCurrentPaymentCardId', []);

        return parent::getCurrentPaymentCardId();
    }

    /**
     * {@inheritDoc}
     */
    public function setCurrentPaymentCardId($currentPaymentCardId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCurrentPaymentCardId', [$currentPaymentCardId]);

        return parent::setCurrentPaymentCardId($currentPaymentCardId);
    }

    /**
     * {@inheritDoc}
     */
    public function addPaymentCard(\App\Entities\DoctorPaymentCards $paymentCard)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addPaymentCard', [$paymentCard]);

        return parent::addPaymentCard($paymentCard);
    }

    /**
     * {@inheritDoc}
     */
    public function getNextRecurringDate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNextRecurringDate', []);

        return parent::getNextRecurringDate();
    }

    /**
     * {@inheritDoc}
     */
    public function setNextRecurringDate($nextRecurringDate)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNextRecurringDate', [$nextRecurringDate]);

        return parent::setNextRecurringDate($nextRecurringDate);
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPassword', []);

        return parent::getPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function setPassword($password)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPassword', [$password]);

        return parent::setPassword($password);
    }

    /**
     * {@inheritDoc}
     */
    public function getEyeTests()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEyeTests', []);

        return parent::getEyeTests();
    }

    /**
     * {@inheritDoc}
     */
    public function setEyeTests($eyeTests)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEyeTests', [$eyeTests]);

        return parent::setEyeTests($eyeTests);
    }

    /**
     * {@inheritDoc}
     */
    public function sendPasswordResetNotification($token)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'sendPasswordResetNotification', [$token]);

        return parent::sendPasswordResetNotification($token);
    }

    /**
     * {@inheritDoc}
     */
    public function setRememberToken($value)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRememberToken', [$value]);

        return parent::setRememberToken($value);
    }

    /**
     * {@inheritDoc}
     */
    public function getRememberTokenName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRememberTokenName', []);

        return parent::getRememberTokenName();
    }

    /**
     * {@inheritDoc}
     */
    public function getAuthenticationType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAuthenticationType', []);

        return parent::getAuthenticationType();
    }

    /**
     * {@inheritDoc}
     */
    public function getAuthIdentifierName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAuthIdentifierName', []);

        return parent::getAuthIdentifierName();
    }

    /**
     * {@inheritDoc}
     */
    public function getAuthIdentifier()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAuthIdentifier', []);

        return parent::getAuthIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    public function getAuthPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAuthPassword', []);

        return parent::getAuthPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function getRememberToken()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRememberToken', []);

        return parent::getRememberToken();
    }

    /**
     * {@inheritDoc}
     */
    public function getEmailForPasswordReset()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEmailForPasswordReset', []);

        return parent::getEmailForPasswordReset();
    }

    /**
     * {@inheritDoc}
     */
    public function notifications()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'notifications', []);

        return parent::notifications();
    }

    /**
     * {@inheritDoc}
     */
    public function readNotifications()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'readNotifications', []);

        return parent::readNotifications();
    }

    /**
     * {@inheritDoc}
     */
    public function unreadNotifications()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'unreadNotifications', []);

        return parent::unreadNotifications();
    }

    /**
     * {@inheritDoc}
     */
    public function notify($instance)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'notify', [$instance]);

        return parent::notify($instance);
    }

    /**
     * {@inheritDoc}
     */
    public function notifyNow($instance, array $channels = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'notifyNow', [$instance, $channels]);

        return parent::notifyNow($instance, $channels);
    }

    /**
     * {@inheritDoc}
     */
    public function routeNotificationFor($driver, $notification = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'routeNotificationFor', [$driver, $notification]);

        return parent::routeNotificationFor($driver, $notification);
    }

}
