<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbleyetestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbleyetest')) {
            Schema::create('tbleyetest', function (Blueprint $table) {
                $table->increments('intId')->comment('primary id');
                $table->integer('patientid')->comment('patient_id');
                $table->string('imagename', 255)->comment('image_name');
                $table->string('imagetype')->comment('image_type');
                $table->string('devicetype')->comment('device_type');
                $table->string('comparepattern')->comment('compare_pattern')->nullable();
                $table->enum('status', ['Active', 'Inactive']);
                $table->enum('sentmail', ['0', '1'])->comment('sent_mail')->default(0);
                $table->timestamp('dateadded')->comment('date_added')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbleyetest');
    }
}
