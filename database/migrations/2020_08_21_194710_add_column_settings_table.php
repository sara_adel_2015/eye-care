<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblsettings', function (Blueprint $table) {
            $table->decimal('doctor_charge_recurring_amount')
                ->default(10.00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblsettings', function (Blueprint $table) {
            $table->dropColumn(['doctor_charge_recurring_amount']);
        });
    }
}
