<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use LaravelDoctrine\ORM\Facades\EntityManager;
use App\Entities\Patient;
use Illuminate\Support\Str;

class AddApiTokenColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('tblpatientlist', function (Blueprint $table) {
            $table->string('api_token', 80)->after('password')
                ->unique()
                ->nullable()
                ->default(null);
        });
        $repo = EntityManager::getRepository(Patient::class);
        $users = $repo->findAll();
        foreach ($users as $user) {
            $apiToken = Str::random(60);
            $user->setApiToken($apiToken);
        }
        EntityManager::flush();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblpatientlist', function (Blueprint $table) {
            $table->dropColumn(['api_token']);
        });
    }
}
