<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDoctorColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbldoctorlist', function (Blueprint $table) {
            $table->string('email')
                ->unique()
                ->nullable();
            $table->string('password', 80)
                ->nullable()
                ->default("password@123");
            $table->string('api_token', 80)
                ->unique()
                ->nullable()
                ->default(null);
        });
        $repo = EntityManager::getRepository(\App\Entities\Doctor::class);
        $users = $repo->findAll();
        foreach ($users as $user) {
            $apiToken = Str::random(60);
            $user->setApiToken($apiToken);
        }
        EntityManager::flush();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldoctorlist', function (Blueprint $table) {
            $table->dropColumn(['api_token', 'password', 'email']);
        });
    }
}
