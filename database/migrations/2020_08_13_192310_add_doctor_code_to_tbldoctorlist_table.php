<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDoctorCodeToTbldoctorlistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $generateCodeLength = env('DOCTOR_GENERATE_CODE_LENGTH');
        Schema::table('tbldoctorlist', function (Blueprint $table) use ($generateCodeLength) {
            $table->string('doctor_code', $generateCodeLength)
                ->unique()->nullable()->default(NULL);

        });
        $repo = EntityManager::getRepository(\App\Entities\Doctor::class);
        $doctors = $repo->findAll();
        foreach ($doctors as $doctor) {
            /**
             * @var $doctor \App\Entities\Doctor
             */
            $doctorCode = Str::random($generateCodeLength);
            $doctor->setDoctorCode($doctorCode);
        }
        EntityManager::flush();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldoctorlist', function (Blueprint $table) {
            $table->dropColumn('doctor_code');
        });
    }
}
