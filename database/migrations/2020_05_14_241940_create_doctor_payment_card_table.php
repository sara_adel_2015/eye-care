<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorPaymentCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tbl_doctor_payment_cards');
        Schema::create('tbl_doctor_payment_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_reference',135);
            $table->string('customer_reference',135);
            $table->string('last_four_digits');
            $table->unsignedInteger('doctor_id');
            $table->foreign('doctor_id','fk_payment_doctor_id')->references('intId')->on('tbldoctorlist');
            $table->timestamps();
        });
        Schema::table('tbldoctorlist', function (Blueprint $table) {
            $table->integer('current_payment_card_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_doctor_payment_cards');
        Schema::table('tbldoctorlist', function (Blueprint $table) {
            $table->dropColumn(['current_payment_card_id']);
        });
    }
}
