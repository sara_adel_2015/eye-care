<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSettingsColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblsettings', function (Blueprint $table) {
            $table->decimal('recurring_charge_amount')
                ->default(10.00);
            $table->integer('recurring_interval_every')
                ->default(1);
            $table->string('recurring_interval_each')
                ->default('month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldoctorlist', function (Blueprint $table) {
            $table->dropColumn(['recurring_charge_amount', 'recurring_interval_every', 'recurring_interval_each']);
        });
    }
}
