<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tblsettings')) {
            Schema::create('tblsettings', function (Blueprint $table) {
                $table->integer('ALLOWED_COMPARE_RATIO')->comment('allowed_compare_ratio');
                $table->integer('ALLOWED_SIMILITRY_PERCENTAGE')->comment('allowed_similarity_percentage');
                $table->text('DIFFERENT_RESULT_RECIEPTION')->comment('different_result_reception');
                $table->text('DIFFERENT_RESULT_RECIEPTION_EMAIL')->nullable()->comment('different_result_reception_email');
                $table->text('NOTIFICATION_EMAIL_NOT_MATCHED')->nullable()->comment('notification_email_not_matched');
                $table->text('NOTIFICATION_EMAIL_MATCHED')->nullable()->comment('notification_email_matched');
                $table->increments('ID')->comment('id_primary_key');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsettings');
    }
}
