<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubscriptionIdColumnToDoctor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbldoctorlist', function (Blueprint $table) {
            $table->string('subscription_id', 135)->nullable();
            $table->string('customer_reference', 135)->nullable();
            $table->boolean('is_request_to_cancel_subscription')->nullable()->default(0);
            $table->boolean('is_subscription_cancelled')->nullable()->default(0);
            $table->dateTime('next_recurring_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldoctorlist', function (Blueprint $table) {
            $table->dropColumn('subscription_id');
            $table->dropColumn('is_request_to_cancel_subscription');
            $table->dropColumn('is_subscription_cancelled');
            $table->dropColumn(['next_recurring_date']);

        });
    }
}