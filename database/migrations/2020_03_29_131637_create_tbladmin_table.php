<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;
use LaravelDoctrine\ORM\Facades\EntityManager;

class CreateTbladminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbladmin')) {
            Schema::create('tbladmin', function (Blueprint $table) {
                $table->increments('intid');
                $table->string('username', 64)->comment('user_name');
                $table->string('password', 64)->comment('password');
                $table->string('varfname', 64)->comment('var_fname');
                $table->string('varlname', 64)->comment('var_lname');
                $table->string('vaemail', 64)->comment('va_email');
                $table->integer('intstatus')->comment('int_status')->default(0);
                $table->date('last_visited')->comment('last_visited');
                $table->string('remember_token', 255)->comment('remember_token');

            });
        }
        $adminEntity = new \App\Entities\Admin();
        $adminEntity->setEmail('admin@admin.com');
        $adminEntity->setFirstName('admin');
        $adminEntity->setLastName('admin');
        $adminEntity->setPassword(Hash::make('Chicago2019'));
        $adminEntity->setStatus(1);
        $adminEntity->setUserName('admin');
        $adminEntity->setLastVisited(new \DateTime());
        $adminEntity->setRememberToken('');
        EntityManager::persist($adminEntity);
        EntityManager::flush();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbladmin');
    }
}
