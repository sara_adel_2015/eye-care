<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblpatientlistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tblpatientlist')) {
            Schema::create('tblpatientlist', function (Blueprint $table) {
                $table->increments('intId');
                $table->string('firstname')->comment('first_name');
                $table->string('lastname')->comment('last_name');
                $table->text('email')->comment('last_name');
                $table->string('password')->comment('password');
                $table->text('address')->nullable()->comment('address');
                $table->string('city', 50)->nullable()->comment('city');
                $table->string('state', 50)->nullable()->comment('state');
                $table->string('zip', 20)->comment('zip');
                $table->string('phone', 20)->comment('phone');
                $table->enum('status', ['Active', 'Inactive']);
                $table->timestamp('dateadded')->comment('date_added')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
                $table->integer('strorder')->comment('str_order');
                $table->string('remember_token', 255)->comment('remember token option');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpatientlist');
    }
}
