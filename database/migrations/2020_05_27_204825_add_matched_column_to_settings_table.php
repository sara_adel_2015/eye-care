<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMatchedColumnToSettingsTable extends Migration
{
    public function up()
    {
        Schema::table('tblsettings', function (Blueprint $table) {
            $table->string('MATCHED_RESULT_RECIEPTION')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblsettings', function (Blueprint $table) {
            $table->dropColumn(['MATCHED_RESULT_RECIEPTION']);
        });
    }
}
