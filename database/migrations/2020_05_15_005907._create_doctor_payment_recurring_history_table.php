<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorPaymentRecurringHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('doctor_payment_recurring_history');
        Schema::create('doctor_payment_recurring_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('doctor_id');
            $table->integer('payment_card_id');
            $table->string('customer_reference');
            $table->string('card_reference');
            $table->decimal('amount');
            $table->boolean('is_success');
            $table->string('charge_id');
            $table->string('invoice_id');
            $table->string('invoice_pdf_url');
            $table->string('event_id');
            $table->text('message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_payment_recurring_history');
    }
}
