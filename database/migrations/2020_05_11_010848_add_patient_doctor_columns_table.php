<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPatientDoctorColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblpatientlist', function (Blueprint $table) {
            $table->unsignedInteger('doctor_id')->nullable();
            $table->foreign('doctor_id', 'fk_doctor_id')->references('intId')->on('tbldoctorlist');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbldoctorlist', function (Blueprint $table) {
            $table->dropColumn(['doctor_id']);
        });
    }
}
