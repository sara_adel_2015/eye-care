<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDictionaryDoctorListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_dictionary_doctor_list')) {
            Schema::create('tbl_dictionary_doctor_list', function (Blueprint $table) {
                $table->increments('intId');
                $table->string('doctorname')->comment('doctor_name');
                $table->text('speciality')->comment('doctor_speciality');
                $table->text('address', 10)->comment('doctor_address');
                $table->string('city', 50)->comment('doctor_city');
                $table->string('state', 50)->comment('doctor_state');
                $table->string('zip', 20)->comment('doctor_zip');
                $table->double('latitude', 20)->comment('latitude');
                $table->double('longitude', 20)->comment('longitude');
                $table->string('weblink')->comment('web_link')->nullable()->default(null);
                $table->string('iphoneimage')->comment('iphone_image');
                $table->enum('status', ['Active', 'Inactive']);
                $table->timestamp('dateadded')->comment('date_added')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
                $table->integer('strorder')->comment('str_order');
                $table->text('phone')->comment('phone')->nullable()->default(null);;

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_dictionary_doctor_list');
    }
}
