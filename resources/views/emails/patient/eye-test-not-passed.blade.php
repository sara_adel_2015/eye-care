<table width="100%" border="0" cellpadding="5" cellspacing="5">
    <tr>
        <td style="font-family: Arial,Helvetica,sans-serif;font-size:12px;font-weight:bold;">
            Dear {{$patient->getFirstName()}}  {{$patient->getLastName()}},
        </td>
    </tr>
    <tr>
        <td style="font-family: Arial,Helvetica,sans-serif;font-size:12px;">

            We found that your last {{$imageTypeName}} eye Amsler grid test differs significantly from your previous
            test.<br/><br/>

            If you feel as if this message was received in error or the test was performed incorrectly, please consider
            repeating the test on the Eyecare Application on your mobile device and compare to the images provided in
            this e-mail. If you believe this test to be accurate or you have any concerns about changes in your vision
            please visit your nearest eye care specialist. Thank you for using the Eyecare Mobile Amsler Grid System.
            <br/><br/>
            If you need assistance locating an ophthalmologist near you, please contact us through our webpage at
            http://amslerapp.com/#contact<br/><br/>Sincerely,<br/><br/>

            Eyecare Amsler Grid Monitoring System<br/>
            amslerapp.com<br/><br/>

            Please do not respond to this e-mail as it is an automatic response and the inbox is not checked for
            responses. Do not anticipate a response if an e-mail is sent/replied to this e-mail address.<br/><br/>
            <br/>
            <br/>
            Thanks.
        </td>
    </tr>
</table>