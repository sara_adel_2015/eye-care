<table width="100%" border="0" cellpadding="5" cellspacing="5">
    <tr>
        <td style="font-family: Arial,Helvetica,sans-serif;font-size:12px;font-weight:bold;">Hello Admin,</td>
    </tr>
    <tr>
        <td style="font-family: Arial,Helvetica,sans-serif;font-size:12px;">Name : {{$fullName}}</td>
    </tr>
    <tr>
        <td style="font-family: Arial,Helvetica,sans-serif;font-size:12px;">Email : {{$email}}</td>
    </tr>
    @if($emailSubject)
        <tr>
            <td style="font-family: Arial,Helvetica,sans-serif;font-size:12px;">Subject : {{$emailSubject}}</td>
        </tr>
    @endif
    <tr>
        <td style="font-family: Arial,Helvetica,sans-serif;font-size:12px;">Comment : {{$comment}}</td>
    </tr>

    <tr>
        <td style="font-family: Arial,Helvetica,sans-serif;font-size:12px;">
            <br/>Thanks.
        </td>
    </tr>
</table>