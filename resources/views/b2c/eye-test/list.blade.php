@extends('b2c/b2c-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {{--<form name="frmCmsPageList" id="frmCmsPageList" method="post" action="doctorlist.php">--}}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td class='loginhead'>Eye Test List</td>
            </tr>
            <tr>
                <td align="right" style="padding-right:20px;"><a href="{{URL::route('eye-test-add')}}"
                                                                 class="links"><img
                                width="20" height="20" src="{{asset('admin/images/add_small.png')}}" border="0"
                                alt="Add New"
                                title="Add New"/></a>
                </td>
            </tr>
            <tr>
                <td align="left" class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}</td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="table-border" border="0" cellspacing="2">
                        <tr>
                            <td align="center" class="list-title" width="5%">Sr.No</td>
                            <td align="center" class="list-title" width="15%">Image</td>
                            <td align="center" class="list-title" width="10%">Type</td>
                            <td align="center" class="list-title" width="10%">Device</td>
                            <td align="center" class="list-title" width="20%">Patient Name</td>
                            <td align="center" class="list-title" width="25%">Compare Pattern</td>
                            <td align="center" class="list-title" width="10%">Date</td>
                            <td class="list-title" colspan="2" width="10%" align="center">Action</td>
                        </tr>
                        <?php $index = 0;?>
                        @foreach ($list as $eyeTest)
                            <?php $index++;?>
                            <tr class="Hrnormal" onmouseover="this.className='Hrhover';"
                                onmouseout="this.className='Hrnormal';">
                                <td align="center" class="list-contents">{{$eyeTest[0]->getId()}}</td>
                                <td align="center" class="list-contents">
                                    @if(empty($eyeTest[0]->getImageName()) or ($eyeTest[0]->getImageName() == " "))
                                        <a href="{{asset('admin/uploads/eyecareImages/noImage.gif') }}" target="_blank"><img
                                                    width="80" height="80"
                                                    src="{{asset('admin/uploads/eyecareImages/noImage.gif') }}"/>
                                        </a>
                                    @else
                                        <a href="{{asset('admin/uploads/eyecareImages/'.$eyeTest[0]->getImageName()) }}"
                                           target="_blank"><img width="80" height="80"
                                                                src="{{asset('admin/uploads/eyecareImages/'.$eyeTest[0]->getImageName()) }}"/>
                                        </a>
                                    @endif

                                </td>
                                <td align="center" class="list-contents">{{ucfirst($eyeTest[0]->getImageType())}}</td>
                                <td align="center" class="list-contents">{{ucfirst($eyeTest[0]->getDeviceType())}}</td>
                                <td align="center" class="list-contents">{{ucfirst($eyeTest[1])}}</td>
                                <td align="center" class="list-contents"
                                    style="word-break:break-all">{{$eyeTest[0]->getComparePattern()}}</td>
                                <td align="center"
                                    class="list-contents">{{$eyeTest[0]->getDateAdded()->format('yy-m-d h:i:s')}}</td>
                                <td width="4%" align="center" class="list-contents">
                                    <a href="{{ URL::route('eye-test-download', $eyeTest[0]->getId()) }}"><img
                                                src="{{asset('admin/images/download_icon.gif')}}" alt="Download"
                                                title="Download"
                                                width="18" height="18" border="0"/></a> &nbsp;&nbsp;

                                    <a href="javascript:void(0)" onclick="deleteEyeTest({{$eyeTest[0]->getId()}});"
                                       class="links">
                                        <img src="{{asset('admin/images/delete.gif')}}"
                                             alt="Delete"
                                             title="Delete"
                                             width="16" height="16"
                                             border="0"/>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                {{--**Pagination here**--}}
                {{ $list->appends(request()->input())->links('b2c/partial/paginator') }}

            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        {{--</form>--}}
    </td>
@endsection
@push('scripts')
    <script>
        csrf_token = "{{ csrf_token() }}";
        pageForm = "frmPageDetails";
        const BASE_URL = '/patient/eye-test';

        function deleteEyeTest(id) {
            if (confirm("Are you sure want to delete this record?")) {
                $.ajax({
                    method: "DELETE",
                    url: BASE_URL + '/' + id,
                    data: {"_token": csrf_token,}
                }).done(function (response) {
                    window.location.href = BASE_URL;
                });
            }
        }
    </script>
@endpush
