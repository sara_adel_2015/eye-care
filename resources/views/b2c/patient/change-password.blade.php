@extends('b2c/b2c-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {!!  Form::open(array('route' => 'patient-change-password','id'=>'frmPageDetails','method'=>'PUT')) !!}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td valign="top" class="">
                    <table width="100%" border="0" cellpadding="4" cellspacing="0">
                        <tr>
                            <td colspan="2">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class='loginhead'>Change Password</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="redrequirdbig" colspan="2" align="right">* Indicates required field</td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>

                        <tr>
                            <td align="left"
                                class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}
                            </td>
                        </tr>


                        <tr>
                            <td class="fontcaption" width="20%">Current Password: <span class="redrequird">*</span></td>
                        </tr>
                        <tr>
                            <td width="80%"><label>
                                    <input type="password" name="current-password" id="current-password"
                                           class="validate[required,length[5,20]]"
                                           size="50" maxlength="100" autocomplete="on"/>
                                </label></td>
                        </tr>
                        <tr>
                            <td class="fontcaption" width="20%">New Password: <span class="redrequird">*</span></td>
                        </tr>
                        <tr>
                            <td width="80%"><label>
                                    <input type="password" name="new-password" id="new-password"
                                           class="validate[required,length[5,20]]"
                                           size="50" maxlength="100" autocomplete="on"/>
                                </label></td>
                        </tr>
                        <tr>
                            {{--<td class="fontcaption" width="20%">Confirm Password: <span class="redrequird">*</span></td>--}}
                        </tr>
                        {{--<tr>--}}
                        {{--<td width="80%"><label>--}}
                        {{--<input type="password" name="confirm-password" id="confirmPwd"--}}
                        {{--class="validate[required,length[5,20]]"--}}
                        {{--size="50" maxlength="100" value=""/>--}}
                        {{--</label></td>--}}
                        {{--<td width="80%"><label>--}}
                        {{--<input type="password" name="new-password_confirmation" id="new-password_confirmation"--}}
                        {{--class=""--}}
                        {{--size="50" maxlength="100" value=""/>--}}
                        {{--</label></td>--}}
                        {{--</tr>--}}
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td valign="top"><img src="{{asset("b2c/images/button-left.png")}}"
                                                                          alt="" width="14"
                                                                          height="33"/></td>
                                                    <td><input type="submit" name="Submit" value="Submit"
                                                               class="buttonmidle"
                                                               onclick="beforeSubmit();"/></td>
                                                    <td align="right" valign="top"><img
                                                                src="{{asset("b2c/images/button-right.png")}}" alt=""
                                                                width="15" height="33"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td valign="top"><img src="{{asset("b2c/images/button-left.png")}}"
                                                                          alt="" width="14"
                                                                          height="33"/></td>
                                                    <td><input type="button" name="reset" value="Reset"
                                                               class="buttonmidle"
                                                               onclick="window.location.href='{{URL::route("patient-change-password-form")}}'"/>
                                                    </td>
                                                    <td align="right" valign="top"><img
                                                                src="{{asset('b2c/images/button-right.png')}}" alt=""
                                                                width="15" height="33"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
            </tr>
        </table>
        {!! Form::close() !!}
    </td>
@endsection
@prepend('scripts')
    <script>
        pageForm = "frmPageDetails";

        function beforeSubmit() {
            $.validationEngine.beforeSubmit(pageForm);
        }
    </script>
@endprepend