@extends('b2c/b2c-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {!!  Form::open(array('route' => 'patient-update-profile','id'=>'frmPageDetails','method'=>'PUT')) !!}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class='loginhead'>Edit Patient Detail</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="redrequirdbig" colspan="2" align="right">* Indicates required field</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td align="left"
                    class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}</td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">First Name: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="firstName" id="firstName" class="validate[required]"
                               value="{{$user->getFirstName()}}" size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Last Name: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="lastName" id="lastName" class="validate[required]"
                               value="{{$user->getLastName()}}" size="50"/>
                    </label></td>
            </tr>

            <tr>
                <td class="fontcaption" width="20%">Email: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="email" id="email" class="validate[required]"
                               value="{{$user->getEmail()}}" size="50" disabled=""/>
                    </label></td>
            </tr>

            <tr>
                <td class="fontcaption" width="20%">Address:</td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <textarea name="address" id="address" cols="45" rows="3"
                        >{{$user->getAddress()}}</textarea>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">City:</td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="city" id="city" value="{{$user->getCity()}}"
                               size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">State:</td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="state" id="state" value="{{$user->getState()}}"
                               size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Zipcode: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="zip" id="zip" value="{{$user->getZip()}}"
                               class="validate[required]"
                               size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Phone: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="phone" id="phone" value="{{$user->getPhone()}}"
                               class="validate[required]" size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Select Doctor: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                {{--<td width="80%"><label>--}}
                {{--<input type="hidden" id="defaultPhysicianId" name="defaultPhysicianId"--}}
                {{--value="@if($user->getDoctor()){{$user->getDoctor()->getDoctorName()}} @endif">--}}
                {{--<input type="text" style="width: 330px;" id="defaultDoctor"--}}
                {{--value="@if($user->getDoctor()){{$user->getDoctor()->getDoctorName()}} @endif"--}}
                {{--placeholder="Search For Doctors By Name OR Email" class="validate[required]">--}}
                {{--<i class="fa fa-search" style="margin-left: -17px"></i>--}}
                {{--</label></td>--}}
                <td width="80%"><label>
                        <select id="defaultPhysicianId" name="doctorId" style="width:250px;">
                            @foreach ($doctors as $doctor)
                                @if($user->getDoctor() && ($doctor->getId() == $user->getDoctor()->getId()))
                                    <option value="{{$doctor->getId()}}" selected>
                                @else
                                    <option value="{{$doctor->getId()}}">
                                        @endif
                                        {{$doctor->getDoctorName()}} - ({{$doctor->getDoctorCode()}})
                                    </option>
                                    @endforeach
                        </select>
                    </label></td>
            </tr>
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('b2c/images/button-left.png')}}" alt=""
                                                              width="14"
                                                              height="33"/></td>
                                        <td><input type="submit" name="Submit" value="Submit" class="buttonmidle"
                                                   onclick="beforeSubmit();"/></td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('b2c/images/button-right.png')}}" alt=""
                                                    width="15" height="33"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        {!! Form::close() !!}
    </td>
@endsection
@prepend('scripts')
    <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery-ui.min.css')}}">
@endprepend
@push('scripts')
    <script>
        pageForm = "frmPageDetails";
        $(function () {
            $("#defaultDoctor").autocomplete({
                source: "/admin/doctor/autocomplete",
                select: function (event, ui) {
                    $("#defaultDoctor").val(ui.item.value),
                        $("#defaultPhysicianId").val(ui.item.id)
                },
                classes: {
                    "ui-autocomplete": "highlight"
                }
            });
        });
    </script>
@endpush