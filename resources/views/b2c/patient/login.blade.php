@include('b2c/partial/head')
<html>
<body style="padding-top:75px;">
{!!  Form::open(array('route' => 'patient-login','id'=>'frmPageDetails')) !!}
<table border="0" align="center">
    <tr>
        <td valign="top" class="">
            <table width="542" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-top:10px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="10" valign="top"><img
                                                        src="{{asset('b2c/images/data-area-1-1.png')}}" alt=""
                                                        width="10" height="10"/></td>
                                            <td valign="top" class="boxtopmid"><img
                                                        src="{{asset('b2c/images/data-area-1-2.png')}}"
                                                        alt="" width="1" height="10"/>
                                            </td>
                                            <td width="10" align="right" valign="top"><img
                                                        src="{{asset('b2c/images/data-area-1-3.png')}}" alt=""
                                                        width="10"
                                                        height="10"/></td>
                                        </tr>
                                        <tr>
                                            <td class="boxvarleft">&nbsp;</td>
                                            <td valign="top" class="formboxdatabg">
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td><img src="{{asset('b2c/images/logo.png')}}" alt=""
                                                                 border="0"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="loginhead"><i class="fa fa-eye"></i> Patient Login</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center"
                                                            class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="background-color:#eeeeee; padding-left:10px; padding-top:10px; padding-bottom:10px;">
                                                            <table width="100%" border="0" cellpadding="3"
                                                                   cellspacing="4">
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="22%" class="logincaption">Email
                                                                        : <span class="redrequird">*</span></td>
                                                                    <td width="7%" class="logindots">&nbsp;</td>
                                                                    <td width="71%"><label></label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" class="logincaption"><input
                                                                                name="email"
                                                                                value="{{old('email')}}"
                                                                                type="text" size="50" id="email"
                                                                                class="validate[required]"/>
                                                                        <span id="spnUsername"
                                                                              style="display:none"
                                                                              class="error_msg"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="logincaption">Password
                                                                        : <span class="redrequird">*</span></td>
                                                                    <td class="logindots">&nbsp;</td>
                                                                    <td><label></label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3"><span class="logincaption">
                                      <input name="password" value="" class="validate[required]"
                                             type="password" size="50" id="pass"/>
                                      <span id="spnPassword" style="display:none" class="error_msg"></span> </span></td>
                                                                </tr>
                                                                <tr colspan="3">
                                                                    <td colspan="2">
                                                                        <a href="{{URL::route('patient-reset-password-form')}}">Forgot Password?</a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" style="padding-top:15px;">
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td><input type="image"
                                                                                           name="Submit"
                                                                                           id="Submit"
                                                                                           src="{{asset("b2c/images/submit.png")}}"
                                                                                           width="88"
                                                                                           height="36"/>
                                                                                </td>
                                                                                <td>&nbsp;</td>
                                                                                <td>
                                                                                    <img src="{{asset("b2c/images/cancel.png")}}"
                                                                                         width="91" height="36"
                                                                                         onClick="window.location='/'"
                                                                                         style="cursor:pointer"
                                                                                         title="Back To Home"
                                                                                         border="0"/></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="boxvarright">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><img
                                                        src="{{asset("b2c/images/data-area-3-1.png")}}"
                                                        alt="" width="10"
                                                        height="10"/></td>
                                            <td valign="top" class="boxbottommid"><img
                                                        src="{{asset("b2c/images/data-area-3-2.png")}}" alt=""
                                                        width="1"
                                                        height="10"/></td>
                                            <td align="right"><img
                                                        src="{{asset("b2c/images/data-area-3-3.png")}}"
                                                        alt="" width="10"
                                                        height="10"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="passwordtxt">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    </td>
                </tr>
            </table>
    </tr>
</table>
{!! Form::close() !!}

</body>
</html>
@prepend('scripts')
    <script>
        pageForm = "frmPageDetails";
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endprepend