<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>eyecare- Amsler Grid Eye Test</title>

    <!-- Bootstrap -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('b2c/fonts/style_font.css')}}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Add jQuery library -->
    <script type="text/javascript" src="{{asset('b2c/fancyBox/lib/jquery-1.10.1.min.js')}}"></script>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="{{asset('b2c/fancyBox/lib/jquery.mousewheel-3.0.6.pack.js')}}"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="{{asset('b2c/fancyBox/source/jquery.fancybox.js?v=2.1.5')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('b2c/fancyBox/source/jquery.fancybox.css?v=2.1.5')}}"
          media="screen"/>

    <script type="text/javascript">
        function Show_Div(str) {
            if (str == "show") {
                document.getElementById('div_name2').style.display = '';
                document.getElementById('showSpan').style.display = 'none';
                document.getElementById('hideSpan').style.display = '';
            }
            else {
                document.getElementById('div_name2').style.display = 'none';
                document.getElementById('showSpan').style.display = '';
                document.getElementById('hideSpan').style.display = 'none';
            }
        }

        function goLoginPage() {
            location.href = "/patient/login";
        }

        function goLoginDoctorPage() {
            location.href = "/doctor/login";
        }
    </script>
</head>
<body>
<div class="container-fluid topline topbgcolor">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="#"><img src="{{url('b2c/images/logo.png')}}" class="logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right nv-rght rdc-padding">
                        <li><a href="#">About</a></li>
                        <li><a href="#Gallery-1">Gallery </a></li>
                        <li><a href="#dwn-app">Download App </a></li>
                        <li><a href="{{route('home-doctor-list')}}" class="fancybox fancybox.iframe">Doctors</a></li>
                        <li><a href="#team-1">Team </a></li>
                        <li><a href="#contact">Contact Us</a></li>
                        <li>
                            <button type="button" class="btn btn-success lg-btn-2 " onclick="goLoginPage();">Login
                            </button>
                            <button type="button" id="doctor-login" class="btn btn-success lg-btn-2 "
                                    onclick="goLoginDoctorPage();">
                                Doctor Login
                            </button>
                        </li>
                        {{--<li>--}}
                        {{--<button type="button" id="doctor-login" class="btn btn-warning lg-btn-1 "--}}
                        {{--onclick="goLoginDoctorPage();">--}}
                        {{--Register--}}
                        {{--</button>--}}
                        {{--</li>--}}
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</div>
<!--Banner-Part-Start-->
<div class="container-fluid banner">
    <div class="container">
        <img src="{{url('b2c/images/banner.png')}}" class="img-responsive ">
    </div>
</div>
<!--Banner-Part-End-->
<!--About-Part-Start-->
<div class="container-fluid  about_area ">
    <div class="container">
        <div class="col-md-12 col-sm-12 rdc-padding">
            <div class="col-md-8 rdc-padding" align="center">
                <h2 class="about_heading">About the <strong class="headingcolor">eyecare- Amsler Grid Eye Test</strong>
                </h2>
                <p style="text-align:justify">This application is designed for personal monitoring for vision loss from
                    macular degeneration. With daily use of the Eyecare Amsler Grid, small changes in visual function
                    can be identified electronically that may otherwise have gone unnoticed and caused long lasting
                    damage. The Eyecare Amsler Grid uses proprietary technology to identify any changes in your personal
                    Amsler gird and personally notify you of that change almost immediately.
                    <span id="div_name2" style=" display: none;">With this information you can get the medical care you need and ideally prevent vision loss with the help of your eye care professional. If you don't have an eye care professional to help take care of your vision needs, this application will.
                            </span>
                    <!--<div class=" col-md-12 morelink"><a href="#" class="pull-right">More...</a></div>-->
                    <span id="showSpan"><a onClick="Show_Div('show')" class=" morelink" href="javascript:;"
                                           style="text-decoration:none;">Show More...</a></span>
                    <span id="hideSpan" style=" display: none;"><a onClick="Show_Div('hide')" class=" morelink"
                                                                   href="javascript:;" style="text-decoration:none;">Hide This...</a></span>
                </p>
                <br>


                <div class=" col-md-12 col-sm-12 rdc-padding"><a class="btn btn-success button1" href="#contact">Eyecare-
                        Amsler Grid Eye Test Support</a></div>
            </div>
            <div class="col-md-4 mrg-top" align="center"><img src="{{url('b2c/images/doctor.png')}}"
                                                              class="img-responsive img-circle crc-img"></div>
        </div>
        <div class="col-md-12 col-sm-12 rdc-padding" id="videoSection">
            <div id="videoSectionPatient">
                <div class="video-title rdc-padding">
                    Watch this short video to see how Eyecare can help you take care of your eyes
                </div>
                <div class="video-content">
                    {{--<iframe src="https://drive.google.com/file/d/12hJEzNLyEN68yC9QCjUJeMkKP1fLnHJh/preview" width="640" height="480"></iframe>--}}
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/ZAswpbslblY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="video-note">
                    Eyecare Specialists: Click <a href="/specialist-info" target="_blank"> Here</a> To Learn More
                </div>
        </div>
    </div>

</div>
<!--About-Part-End-->
<!--Gallery-Part-Start-->
<div class="container-fluid gallery_area rdc-padding" id="Gallery-1">

    <div class="container">
        <div class="col-md-12">

            <h2>Gallery</h2>
            <p>View here actual screenshots from the application. These images depict how you can keep track of any
                changes that may be occurring with your vision. Take control of your eye sight with the Eyecare
                interactive Amsler grid.</p>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>

                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="{{url('b2c/images/banner_1.png')}}">
                    </div>

                    <div class="item">
                        <img src="{{url('b2c/images/banner_2.png')}}">
                    </div>

                    <div class="item">
                        <img src="{{url('b2c/images/banner_1.png')}}">
                    </div>

                    <div class="item">
                        <img src="{{url('b2c/images/banner_2.png')}}">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
    </div>

</div>
<!--Gallery-Part-End-->
<!--appplay-Part-Start-->
<div class="container-fluid appbtn_area" id="dwn-app">
    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <div class="col-md-6 col-sm-6"><a href="https://itunes.apple.com/app/id1016180237" target="_blank"><img
                            src="{{url('b2c/images/app_btn.png')}}" class="img-responsive  mrg-top"></a></div>
            <div class="col-md-6 col-sm-6"><a href="https://play.google.com/store/apps/details?id=com.eyecareapp"
                                              target="_blank"><img src="{{url('b2c/images/google_play.png')}}"
                                                                   class="img-responsive  mrg-top"></a></div>
        </div>
    </div>
</div>
<!--aboutteam-Part-Start-->
<!--Aboutteam-Part-Start-->
<div class="container-fluid team_area" id="team-1">
    <div class="container">
        <div class="col-md-12">

            <h2>About Team</h2>
            <p>Drs. Garoon and Garoon are ophthalmologists in the United States. Dr. Ira Garoon is a board certified,
                retina fellowship trained ophthalmologist who owns his own private practice in Illinois. With over 30
                years of training Dr. Garoon is well known in his community for his efforts at preventing vision loss
                and blindess. Dr. Robert Garoon is an ophthalmologist in training with future interests in disease of
                the retina and vitreous. He is especially proud of his efforts focusing on telemedicine and making
                preventative efforts at vision loss a reality through technology.</p>
            <div class="col-md-8 col-md-offset-2">
                <div class="col-md-6">
                    <div class="col-md-12"><img src="{{url('b2c/images/robert.png')}}" class="img-responsive drimg"></a>
                    </div>
                    <div class="col-md-12 rdc-padding">
                        <h3>Dr. Robert Garoon</h3>
                        <p>Ophthalmologist in Training</p>
                        <p align="center">
                            <span class="scol-icon"><a href="https://twitter.com/eyecareapp"><img src="images/twtr.png"
                                                                                                  alt=""></a></span>
                            <span class="scol-icon"><a
                                        href="https://www.facebook.com/profile.php?id=100010019378866&fref=ts"><img
                                            src="images/fcbk.png" alt=""></a></span>
                            <span class="scol-icon"><a href="https://www.pinterest.com/amslerapp/amsler-app/"><img
                                            src="images/pin.png" alt=""></a></span></p>
                        </a></div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12"><img src="{{url('b2c/images/ira.png')}}" class="img-responsive drimg"></a>
                    </div>
                    <div class="col-md-12 rdc-padding">
                        <h3>Dr. Ira Garoon</h3>
                        <p>Specialist in Diseases of Retina & Vitreous</p>
                        <p align="center">
                            <span class="scol-icon"><a href="https://twitter.com/eyecareapp"><img
                                            src="{{url('b2c/images/twtr.png')}}" alt=""></a></span>
                            <span class="scol-icon"><a
                                        href="https://www.facebook.com/profile.php?id=100010019378866&fref=ts"><img
                                            src="images/fcbk.png" alt=""></a></span>
                            <span class="scol-icon"><a href="https://www.pinterest.com/amslerapp/amsler-app/"><img
                                            src="{{url('b2c/images/pin.png')}}" alt=""></a></span></p>
                        </a></div>
                </div>
            </div>

        </div>
    </div>

</div>
<!--Aboutteam-Part-End-->
<!--Contactus-Part-Start-->
<div class="container-fluid contact_area" id="contact">
    <div class="container ">
        <div class="col-md-12">

            <h2>Contact Us</h2>
            <p>Please feel free to send us any questions, comments or concerns you may have with the application, Amsler
                grid testing, or macular degeneration. We will get back to you at the earliest opportunity. If this is
                an emergency please seek care from your local eye care specialist.</p>
            <div id="showMsg" style="text-align:center;width:100%;"></div>
            <div class=" form_area">

                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" class="form-control" id="fullname" placeholder="Your Name *">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Your E-mail *">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="subject" placeholder="Subject">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control" rows="9" id="comment" placeholder="Your Message *"></textarea>
                    </div>

                    <button type="submit" class="btn btn-default sbmt-btn" align="center" onclick="contactAdd();">
                        Submit
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container-fluid footer-bg">
    <div class="container">
        <div class="col-md-3">
            <p>© 2015 eyecare</p>

        </div>

        <div class="col-md-09">
            <div class=" term-text  ">

                <ul>
                    <li><a href="{{route('cms-terms')}}" target="_blank">Terms</a></li>
                    <li><a href="{{route('cms-learn')}}" target="_blank"> About AMD </a></li>
                    <li><a href="{{route('cms-privacy')}}" target="_blank">Privacy</a></li>
                    <li><a href="{{route('admin-login-form')}}" target="_blank">Admin Login</a></li>
                    {{--<li><a href="{{route('patient-reset-password-form')}}" target="_blank">Patient Forget Password</a></li>--}}
                    {{--<li><a href="{{route('doctor-reset-password-form')}}" target="_blank">Doctor Forget Password</a></li>--}}

                </ul>
            </div>
        </div>


    </div> <!--End ofcontainer-->

</div>   <!--End of container-fluid footer-bg-->


<!--Contactus-Part-End-->

<script src="js/bootstrap.min.js"></script>

<script>$(function () {
        $('a[href*=#]:not([href=#])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 800);
                    return true;
                }
            }
        });

        $('.fancybox').fancybox();
    });


    function ValidateEmail(mail) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return (true)
        }
        return (false)
    }

    function contactAdd() {
        if (document.getElementById('fullname').value == '' || document.getElementById('fullname').value == 'Your Name *') {
            alert('Please enter Name');
            document.getElementById('fullname').focus();
            return false;
        }

        if (document.getElementById('email').value == '' || document.getElementById('fullname').value == 'Your E-mail *') {
            alert('Please enter E-mail');
            document.getElementById('email').focus();
            return false;
        }

        if (!ValidateEmail(document.getElementById('email').value)) {
            alert("You have entered an invalid email address!")
            document.getElementById('email').focus();
            return false;
        }

        if (document.getElementById('comment').value == '' || document.getElementById('comment').value == 'Your Message *') {
            alert('Please enter Message');
            document.getElementById('comment').focus();
            return false;
        }

        const csrf_token = "{{ csrf_token() }}";
        let formData = "fullname=" + document.getElementById('fullname').value;
        formData += "&email=" + document.getElementById('email').value;
        formData += "&subject=" + document.getElementById('subject').value;
        formData += "&comment=" + document.getElementById('comment').value;
        formData += "&_token=" + csrf_token;
        $.ajax({
            url: "/contact-us",
            type: "POST",
            data: formData,
            success: function (response, textStatus, jqXHR) {
                if (response.data == "mailsent") {
                    document.getElementById('fullname').value = '';
                    document.getElementById('email').value = '';
                    document.getElementById('subject').value = '';
                    document.getElementById('comment').value = '';
                    document.getElementById('showMsg').innerHTML = '<span style="color:#fff;padding: 5px; background:#008000;font-size:12px;">Your contact details sent successfully to administrator</span>';
                }
                else {
                    document.getElementById('showMsg').innerHTML = '<span style="color:#fff;padding: 5px; background:#ff0000;font-size:12px;">Error occured. Try later!</span>';
                }
                //data - response from server
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }
</script>
</body>
</html>