@include('b2c/partial/head')
<html>
<body style="padding-top:75px;">
{!!  Form::open(array('route' => 'doctor-register','id'=>'frmPageDetails','files' => true)) !!}
<table border="0" align="center">
    <tr>
        <td valign="top" class="">
            <table width="542" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-top:10px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="10" valign="top"><img
                                                        src="{{asset('b2c/images/data-area-1-1.png')}}" alt=""
                                                        width="10" height="10"/></td>
                                            <td valign="top" class="boxtopmid"><img
                                                        src="{{asset('b2c/images/data-area-1-2.png')}}"
                                                        alt="" width="1" height="10"/>
                                            </td>
                                            <td width="10" align="right" valign="top"><img
                                                        src="{{asset('b2c/images/data-area-1-3.png')}}" alt=""
                                                        width="10"
                                                        height="10"/></td>
                                        </tr>
                                        <tr>
                                            <td class="boxvarleft">&nbsp;</td>
                                            <td valign="top" class="formboxdatabg">
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td><img src="{{asset('b2c/images/logo.png')}}" alt=""
                                                                 border="0"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="loginhead"><i class="fa fa-user-md"> </i> Sign Up As
                                                            A Doctor
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center"
                                                            class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="background-color:#eeeeee; padding-left:10px; padding-top:10px; padding-bottom:10px;">
                                                            <table width="100%" border="0" cellpadding="3"
                                                                   cellspacing="4">
                                                                <!-- Doctor Name -->
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="22%" class="logincaption">Doctor Name
                                                                        : <span class="redrequird">*</span></td>
                                                                    <td width="7%" class="logindots">&nbsp;</td>
                                                                    <td width="71%"><label></label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="1" class="logincaption"><input
                                                                                name="firstName"
                                                                                value="{{ old('firstName') }}"
                                                                                placeholder="Enter First Name"
                                                                                type="text" size="50" id="firstName"
                                                                                class="validate[required]"/>
                                                                        <span id="spnUsername"
                                                                              style="display:none"
                                                                              class="error_msg">
                                                                        </span>
                                                                    </td>
                                                                    <td colspan="1" class="logincaption"><input
                                                                                name="lastName"
                                                                                value="{{ old('lastName') }}"
                                                                                placeholder="Enter Last Name"
                                                                                type="text" size="50" id="lastName"
                                                                                class="validate[required]"/>
                                                                        <span id="spnUsername"
                                                                              style="display:none"
                                                                              class="error_msg">
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <!-- Name -->
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="22%" class="logincaption">Email
                                                                        : <span class="redrequird">*</span></td>
                                                                    <td width="7%" class="logindots">&nbsp;</td>
                                                                    <td width="71%"><label></label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" class="logincaption"><input
                                                                                name="email"
                                                                                value="{{ old('email') }}"
                                                                                type="text" size="50" id="email"
                                                                                class="validate[required]"/>
                                                                        <span id="spnUsername"
                                                                              style="display:none"
                                                                              class="error_msg"></span></td>

                                                                </tr>
                                                                <tr>
                                                                    <td class="logincaption">Password
                                                                        : <span class="redrequird">*</span></td>
                                                                    <td class="logindots">&nbsp;</td>
                                                                    <td><label></label></td>
                                                                </tr>

                                                                <tr>
                                                                    <td colspan="3"><span class="logincaption">
                                      <input name="password" class="validate[required]"
                                             type="password" size="50" id="pass" placeholder="*****"/>
                                      <span id="spnPassword" style="display:none" class="error_msg"></span> </span></td>
                                                                </tr>
                                                                <!-- Speciality-->
                                                                <tr>
                                                                    <td width="22%" class="logincaption">Speciality
                                                                        : <span class="redrequird">*</span></td>
                                                                    <td width="7%" class="logindots">&nbsp;</td>
                                                                    <td width="71%"><label></label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" class="logincaption">
                                                                        <input
                                                                                name="speciality"
                                                                                value="{{ old('speciality') }}"
                                                                                type="text" size="50" id="speciality"
                                                                                class="validate[required]"/>
                                                                        <span id="spnUsername"
                                                                              style="display:none"
                                                                              class="error_msg"></span></td>
                                                                </tr>
                                                                <!-- -->
                                                                <!-- Address-->
                                                                <tr>
                                                                    <td width="22%" class="logincaption">Address Line1
                                                                        : <span class="redrequird">*</span></td>
                                                                    <td width="7%" class="logindots">&nbsp;</td>
                                                                    <td width="71%"><label></label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" class="logincaption">
                                                                        <textarea name="address" id="address"
                                                                                  class="validate[required]"
                                                                                  style="width:324px">
                                                                            {{ old('address') }}
                                                                        </textarea>
                                                                        <span id="spnUsername"
                                                                              style="display:none"
                                                                              class="error_msg"></span></td>
                                                                </tr>
                                                                <!-- -->
                                                                <!-- City |State | ZIP -->
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="22%" class="logincaption">Address Info
                                                                        : <span class="redrequird">*</span></td>
                                                                    <td width="7%" class="logindots">&nbsp;</td>
                                                                    <td width="71%"><label></label></td>
                                                                </tr>
                                                                <tr>
                                                                    {{--<td colspan="2" width="22%" class="logincaption">City--}}
                                                                    {{--: <span class="redrequird">*</span>--}}
                                                                    {{--</td>--}}
                                                                    <td colspan="1" class="logincaption"><input
                                                                                name="city"
                                                                                value="{{ old('city') }}"
                                                                                placeholder="City"
                                                                                type="text" size="50" id="city"
                                                                                class="validate[required]"/>
                                                                        <span id="spnUsername"
                                                                              style="display:none"
                                                                              class="error_msg">
                                                                        </span>
                                                                    </td>
                                                                    <td colspan="1" class="logincaption"><input
                                                                                name="state"
                                                                                value="{{ old('state') }}"
                                                                                placeholder="State"
                                                                                type="text" size="50" id="state"
                                                                                class="validate[required]"/>
                                                                        <span id="spnUsername"
                                                                              style="display:none"
                                                                              class="error_msg">
                                                                        </span>
                                                                    </td>
                                                                    <td colspan="1" class="logincaption"><input
                                                                    name="zip"
                                                                    value="{{ old('zip') }}"
                                                                    placeholder="ZIP Code"
                                                                    type="text" size="50" id="zip"
                                                                    class="validate[required]"/>
                                                                    <span id="spnUsername"
                                                                    style="display:none"
                                                                    class="error_msg">
                                                                    </span>
                                                                    </td>
                                                                </tr>
                                                                <!-- Address Info -->
                                                                <!--profile photo -->
                                                                <tr>
                                                                    <td width="22%" class="logincaption">Upload your
                                                                        photo
                                                                        :
                                                                    </td>
                                                                    <td width="7%" class="logindots">
                                                                        <label class="redrequird">
                                                                            <font class="note redrequird">[Note: Upload
                                                                                only
                                                                                jpg, gif, png file type]</font> <br/>
                                                                        </label>
                                                                    </td>
                                                                    <td width="71%">

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" class="logincaption">
                                                                        <input type="file" name="profileImage"
                                                                               id="profileImage"
                                                                               value="{{old ('profileImage')}}"/>
                                                                        <span id="spnUsername"
                                                                              style="display:none"
                                                                              class="error_msg"></span>
                                                                    </td>
                                                                </tr>
                                                                <!-- END profile photo-->
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <hr style="margin-top: 15px"></hr>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <h3><i class="fa fa-money"></i> Card Information
                                                                        </h3>
                                                                    </td>
                                                                </tr>

                                                                <!-- Card Number-->
                                                                <tr>
                                                                    <form action="/charge" method="post"
                                                                          id="payment-form">
                                                                        <td colspan="2">
                                                                            <div id="card-element">
                                                                                <!-- A Stripe Element will be inserted here. -->
                                                                            </div>

                                                                            <!-- Used to display form errors. -->
                                                                            <div id="card-errors" role="alert"></div>
                                                                            <input type="hidden" id="token" name="token"
                                                                                   value="{{ old('token') }}"/>
                                                                        </td>
                                                                        <td>
                                                                            {{--<button id="stripeSubmit-Btn">--}}
                                                                            {{--Submit Payment--}}
                                                                            {{--</button>--}}
                                                                        </td>
                                                                    </form>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" class="logincaption">
                                                                        By checking this box, you confirm
                                                                        your {{$subscriptionAmount}} recurring
                                                                        monthly payment and acceptance of the
                                                                        <a href="{{route('cms-doctor-terms')}}"
                                                                           target="_blank">
                                                                            Terms And Conditions
                                                                        </a>
                                                                        : <span class="redrequird">*</span>
                                                                    </td>
                                                                    <td colspan="1" width="7%" class="logindots">
                                                                        <input
                                                                                name="isAccept"
                                                                                id="isAccept"
                                                                                value="{{ old('isAccept') }}"
                                                                                type="checkbox"
                                                                                class="validate[required] form-control-lg"/>
                                                                    </td>
                                                                    <td width="71%"><label></label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" class="logincaption">
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" style="padding-top:15px;">
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    {{--<button id="stripeSubmit-Btn">--}}
                                                                                    {{--Submit Payment--}}
                                                                                    {{--</button>--}}
                                                                                    <input type="image"
                                                                                           name="Submit"
                                                                                           id="stripeSubmit-Btn"
                                                                                           src="{{asset("b2c/images/submit.png")}}"
                                                                                           width="88"
                                                                                           height="36"
                                                                                    />
                                                                                </td>
                                                                                <td>&nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    <img src="{{asset("b2c/images/cancel.png")}}"
                                                                                         width="91" height="36"
                                                                                         onClick="window.location='/'"
                                                                                         style="cursor:pointer"
                                                                                         title="Back To Home"
                                                                                         border="0"/></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="boxvarright">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><img
                                                        src="{{asset("b2c/images/data-area-3-1.png")}}"
                                                        alt="" width="10"
                                                        height="10"/></td>
                                            <td valign="top" class="boxbottommid"><img
                                                        src="{{asset("b2c/images/data-area-3-2.png")}}" alt=""
                                                        width="1"
                                                        height="10"/></td>
                                            <td align="right"><img
                                                        src="{{asset("b2c/images/data-area-3-3.png")}}"
                                                        alt="" width="10"
                                                        height="10"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="passwordtxt">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    </td>
                </tr>
            </table>
    </tr>
</table>
{!! Form::close() !!}

</body>
</html>
<link rel="stylesheet" href="{{asset('b2c/css/payment-checkout.css')}}">
<script src="https://js.stripe.com/v3/"></script>
<script src="{{asset('b2c/js/payment-checkout.js')}}"></script>
<script>

    var stripe = Stripe("{{$apiKey}}");

</script>
