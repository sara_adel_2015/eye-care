@extends('b2c/b2c-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {!!  Form::open(array('route' => 'doctor-save-payment-card','id'=>'frmPageDetails','method'=>'POST')) !!}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td valign="top" class="">
                    <table width="100%" border="0" cellpadding="4" cellspacing="0">
                        <tr>
                            <td colspan="3">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class='loginhead'><i class="fa fa-money"></i>Add New Card Information</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="redrequirdbig" colspan="3" align="right">* Indicates required field</td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>

                        <tr>
                            <td align="left"
                                class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="card-element">
                                    <!-- A Stripe Element will be inserted here. -->
                                </div>

                                <!-- Used to display form errors. -->
                                <div id="card-errors" role="alert"></div>
                                <input type="hidden" id="token" name="token"
                                       value="{{ old('token') }}"/>
                            </td>
                            <td>
                                <button id="stripeSubmit-Btn">
                                    Submit Payment
                                </button>
                            </td>
                        </tr>
                        <!--End Card Expiration -->
                        <!-- New Card Form -->

                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                @include('b2c/partial/submit-form-action', ['resetBtnRoute' => 'doctor-add-payment-card'])
                            </td>
                        </tr>
                    </table>
            </tr>
        </table>
        {!! Form::close() !!}
    </td>
@endsection
@prepend('scripts')
    <script>
        pageForm = "frmPageDetails";

        function beforeSubmit() {
            $.validationEngine.beforeSubmit(pageForm);
        }
    </script>
    <link rel="stylesheet" href="{{asset('b2c/css/payment-checkout.css')}}">
    <script src="https://js.stripe.com/v3/"></script>
    <script src="{{asset('b2c/js/payment-checkout.js')}}"></script>
    <script>
        var stripe = Stripe("{{$apiKey}}");
    </script>
@endprepend