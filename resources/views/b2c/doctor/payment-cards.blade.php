@extends('b2c/b2c-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {!!  Form::open(array('route' => 'doctor-update-current-payment','id'=>'frmPageDetails','method'=>'PUT')) !!}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            {{--<tr>--}}
            {{--<td valign="top" class="">--}}
            {{--<table width="100%" border="0" cellpadding="4" cellspacing="0">--}}
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class='loginhead'>Payment Cards</td>
                        </tr>
                        <tr>
                            <td align="right"><a href="{{URL::route('doctor-add-payment-card')}}"
                                                 class="links"><img
                                            width="20" height="20" src="{{asset('admin/images/add_small.png')}}"
                                            border="0"
                                            style="margin-top:-25px;"
                                            alt="Add New"
                                            title="Add New"/></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td class="redrequirdbig" colspan="2" align="right">* Indicates required field</td>
            </tr>
            <tr>

                @if(Auth::user()->getIsSubscriptionCancelled())
                    <td align="center" style="font-size: medium">
                        <b>Your Subscription has been cancelled</b>
                    </td>
                    <td></td>
                @elseif(Auth::user()->getIsRequestToCancelSubscription())
                    <td align="center" style="font-size: medium">
                        <b>Your Request to cancel Subscription is in processing</b>
                    </td>
                    <td></td>
                @else
                    <td></td>
                    <td align="right">
                        <a id="cancelSubscription" href="javascript:void(0)">
                            Request Cancel Subscription
                        </a>
                        @endif
                    </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>

            <tr>
                <td align="left"
                    class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}
                </td>
            </tr>
            @foreach ($paymentCards as $paymentCard)
                <tr>
                    <td width="80%">
                        <label>
                            @if($paymentCard->getId() == $currentPaymentCard)
                                <input type="radio" name="paymentCardId" id="paymentCardId"
                                       value="{{$paymentCard->getId()}}" checked/>
                                <span><b>  ************{{$paymentCard->getLastFourDigits()}}</b>
                                        </span>
                                <span class="redrequird">(Default)</span>
                            @else
                                <input type="radio" name="paymentCardId" id="paymentCardId"
                                       value="{{$paymentCard->getId()}}"/>
                                <span><b>  ************{{$paymentCard->getLastFourDigits()}}</b>
                                        </span>
                            @endif
                        </label>
                    </td>
                    <td width="20%">
                        <a href="{{URL(route('doctor-payment-card-history',['id'=>$paymentCard->getId()]))}}"
                           title="View History"><i
                                    class="fa fa-history"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    @include('b2c/partial/submit-form-action', ['resetBtnRoute' => 'doctor-payment-cards'])
                </td>
            </tr>
            {{--</table>--}}
            {{--</tr>--}}
        </table>
        {!! Form::close() !!}
    </td>
@endsection
@prepend('scripts')
    <script>
        pageForm = "frmPageDetails";

        function beforeSubmit() {
            $.validationEngine.beforeSubmit(pageForm);
        }

        $(function () {
            $('#cancelSubscription').on('click', function () {
                if (confirm('Are you sure you want to cancel your subscription')) {
                    window.location.href = "{{route('request-cancel-subscription')}}"
                }
            })
        });
    </script>
    <link rel="stylesheet" href="{{asset('b2c/css/payment-checkout.css')}}">

@endprepend
