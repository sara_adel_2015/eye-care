@extends('b2c/b2c-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {{--<form name="frmCmsPageList" id="frmCmsPageList" method="GET" action="{{URL::route('patient-list')}}">--}}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td class='loginhead'>Patient List</td>
            </tr>
            <!-- Search -->
            <!-- Search -->
            <tr>
                <td align="left">

                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="middle">
                                <b>Search:</b> &nbsp;&nbsp;<?php
                                foreach (range('A', 'Z') as $char) {
                                    echo "<a href='/doctor/patient?search=" . $char . "'>" . $char . "</a> &nbsp;&nbsp;";
                                }?></td>
                            <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt="" width="14"
                                                  height="33"/></td>
                            <td><input type="button" name="reset" value="Reset" class="buttonmidle"
                                       onclick="window.location.href='/doctor/patient'"/></td>
                            <td align="right" valign="top"><img src="{{asset('admin/images/button-right.png')}}" alt=""
                                                                width="15"
                                                                height="33"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- End Search-->
            <tr>
                <td align="left" class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}</td>
            </tr>

            <tr>
                <td>
                    <table width="100%" class="table-border" border="0" cellspacing="2">
                        <tr>
                            <td align="center" class="list-title" width="7%">Sr.No</td>
                            <td align="center" class="list-title" width="32%">Patient Name</td>
                            <td align="center" class="list-title" width="10%">Status</td>
                            <td class="list-title" colspan="3" width="20%" align="center">Action</td>
                        </tr>
                        <?php $index = 0;?>
                        @foreach ($patients as $patient)
                            <?php $index++;?>
                            <tr class="Hrnormal" onmouseover="this.className='Hrhover';"
                                onmouseout="this.className='Hrnormal';">
                                <td align="center" class="list-contents">{{$patient->getId()}}</td>
                                <td align="center"
                                    class="list-contents">{{$patient->getFirstName()}} {{$patient->getLastName()}}
                                </td>
                                <td align="center" class="list-contents">
                                    @if ($patient->getStatus() == 'Active')
                                        <a href="javascript:void(0)"
                                           onclick="updateStatus('{{$patient->getId()}}','IN_ACTIVE');"><img
                                                    alt="{{$patient->getStatus()}}" title="{{$patient->getStatus()}}"
                                                    src="{{asset('admin/images/true.gif')}}"
                                                    al border="0"/></a>
                                    @elseif ($patient->getStatus() == 'Inactive')
                                        <a href="javascript:void(0)"><img
                                                    alt="{{$patient->getStatus()}}"
                                                    title="{{$patient->getStatus()}}"
                                                    src="{{asset('admin/images/false.gif')}}"
                                                    al border="0"/></a>
                                    @endif
                                </td>
                                <td align="center" class="list-contents">
                                    <a href="{{URL::route('doctor-eye-test-list')}}?patientId={{$patient->getId()}}"
                                       class="links"><img
                                                src="{{asset('admin/images/view.png')}}" alt="View Eyetest"
                                                title="View Eyetest" width="16"
                                                height="16" border="0"/></a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                {{--**Pagination here**--}}
                {{--{{ $patients->appends(request()->input())->links('admin/partial/paginator') }}--}}

            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        {{--</form>--}}
    </td>
@endsection
@push('scripts')
    <script>
        csrf_token = "{{ csrf_token() }}";
    </script>
    <script type="text/javascript" src="{{asset('admin/js/patient.js')}}"></script>
@endpush