@extends('b2c/b2c-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {{--<form name="frmCmsPageList" id="frmCmsPageList" method="post" action="doctorlist.php">--}}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td class='loginhead'><i class="fa fa-history"></i>Payment Card Recurring History </td>
            </tr>
            <tr>
                <td align="right" style="padding-right:20px;">
                </td>
            </tr>
            <tr>
                <td align="left" class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}</td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="table-border" border="0" cellspacing="2">
                        <tr>
                            <td align="center" class="list-title" width="20%">Payment Transaction ID</td>
                            <td align="center" class="list-title" width="15%">Amount</td>
                            <td align="center" class="list-title" width="10%">Status</td>
                            <td align="center" class="list-title" width="10%">Message</td>
                            <td align="center" class="list-title" width="10%">Date</td>
                        </tr>
                        <?php $index = 0;?>
                        @foreach ($list as $history)
                            <tr class="Hrnormal" onmouseover="this.className='Hrhover';"
                                onmouseout="this.className='Hrnormal';">
                                <td align="center" class="list-contents">{{$history->getChargeId()}}</td>
                                <td align="center" class="list-contents">{{$history->getAmount()}}</td>
                                <td align="center" class="list-contents">
                                    @if($history->getIsSuccess())
                                        <span style="color: forestgreen">
                                               <i class="fa fa-check"></i>Success
                                        </span>
                                    @else <span style="color: red">Failed</span>
                                    @endif
                                </td>
                                <td align="center" class="list-contents">{{$history->getMessage()}}</td>
                                <td align="center"
                                    class="list-contents">{{$history->getCreatedAt()->format('yy-m-d h:i:s')}}</td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                {{--**Pagination here**--}}
                <td>{{$list->links()}}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        {{--</form>--}}
    </td>
@endsection
@push('scripts')
    <script>
        csrf_token = "{{ csrf_token() }}";
        pageForm = "frmPageDetails";
        const BASE_URL = '/patient/eye-test';

        function deleteEyeTest(id) {
            if (confirm("Are you sure want to delete this record?")) {
                $.ajax({
                    method: "DELETE",
                    url: BASE_URL + '/' + id,
                    data: {"_token": csrf_token,}
                }).done(function (response) {
                    window.location.href = BASE_URL;
                });
            }
        }
    </script>
@endpush
