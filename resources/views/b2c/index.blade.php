@extends('b2c/b2c-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td class="loginhead">Home</td>
            </tr>
            <tr>
                <td align="left" class="{{ Session::get('flash_type') }}"> {{ Session::get('flash_message') }}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="left" class="homefontformtxt"> Welcome to administration!</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="left" class="homefontformtxt">Please select an option from the navigation bar above.</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </td>
@endsection