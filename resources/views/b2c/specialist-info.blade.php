<html>
<head>
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
</head>
<body>
<div class="col-md-12 col-sm-12 rdc-padding">
    <div class="col-md-8 rdc-padding video-title" align="left">
        <h2 class="about_heading">
            <strong class="headingcolor">
                Eyecare- Amsler Grid Interactive App: How to Video for Eye Care Specialists
            </strong>
        </h2>
        <div class="video-content">
            {{--<iframe width="700" height="315" src="https://drive.google.com/file/d/1rn4BrGWi1jRIGwLB-G9hmLNX2Ap6u_Qg/view?ts=606b1f24" frameborder="0" allowfullscreen></iframe>--}}
            {{--</iframe>--}}
            {{--<iframe src="https://drive.google.com/file/d/1rn4BrGWi1jRIGwLB-G9hmLNX2Ap6u_Qg/preview" width="640" height="480"></iframe>--}}
            <iframe width="560" height="315" src="https://www.youtube.com/embed/mhqv3ce2yoY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <p style="text-align:justify">
                The Eyecare Amsler Grid App will help you keep track of your patients vision remotely. Any time your patient uses the app you will get an update about their current use. You can easily upload these images to the patient’s medical records to make sure the patient is using the grid accurately and there are no changes in their vision!
            </p>
        </div>
        <div class="video-note">
            Click <a href="{{URL::route('doctor-register')}}">Here</a> to sign up today!
        </div>
        <div class="video-note">
            Click <a href="/"> Here </a> to return to homepage
        </div>
        <br>
    </div>
</div>
</body>
</html>