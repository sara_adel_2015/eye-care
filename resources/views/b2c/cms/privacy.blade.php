<html xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:w="urn:schemas-microsoft-com:office:word"
      xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
      xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <meta name=Title content="">
    <meta name=Keywords content="">
    <meta http-equiv=Content-Type content="text/html; charset=macintosh">
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content="Microsoft Word 14">
    <meta name=Originator content="Microsoft Word 14">
    <link rel=File-List href="privacy_files/filelist.xml">
    <!--[if gte mso 9]><xml>
        <o:DocumentProperties>
            <o:Author>Abdurrashid</o:Author>
            <o:LastAuthor>Abdurrashid</o:LastAuthor>
            <o:Revision>2</o:Revision>
            <o:TotalTime>1</o:TotalTime>
            <o:Created>2015-06-29T08:10:00Z</o:Created>
            <o:LastSaved>2015-06-29T08:12:00Z</o:LastSaved>
            <o:Pages>3</o:Pages>
            <o:Words>3042</o:Words>
            <o:Characters>17344</o:Characters>
            <o:Lines>144</o:Lines>
            <o:Paragraphs>40</o:Paragraphs>
            <o:CharactersWithSpaces>20346</o:CharactersWithSpaces>
            <o:Version>14.0</o:Version>
        </o:DocumentProperties>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
        </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <link rel=themeData href="privacy_files/themedata.xml">
    <!--[if gte mso 9]><xml>
        <w:WordDocument>
            <w:Zoom>75</w:Zoom>
            <w:SpellingState>Clean</w:SpellingState>
            <w:TrackMoves/>
            <w:TrackFormatting/>
            <w:PunctuationKerning/>
            <w:ValidateAgainstSchemas/>
            <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
            <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
            <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
            <w:DoNotPromoteQF/>
            <w:LidThemeOther>EN-US</w:LidThemeOther>
            <w:LidThemeAsian>JA</w:LidThemeAsian>
            <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
            <w:Compatibility>
                <w:BreakWrappedTables/>
                <w:SnapToGridInCell/>
                <w:WrapTextWithPunct/>
                <w:UseAsianBreakRules/>
                <w:DontGrowAutofit/>
                <w:SplitPgBreakAndParaMark/>
                <w:EnableOpenTypeKerning/>
                <w:DontFlipMirrorIndents/>
                <w:OverrideTableStyleHps/>
                <w:UseFELayout/>
            </w:Compatibility>
            <m:mathPr>
                <m:mathFont m:val="Cambria Math"/>
                <m:brkBin m:val="before"/>
                <m:brkBinSub m:val="&#45;-"/>
                <m:smallFrac m:val="off"/>
                <m:dispDef/>
                <m:lMargin m:val="0"/>
                <m:rMargin m:val="0"/>
                <m:defJc m:val="centerGroup"/>
                <m:wrapIndent m:val="1440"/>
                <m:intLim m:val="subSup"/>
                <m:naryLim m:val="undOvr"/>
            </m:mathPr></w:WordDocument>
    </xml><![endif]--><!--[if gte mso 9]><xml>
        <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
                        DefSemiHidden="true" DefQFormat="false" DefPriority="99"
                        LatentStyleCount="276">
            <w:LsdException Locked="false" Priority="0" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
            <w:LsdException Locked="false" Priority="9" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
            <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
            <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
            <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
            <w:LsdException Locked="false" Priority="10" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Title"/>
            <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
            <w:LsdException Locked="false" Priority="11" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
            <w:LsdException Locked="false" Priority="22" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
            <w:LsdException Locked="false" Priority="20" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
            <w:LsdException Locked="false" Priority="59" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Table Grid"/>
            <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
            <w:LsdException Locked="false" Priority="1" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 1"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
            <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
            <w:LsdException Locked="false" Priority="34" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
            <w:LsdException Locked="false" Priority="29" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
            <w:LsdException Locked="false" Priority="30" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 1"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 2"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 2"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 3"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 3"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 4"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 4"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 5"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 5"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
            <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
            <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light List Accent 6"/>
            <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
            <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Dark List Accent 6"/>
            <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
            <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
            <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                            UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
            <w:LsdException Locked="false" Priority="19" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
            <w:LsdException Locked="false" Priority="21" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
            <w:LsdException Locked="false" Priority="31" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
            <w:LsdException Locked="false" Priority="32" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
            <w:LsdException Locked="false" Priority="33" SemiHidden="false"
                            UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
            <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
            <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
        </w:LatentStyles>
    </xml><![endif]-->
    <style>
        <!--
        /* Font Definitions */
        @font-face
        {font-family:Times;
            panose-1:2 0 5 0 0 0 0 0 0 0;
            mso-font-charset:0;
            mso-generic-font-family:auto;
            mso-font-pitch:variable;
            mso-font-signature:3 0 0 0 1 0;}
        @font-face
        {font-family:"\FF2D\FF33 \660E\671D";
            mso-font-charset:78;
            mso-generic-font-family:auto;
            mso-font-pitch:variable;
            mso-font-signature:1 134676480 16 0 131072 0;}
        @font-face
        {font-family:"\FF2D\FF33 \660E\671D";
            mso-font-charset:78;
            mso-generic-font-family:auto;
            mso-font-pitch:variable;
            mso-font-signature:1 134676480 16 0 131072 0;}
        @font-face
        {font-family:"Lato Regular";
            panose-1:2 15 5 2 2 2 4 3 2 3;
            mso-font-charset:0;
            mso-generic-font-family:auto;
            mso-font-pitch:variable;
            mso-font-signature:-520092929 1342237951 33 0 415 0;}
        /* Style Definitions */

        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {mso-style-unhide:no;
            mso-style-qformat:yes;
            mso-style-parent:"";
            margin:0cm;
            margin-bottom:.0001pt;
            mso-pagination:widow-orphan;
            font-size:12.0pt;
            font-family:Cambria;
            mso-ascii-font-family:Cambria;
            mso-ascii-theme-font:minor-latin;
            mso-fareast-font-family:"\FF2D\FF33 \660E\671D";
            mso-fareast-theme-font:minor-fareast;
            mso-hansi-font-family:Cambria;
            mso-hansi-theme-font:minor-latin;
            mso-bidi-font-family:"Times New Roman";
            mso-bidi-theme-font:minor-bidi;}
        h3
        {mso-style-priority:9;
            mso-style-unhide:no;
            mso-style-qformat:yes;
            mso-style-link:"Heading 3 Char";
            mso-margin-top-alt:auto;
            margin-right:0cm;
            mso-margin-bottom-alt:auto;
            margin-left:0cm;
            mso-pagination:widow-orphan;
            mso-outline-level:3;
            font-size:13.5pt;
            font-family:Times;
            font-weight:bold;}
        a:link, span.MsoHyperlink
        {mso-style-noshow:yes;
            mso-style-priority:99;
            color:blue;
            text-decoration:underline;
            text-underline:single;}
        a:visited, span.MsoHyperlinkFollowed
        {mso-style-noshow:yes;
            mso-style-priority:99;
            color:purple;
            mso-themecolor:followedhyperlink;
            text-decoration:underline;
            text-underline:single;}
        p
        {mso-style-noshow:yes;
            mso-style-priority:99;
            mso-margin-top-alt:auto;
            margin-right:0cm;
            mso-margin-bottom-alt:auto;
            margin-left:0cm;
            mso-pagination:widow-orphan;
            font-size:10.0pt;
            font-family:Times;
            mso-fareast-font-family:"\FF2D\FF33 \660E\671D";
            mso-fareast-theme-font:minor-fareast;
            mso-bidi-font-family:"Times New Roman";
            mso-bidi-theme-font:minor-bidi;}
        span.Heading3Char
        {mso-style-name:"Heading 3 Char";
            mso-style-priority:9;
            mso-style-unhide:no;
            mso-style-locked:yes;
            mso-style-link:"Heading 3";
            mso-ansi-font-size:13.5pt;
            mso-bidi-font-size:13.5pt;
            font-family:Times;
            mso-ascii-font-family:Times;
            mso-hansi-font-family:Times;
            font-weight:bold;}
        span.SpellE
        {mso-style-name:"";
            mso-spl-e:yes;}
        .MsoChpDefault
        {mso-style-type:export-only;
            mso-default-props:yes;
            font-family:Cambria;
            mso-ascii-font-family:Cambria;
            mso-ascii-theme-font:minor-latin;
            mso-fareast-font-family:"\FF2D\FF33 \660E\671D";
            mso-fareast-theme-font:minor-fareast;
            mso-hansi-font-family:Cambria;
            mso-hansi-theme-font:minor-latin;
            mso-bidi-font-family:"Times New Roman";
            mso-bidi-theme-font:minor-bidi;}
        @page WordSection1
        {size:595.0pt 842.0pt;
            margin:72.0pt 90.0pt 72.0pt 90.0pt;
            mso-header-margin:35.4pt;
            mso-footer-margin:35.4pt;
            mso-paper-source:0;}
        div.WordSection1
        {page:WordSection1;}
        /* List Definitions */

        @list l0
        {mso-list-id:616908411;
            mso-list-template-ids:857099126;}
        @list l0:level1
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:36.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l0:level2
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l0:level3
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:108.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l0:level4
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l0:level5
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:180.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l0:level6
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l0:level7
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:252.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l0:level8
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:288.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l0:level9
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:324.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l1
        {mso-list-id:1386179379;
            mso-list-template-ids:-1326182776;}
        @list l1:level1
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:36.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l1:level2
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l1:level3
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:108.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l1:level4
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l1:level5
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:180.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l1:level6
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l1:level7
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:252.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l1:level8
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:288.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l1:level9
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:324.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l2
        {mso-list-id:1655184953;
            mso-list-template-ids:1584188434;}
        @list l2:level1
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:36.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l2:level2
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l2:level3
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:108.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l2:level4
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l2:level5
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:180.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l2:level6
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l2:level7
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:252.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l2:level8
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:288.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        @list l2:level9
        {mso-level-number-format:bullet;
            mso-level-text:\F0B7;
            mso-level-tab-stop:324.0pt;
            mso-level-number-position:left;
            text-indent:-18.0pt;
            mso-ansi-font-size:10.0pt;
            font-family:Symbol;}
        ol
        {margin-bottom:0cm;}
        ul
        {margin-bottom:0cm;}
        -->
    </style>
    <!--[if gte mso 10]>
    <style>
        /* Style Definitions */

        table.MsoNormalTable
        {mso-style-name:"Table Normal";
            mso-tstyle-rowband-size:0;
            mso-tstyle-colband-size:0;
            mso-style-noshow:yes;
            mso-style-priority:99;
            mso-style-parent:"";
            mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
            mso-para-margin:0cm;
            mso-para-margin-bottom:.0001pt;
            mso-pagination:widow-orphan;
            font-size:12.0pt;
            font-family:Cambria;
            mso-ascii-font-family:Cambria;
            mso-ascii-theme-font:minor-latin;
            mso-hansi-font-family:Cambria;
            mso-hansi-theme-font:minor-latin;}
    </style>
    <![endif]-->
</head>

<body bgcolor=white lang=EN-US link=blue vlink=purple style='tab-interval:36.0pt'>

<div class=WordSection1>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                class=SpellE><span style='font-family:"Lato Regular"'>Eyecare</span></span><span
                style='font-family:"Lato Regular"'> App and <span class=SpellE>Amsler</span>
App Website ( �us� or �we� or �our�) operate this application/website (the
&quot;Site&quot;), and other health related websites and the services related to
or offered on the Sites (hereinafter, the &quot;Services&quot;). We may
include, without limitation, tools, applications, email services, bulletin and
message boards, chat areas, news groups, forums, communities, calendars, and
downloadable mobile applications related to the Site.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We provide you this privacy policy (the
&quot;Policy&quot;) to help you understand the kinds of information we may
gather about you when you visit the Site or use any of our Services, how we may
use and disclose the information, and how you can control, correct and/or
update the information.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>To be clear about the terminology we are
using, when we use the phrase &quot;Personal Information&quot; in this Policy,
we mean information that can be used to identify or contact a person, like a
person�s full name, address, e-mail address, or phone number. When we use the
phrase &quot;Anonymous Information&quot; in this Policy, we mean information
that cannot reasonably be used to identify individual persons.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>By visiting this Site or using the Services,
you are accepting the policies and practices described in this Policy. Each
time you visit the Site or use the Services, you agree and expressly consent to
our collection, use and disclosure of the information that you provide as
described in this Policy.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>Your Personal Information will be processed
by us in the United States.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>In addition to this general Policy, each
Service may have additional privacy provisions that are specific to the
particular Service. These supplemental disclosures are made adjacent to the
particular Service at the time we collect the information. These supplemental
disclosures also govern the use of your information.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>1. The Information We
Gather<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We collect information about you in the
following ways:</span><span style='font-family:"Lato Regular";mso-bidi-font-family:
"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>A. Information you voluntarily provide us</span></b><span
                style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We collect and maintain information that you
voluntarily submit to us during your use of the Sites and Services. For
example:<o:p></o:p></span></p>

    <ul type=disc>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l1 level1 lfo1;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>when you register for a Site, you may provide us
     certain information including your name, email address, screen name,
     password, demographic information and the health topics that interest you;<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l1 level1 lfo1;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>to engage a Site's social networking function, you may
     add to your profile by providing information about your interests or
     health topics. For example, your profile may list your favorite healthy
     foods or movie;<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l1 level1 lfo1;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>if you are registering for a customized weight loss
     program, you may provide us answers to questions we use to provide you the
     program, like your actual and goal weights, your preferred method of
     exercise, or your caloric intake;<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l1 level1 lfo1;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>if you sign up to receive a newsletter, enter a contest
     or participate in social networking activities, you may provide us contact
     information (e.g., email or physical address);<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l1 level1 lfo1;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>if you purchase a product or service, you may provide
     us your credit card information; and<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l1 level1 lfo1;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>you may provide us with Personal Information in the
     course of email, customer support interactions and surveys.<o:p></o:p></span></li>
    </ul>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>You can choose not to provide us with
certain information, but this may stop you from gaining access to a Service or
limit the features that you can use. </span><span style='font-family:"Lato Regular";
mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>B. Information we collect through your use
of the Site.</span></b><span style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>As you use the Site and Services, certain
information may also be passively collected. Through cookies, pixels, beacons,
log files and other technologies, we may collect information about how you use
the Site and the Services. For example we may determine through an IP address
that a particular computer or device is located in New York City and we may use
this information to deliver advertisements promoting New York City-based
businesses. This information allows us to deliver more helpful information,
programs, tools and advertisements. Please see the <a
                    href="http://www.everydayhealth.com/privacyterms/#cookies_and_targeted_advertising"><span
                        style='color:windowtext;text-decoration:none;text-underline:none'>Cookies and
Targeted Advertising</span></a> sections below for more information.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>C. Information we receive from third
parties.</span></b><span style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We may also combine online and/or offline
information received from third parties with the information we have already
collected from you via our Sites and Services. The third party information is
used for a variety of purposes including to verify other information about you
(e.g., verify your mailing address to send you requested products or services)
and to enhance the content and advertising we provide to you.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>2. How We Use Your
Information<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We will use your Personal Information in the
ways described below or described at the time that the information is
collected.</span><span style='font-family:"Lato Regular";mso-bidi-font-family:
"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>A. Advertising.</span></b><span
                style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We believe that advertising is more
interesting to you when it is relevant. Accordingly, we customize the
advertisements that you see based upon: (<span class=SpellE>i</span>) the
information that you provide us (e.g., age, gender, stated health interests and
other information in your profile); (ii) geographic location information, which
we may determine through your IP address, from your mobile device, or other
ways; (iii) data we receive from third parties or; (iv) your visits on the
Sites or use of Services (commonly referred to as &quot;Behavioral
Advertising&quot;). We may use your Personal Information to deliver customized
advertising to you on this Site, on other websites and on other digital and
offline media channels.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>You can limit the way that we tailor
advertising based on your self-reported interests and other information in your
profile by clicking on the &quot;Profile&quot; or &quot;My Account&quot;
section of this Site and deleting or modifying the information. Please note
that if you have registered on multiple Sites, you'll need to modify your
profile on each Site. In addition, you can delete the cookies that we have placed
on your browser.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>B. To Provide the Sites and Services.</span></b><span
                style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>In general, we use your Personal Information
as necessary or appropriate for purposes including to:<o:p></o:p></span></p>

    <ul type=disc>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo2;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>administer your account;<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo2;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>register you and provide you access to the Site or
     Services;<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo2;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>respond to inquiries or requests that you direct to us;<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo2;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>fulfill your requests for products or services;<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo2;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>send communications and administrative emails about the
     Sites or Services;<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo2;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>personalize and better tailor the features, performance
     and support of the Sites and Services for your use;<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo2;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>send you promotional/marketing information,
     newsletters, offers or other information regarding opportunities and
     functionality that we think would be of particular interest to you;<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo2;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>improve the quality of the Site and the Services, and<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo2;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>analyze, benchmark and conduct research on user data
     and user interactions with the Site and Services.<o:p></o:p></span></li>
    </ul>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>3. Cookies and
Targeted Advertising<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>A. Cookies, Web Beacons and IP Address
Information</span></b><span style='font-family:"Lato Regular";mso-bidi-font-family:
"Times New Roman"'><o:p></o:p></span></p>

    <ul type=disc>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo3;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>&quot;Cookies&quot; are small pieces of information
     that a website places on your browser when you visit that website. We may
     use cookies to provide you with a more personal and interactive experience
     with the Sites and Services. For example, we may use cookies to control
     what advertisements you see, the sequence of advertisements and to make
     sure you don't see the same advertisement too many times. For contests or
     sweepstakes, we may also use cookies in order to track your progress and
     the number of entries in some promotions. For polls, we may use cookies to
     help ensure that an individual can't vote more than once on a particular
     question or issue. We do not store Personal Information in any cookies on
     your computer.<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo3;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>Cookies can be removed by following your Internet
     browser's directions within the help tab. In order to use certain Services
     offered through the Sites, your web browser must accept cookies. If you
     choose to disable cookies, some aspects of the Sites may not work
     properly, and you may not be able to access our Services.<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo3;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>We may also use cookies, pixels, web beacons (which are
     usually small, transparent graphic images), operating system and device
     information and navigational data like Uniform Resource Locators (URL) to
     gather information regarding the date and time of your visit, the features
     and information for which you searched and viewed, the email you opened,
     or on which advertisements you clicked. This type of information is
     collected to make the Sites and Services more useful to you and to tailor
     the experience with us to meet your special interests and needs.<o:p></o:p></span></li>
        <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo3;tab-stops:list 36.0pt'><span style='font-family:
     "Lato Regular";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
     "Times New Roman"'>An &quot;Internet protocol address&quot; or &quot;IP
     Address&quot; is a number that is automatically assigned to your computer
     when you use the Internet. We, or our service providers, may use your IP
     Address when you access the Sites or Services or use other means to assist
     with delivering geographically targeted advertisements.<o:p></o:p></span></li>
    </ul>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>B. Targeted Advertising</span></b><span
                style='font-family:"Lato Regular";mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>Companies Everyday Health works with may use
cookies for purposes including understanding Web usage patterns of users who
see advertisements on the Sites, controlling the sequence of advertisements you
see, making sure you don't see the same advertisement too many times,
determining your interests and tailoring advertisements and promotions based on
those interests.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>For example, if you read an article about a
particular health condition on the Sites, we may use cookies from a vendor to
later serve you an advertisement for a product related to the viewed article.
These third-party vendors may connect information about pages you visit on our
Sites with information about pages you visit on other websites and media
channels and show you advertising based on this combined information. The
advertisement may appear when you are visiting a different section of this Site
or another website or media channel if the website or media channel also has a
relationship with us or our vendor.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>You can opt-out of cookies placed by those
vendors listed. After you opt-out, you will still see advertisements but the
advertising may not be as relevant to your interests. If you change your
computer, change your browser, or delete your cookies, you will need to renew
your opt-out. <o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>4. How We Disclose
Information to Third Parties<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We may share your Personal Information with
third parties as specifically approved by you or under the circumstances
described below. If you do not want us to use or disclose Personal Information
collected about you in the ways identified in this Policy, you should not use
the Site or Services.</span><span style='font-family:"Lato Regular";mso-bidi-font-family:
"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>A. Disclosure for legal reasons.</span></b><span
                style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We may disclose and release your Personal
Information to third parties: (a) to comply with valid legal requirements such
as a law, regulation, search warrant, subpoena or court order; or (b) in
special cases, such as a physical threat to you or others, a threat to homeland
security, a threat to our system or network, or in any cases in which we
believe it is reasonably necessary to investigate or prevent harm, fraud,
abuse, illegal conduct or a violation or alleged violation of this Policy or
other agreement we may have with you.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>B. Agents and Contractors.</span></b><span
                style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We may provide your Personal Information to
service providers who work on our behalf or help us to operate our business,
the Sites and the Services. Examples of such service providers include vendors
and suppliers that provide us with technology, research, and/or marketing
assistance; provide services and/or content for sending email and providing
customer service; analyze data, serve advertisements on our Sites and Services
and on third-party channels; and process payments (including credit card
payments). Access to your Personal Information by these service providers is
limited to the information reasonably necessary to perform its limited
function.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>C. Aggregated, Anonymous Information.</span></b><span
                style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We may share aggregated Anonymous
Information about you with third parties. Aggregated information is your
Anonymous Information that is combined with the Anonymous Information of other
users.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>D. Contests, sweepstakes and polls.</span></b><span
                style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>Some or all of the information collected
during a promotions, contest, polls, or sweepstakes (�Promotions�) may be
disclosed publicly. It may also be shared with other third parties as disclosed
at the time of collection or in the Promotion rules. These third parties may
include providers of prizes in order to update you of your status of your prize
or other fulfillment needs or a co-sponsor who may use your Personal
Information to administer the Promotion or for their marketing purposes, but only
in conformance with their privacy policy as made available to you at the time
of collection. <span class=SpellE>Eyecare</span> and <span class=SpellE>Amslerapp</span>
is not responsible for their privacy policy or practices.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>When you participate in a Promotion, you are
subject to any official rules for that Promotion, which may contain additional
information about the specific privacy practices associated with the Promotion.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>E. Long-Term-License Services.</span></b><span
                style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>This Policy does not cover the use of your
Personal Information by Licensors. The privacy practices of <span class=SpellE>Eyecare�s</span>
Licensors may differ from this policy and we encourage you to contact those
third parties directly if you have questions regarding their use of your
Personal Information.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>F. Sponsors, Merchants and other ways that
you approve at the time of collection.</span></b><span style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We may collect Personal Information about
you and share it with third parties to the extent that you give us permission
at the time of collection. For example, if you purchase a deal, product, or
service, we will provide your information to the applicable merchant or vendor.
We also sometimes gather Personal Information on behalf of a sponsor in
association with a promotion. In such a case, we will provide you notice at the
time of collection that the information is being collected on the sponsor's behalf.
We do not control and are not responsible for how these third parties use your
information.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular"'>G. Within our Company; Changes to our
Company.</span></b><span style='font-family:"Lato Regular"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We may share your Personal Information with
any of our subsidiaries, joint ventures, or other companies under common
control. Additionally, in the event we go through a business transition such as
a merger, acquisition by another company, or sale of all or a portion of our
assets, your Personal Information may be among the assets transferred. You
acknowledge that such transfers may occur and are permitted by this Policy.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>5. Message Boards and
Chats<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We may make message boards, chat rooms, and
other interactive forums available as part of the Services. You should be aware
that any information which you post to these interactive forums or otherwise
choose to make publicly available, including your Personal Information, may be
disclosed and available to all users who have access to that portion of the
Site or Services. By using these interactive forums, you agree that we are not
responsible for any information that you disclose or communicate in such
forums, and any disclosures you make are at your own risk.</span><span
                style='font-family:"Lato Regular";mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>6. Children's
Information<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>The Site and Services are not directed or
intended for children under 13 years of age. We do not knowingly collect
Personal Information from individuals under 13 years of age. If you are under
13 years of age, you should not register or provide Personal Information on the
Site or through the Services. If you are the parent or guardian of a child whom
you believe has disclosed Personal Information to us, please contact us at <a
                    href="mailto:admin@amslerapp.com">admin@amslerapp.com</a> so that we may delete
and remove such child's information from our systems.</span><span
                style='font-family:"Lato Regular";mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>7. Updating and Control
of Your Personal Information<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We offer you choices regarding the
collection, use, and sharing of your Personal Information. When you receive
promotional communications from us you will have the opportunity to
&quot;opt-out&quot; by following the unsubscribe instructions provided in the
promotional e-mail or newsletter you receive or by editing your preference to
receive these communications within the &quot;Manage My Email&quot; or similar
feature on the individual Sites when you are logged in as a registered user.&nbsp;</span><span
                style='font-family:"Lato Regular";mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>You can update or correct your Personal
Information by logging in and clicking on your user name at the top of the Site
to access the &quot;Profile&quot; section. If you no longer wish <span
                    class=SpellE>Eyecare</span> App to use or share your Personal Information in
accordance with this Policy or you no longer desire to use or access this Site,
you can also remove your Personal Information from the &quot;Profile&quot;
section of the Site.&nbsp;If you would like assistance disabling your account,
please contact customer service at <a href="mailto:admin@amslerapp.com">admin@amslerapp.com</a>
.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>Keep in mind, however, that even if you
delete information from your profile, we may retain your Personal Information
in conformance with our data retention policy and Personal Information may
remain within our databases, access logs, and other records. In addition, we
are not responsible for updating or removing your Personal Information
contained in the lists or databases of third parties who have been provided
information as permitted by this Policy.<o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>8. Third Party Links<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>The Sites may link to websites operated by
third parties that we do not control. We do not monitor, control, or endorse
the information collection or privacy practices of any third parties. We
encourage you to become familiar with the privacy practices of every website
you visit and to contact them if you have any questions about their respective
privacy policies and practices. This Policy applies solely to information
collected by us through the Sites or Services and does not apply to these third
party websites. The ability to access information of third parties from the
Sites or Services, or links to other websites or locations, is for your
convenience only and does not signify our endorsement of such third parties,
their products, services, websites, locations or their content.</span><span
                style='font-family:"Lato Regular";mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>9. Security<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We place a priority on the security of
Personal Information, and we undertake reasonable security measures to help
protect the data that resides on our servers. We also use secure server
software (SSL) to process all financial transactions that occur on the Site.
SSL technology can encrypt Personal Information transmitted over the Internet.
However, no security system is impenetrable. We do not warrant the security of
our servers, nor do we warrant that your information, including Personal
Information, will be completely secure or not be intercepted while being
transmitted over the Internet.</span><span style='font-family:"Lato Regular";
mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>10. How To Contact Us<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>If you have any questions regarding privacy
or this Policy, you may contact us as follows:</span><span style='font-family:
"Lato Regular";mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>Telephone: <a
                    href="file://localhost/tel/847-294-0080">847-294-0080</a><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>Email: <a href="mailto:admin@amslerapp.com">admin@amslerapp.com</a><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
mso-outline-level:3'><b><span style='font-family:"Lato Regular";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>11. Privacy Policy
Changes<o:p></o:p></span></b></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular"'>We reserve the right to change, modify, add
or remove portions of this Policy at any time and without prior notice, and any
changes will become effective immediately upon being posted unless we advise
you otherwise. Your continued use of the Site or Services after this Policy has
been amended shall be deemed to be your continued acceptance of the terms and
conditions of the Policy, as amended. We encourage you to bookmark this Web
page and review this Policy regularly.</span><span style='font-family:"Lato Regular";
mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span
                    style='font-family:"Lato Regular";mso-bidi-font-family:"Times New Roman"'>12.
Personal Health Information</span></b><span style='font-family:"Lato Regular";
mso-bidi-font-family:"Times New Roman"'><o:p></o:p></span></p>

    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span
                style='font-family:"Lato Regular";mso-bidi-font-family:"Times New Roman"'>We
place a priority on ensuring the health and well being of your eye and visual
functions are cared for. However, by using this application and agreeing to its
terms and conditions you agree to release those in association with the <span
                    class=SpellE>Amslerapp</span> website and <span class=SpellE>Eyecare</span>
application of any harm that may come to the eye or visual function as a result
of using the mobile application. Additionally you agree that we can use your
personal health information as used in this application for your benefit and
that we will not use it to divulge your information to any third parties or
other vendors. However, your agreement in the terms and conditions stipulate
that you release any liability for us if there is any delay in care for your
general health or eye care health as a result of using this program. The use of
the <span class=SpellE>eyecare</span> application is strictly observatory and
for your own record keeping and we take no responsibility in ensuring you have
the appropriate follow up needed to care for your eyes as deemed medically
necessary. All personal health information will be protected but you release
the <span class=SpellE>Eycare</span> or <span class=SpellE>amslerapp</span>
website users from any liability if there is any compromised of personal health
information collected through the use of the application or website.<o:p></o:p></span></p>

    <p class=MsoNormal><span style='font-family:"Lato Regular"'><o:p>&nbsp;</o:p></span></p>

</div>

</body>

</html>
