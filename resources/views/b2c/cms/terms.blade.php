@include('b2c/partial/head')
<html>

<head>
    <meta name=Title content="">
    <meta name=Keywords content="">
    <meta http-equiv=Content-Type content="text/html; charset=macintosh">
    <meta name=Generator content="Microsoft Word 14 (filtered)">
    <style>
        body {
            background-color: white !important;
            padding: 30px;
        }

        <!--
        /* Font Definitions */
        @font-face {
            font-family: "\FF2D\FF33 \660E\671D";
            panose-1: 0 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "\FF2D\FF33 \660E\671D";
            panose-1: 0 0 0 0 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Lato Regular";
            panose-1: 2 15 5 2 2 2 4 3 2 3;
        }

        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            font-family: Cambria;
        }

        p.MsoPlainText, li.MsoPlainText, div.MsoPlainText {
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 10.5pt;
            font-family: Courier;
        }

        span.PlainTextChar {
            font-family: Courier;
        }

        .MsoChpDefault {
            font-size: 10.0pt;
            font-family: Cambria;
        }

        @page WordSection1 {
            size: 595.0pt 842.0pt;
            margin: 72.0pt 45.45pt 72.0pt 45.45pt;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        -->
    </style>

</head>

<body bgcolor=white lang=EN-US>

<div class=WordSection1>

    <p class=MsoPlainText style='text-align:justify;text-justify:inter-ideograph'><span
                style='font-size:14.0pt;font-family:"Lato Regular"'>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>TERMS OF USE</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-size:19px;font-family:"Lato Regular",serif;color:black;background:floralwhite;'>This Agreement provides the agreed-to terms and conditions of use for this Amsler App (&quot;hereinafter App&quot;):</span><span
                style='font-size:19px;font-family:"Lato Regular",serif;color:black;'><br> <span
                    style="background:floralwhite;">As used herein &quot;Provider&quot; means Robert Garoon, Ira Garoon, their affiliates, professional corporations, subsidiaries, successors, agents, employees, and assignees. As used herein, &quot;User&quot; means any person or entity who makes use of this App, including individual using the Amsler grid application to make tracings or a physician/healthcare-provider who is reviewing the grid tracings. You the User, as a condition precedent to your making any use of this App, hereby agree to all of the terms and conditions stated herein, and you also specifically agree to the disclaimer(s) stated herein and/or implied as a matter of law. User hereby acknowledges and agrees that the use of this App and its content does not constitute or imply the providing of professional medical or other health care advice or service, and is not to be used to self-diagnose or treat a medical or eye condition. This App is intended for educational/information purposes only. This App&#39;s link to other sites, including the American Academy of Ophthalmology, the practice of Dr. Ira Garoon and Dr. Robert Garoon, and other medical practices, does not constitute an assigned or approved referral or consultation request, and dos not constitute User&#39;s entry into a doctor/patient relationship with Provider or other User. It is hereby agreed that Providers shall not be liable or responsible for any content, accuracy or opinions expressed. At no time are Providers obligated to provide Users medical consultation or follow up, or provide office appointment or emergency or hospital care or surgery for any request through this App. It is further agreed that Providers are not liable or responsible for any failure of App from software, hardware or update in its content including inaccuracies, typographical errors, viruses, spyware or malware. The User acknowledges, agrees and understands&nbsp;that treatment and/or therapy for any medical disease or condition and, in particular, Macular Degeneration, is constantly evolving and must start from an in-person medical examination by a qualified doctor, and so any content in this App referring to such diagnostic or treatment plans is intended for discussion and informational purposes only. Providers do not warrant, state or imply that this App is updated to reflect the ever-evolving changes in medicine to meet the User&#39;s general or medical condition or needs. The Provider has no obligation to provide updates, maintenance or support. The Provider does not state, warrant, or guarantee that this App will be free of viruses. User hereby expressly agrees that under no circumstances will Provider be liable to User for any medical, legal, financial, business, technical or reputation damages or losses including: litigation, legal fees, punitive damages, or charges incurred. If you the User do not agree to all the terms and conditions of this Agreement and its limitations of liability, the Provider does not agree to or authorize your use of this App for any purpose whether on your device or another device not owned by you the User. The unauthorized use or display of this App or any of its content or materials is explicitly prohibited. All title, right, and interest in the&nbsp;content, source code, and intellectual property right is owned and controlled by Provider. User hereby agrees that User shall not use this App in any way that might or could be harmful or injurious to the Provider including the Provider&#39;s reputation, goodwill, and business interests, or that might or could impair, disable, damage, or modify this App or the Provider&#39;s medical practice, website, or business. The User hereby specifically agrees not to use this App for any unlawful purpose, including hacking, data-mining, reverse engineering or any attempt to obtain this App&#39;s source code. The User also acknowledges, and agrees that the content, source code, methodology and mode of practice of this App and Provider&#39;s business and medical practice are proprietary, and highly confidential. The Provider reserves the right to consider any such violation or attempt a breach of this Agreement. You the User agree to indemnify, and hold harmless the Provider, affiliates, agents, successors, and assignees from any and all claims, actions, suits, proceedings, costs, expenses, damages, liabilities, and attorney fees arising from or in any way related to your use of this App&#39;s information obtained through this App. It is hereby agreed that this Agreement shall be construed and interpreted under and governed by the laws of the state of Illinois and the United States of America. By your use of this App, you hereby agree that the forum for any dispute arising out of or in connection with your use of this App shall be in Cook County, Illinois&#39; state courts in Chicago, IL. In addition, by your use of this App, you agree that Illinois&#39; State Court shall have both subject matter and personal jurisdiction over any such dispute, and you the User, and User hereby waives any challenges to forum jurisdiction in Chicago IL., and hereby accepts venue there for any such actions. User agrees to pay all Providers expenses associated with any litigation arising from or in connection with User&#39;s use of this App, including Provider&#39;s attorney&#39;s fees and court fees as well as loss of business income.</span></span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>I. PRIVACY</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>By using the Services, you consent to our processing your information consistent with AmslerApp Eyecare policy privacy.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>II. IMPORTANT INFORMATION ABOUT YOUR USE OF THE SERVICES&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Mobile Terms and Conditions</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Effective Date: 08/14/2020</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>DO NOT USE THIS SITE FOR EMERGENCY MEDICAL NEEDS. If you experience a medical emergency, call 911 immediately.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>YOUR USE OF EYECARE AMSLER APP</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>You acknowledge that you are consenting to receiving care via Eyecare Amsler App. The scope of care will be at the sole discretion of the health services-provider who is assisting you, if you so elect to that relationship, with no guarantee of diagnosis, treatment, or prescription. The health services-provider will determine whether or not the condition being diagnosed and/or treated is appropriate for a telehealth encounter and/or needs additional evaluation. &nbsp; You understand and agree that your interaction with health services-providers using Eyecare Amsler App is not necessarily intended to take the place of appointments with your regular ophthalmic care-provider unless judged to be appropriate by a physician that the User has entered into a relationship with previously. Do not disregard medical advice from your regular doctor or other health professional because of information provided by a health services-provider via Eyecare Amsler App.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Amsler App- Eyecare is a technology platform and, as such, is not licensed to practice medicine and does not render medical care or medical advice.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>All services or recommendations made by a health services-provider are based on their independent professional judgment.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;INFORMATION ABOUT HEALTH SERVICES PROVIDERS</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>When you choose to use Eyecare Amsler App you will be provided with the opportunity to choose your own healthcare-provider or may elect to use the App without the supervision of your own practitioner. The information we make available about any particular health services-provider is supplied to us by that professional or online references, and to the best of our knowledge it is accurate, however, we make no warranty as to its accuracy. You are ultimately responsible for choosing which health services-provider to communicate with.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Health services-providers are licensed by applicable state medical boards. &nbsp;For more information about a health services-provider&#39;s licensure, you can contact the medical board in your state.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>INFORMED CONSENT FOR SERVICES PERFORMED BY HEALTH SERVICES-PROVIDER&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We are providing this information on behalf of the health services-provider:</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The delivery of health care through services using communication tools such as Eyecare Amsler App is commonly called &quot;Telemedicine.&quot; &nbsp;Telemedicine involves the use of electronic communications to enable health care providers at sites remote from users to provide consultative services. &nbsp;The information may be used for diagnosis, therapy, follow-up and/or education, and may include live two-way audio and video and other materials (e.g. medical records, data from medical devices).</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The communications systems used will incorporate network and software security protocols to protect the confidentiality of patient information and will include reasonable measures to safeguard the data and to ensure its integrity against intentional or unintentional corruption.&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;Anticipated Benefits of Telemedicine:&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&bull; Improved access to care by enabling a patient to remain at his or her home or office while consulting a clinician.&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&bull; More efficient medical evaluation and management.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Possible Risks of Telemedicine:&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>As with any medical procedure, there are potential risks associated with the use of telemedicine. &nbsp;These risks may include, without limitation, the following:&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&bull; Delays in medical evaluation and consultation or treatment may occur due to deficiencies or failures of the equipment.&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&bull; Security protocols could fail, causing a breach of privacy of personal medical information.&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&bull; Lack of access to complete medical records may result in adverse drug interactions or allergic reactions or other negative outcomes.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>By agreeing to these Terms of Use and by using Eyecare Amsler App, you acknowledge that you understand and agree with the following:</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>1. &nbsp; The laws that protect privacy and the confidentiality of medical information also apply to telemedicine.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>2. &nbsp; Telemedicine may involve electronic communication of your personal medical information to health service providers who may be located in other areas, including out of state.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>3. &nbsp; You may expect the anticipated benefits from the use of telemedicine in your care, but that no results can be guaranteed or assured.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>4. &nbsp; Your healthcare information may be shared with other individuals for treatment, payment and healthcare operations purposes. Billing codes and visit summaries are shared with others and with you. &nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>5. &nbsp; Your healthcare information may be further shared in the following circumstances:</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp; &nbsp; &nbsp;a) When a valid court order is issued for medical records.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp; &nbsp; &nbsp;b) Reporting suspected abuse, neglect, or domestic violence.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp; &nbsp; &nbsp;c) &nbsp;Preventing or reducing a serious threat to anyone&#39;s health or safety.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Patient Consent to the Use of Telemedicine&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Having read and understood the information provided above, and understanding the risks and benefits of telemedicine, by agreeing to these Terms of Use you hereby give your informed consent to participate in a telemedicine visit under the terms described herein.&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>CHARGES FOR SERVICES</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>You understand and agree that you are responsible for all charges relating to your use of Eyecare Amsler App.&nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>You will be informed of the fee to be charged when you sign up for using the services of the Application. Fees may vary and you will be notified of fee changes by new invoices or direct notification by the App. All charges are final but charges may be protested and undergo a review process to verify the authenticity of the claims in a process that is decided by the Provider. Your personal practitioner may deem it fitting to charge you for services rendered by reviewing images that you have sent for review. You will be responsible for all charges related to your online visit that are not covered by your insurance or to individual. &nbsp;As a rendering practitioner, you authorize Eyecare Amsler App to charge your credit card for the opportunity to review Users records and Amsler grid tracings. &nbsp;You will be asked to supply credit card information, which will be verified prior to your ability to review users&rsquo; grid tracings. A practitioner will not be able to use Eyecare Amsler App to communicate with a user if the credit card information you provide is inaccurate or if your credit card is declined.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>By initiating an online account, you authorize us to charge your credit card for any charges that are your responsibility.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>In order to facilitate payment for your online visit, we will share your credit card information and related personal information with the designated credit card payment processor. This information is shared solely for the purpose of collecting the fee.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>INSURANCE BENEFITS</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>You may, at your sole discretion, elect to choose a practitioner to review your amsler grid tracings with whom you may already have provided with your health insurance information. You must provide your insurance information to this individual in order to receive additional care in monitoring the grids. &nbsp;This information will be used to check your eligibility and submit a claim on your behalf. Your copay and/or deductible will be estimated based on the response from your health plan.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The actual payment by your health plan may be more or less than the estimated amount. The App takes no responsibility for the User being charged by a practitioner they selected to review their images and all questions, grievances, concerns about billing, insurance claims and other concerns about billing and/or insurance issues should be directed at the individual user/physician requesting payment.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>If your health plan has entered into a contract with your selected health services-provider to reimburse the professional for services, the amount your insurer has agreed to reimburse the provider will be reflected in the fee that you are charged. You are encouraged to contact your health plan to see if the services offered through Eyecare Amsler App are covered prior to using the App.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>PLEASE NOTE that in order to take advantage of any insurance benefits your insurer may offer you must create an Eyecare Amsler App account to enter into an agreement with a physician User. Users who have not created an account will not have their images reviewed.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>PASSWORDS AND ACCOUNT</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Your username and password (&quot;Log-In ID&quot;) will allow you to access Eyecare Amsler App. You are solely responsible for controlling your Log-In ID, and are prohibited from making it available to anyone else. You are responsible for all activities that occur under your Log-In ID. You agree to notify Eyecare Amsler App Customer Support immediately of any unauthorized use of your Log-In ID or of any need to reset or lock down the Log-In ID associated with your account.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>YOUR COMMUNICATIONS</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>You are responsible for your own communications using the App. You may not:</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; communicate material that is obscene, defamatory, threatening, harassing, abusive, hateful, or embarrassing to any person or entity;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; communicate a sexually-explicit image;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; communicate chain letters or pyramid schemes;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; impersonate another person;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; violate the Children&#39;s On Line Privacy Protection Act;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; communicate material that is copyrighted, without the permission of the copyright owner;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; communicate material that reveals trade secrets, unless you have permission of the owner;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; communicate material that infringes on any other intellectual property rights of others or on the privacy or publicity rights of others.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>TERMINATION</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Eyecare Amsler App may terminate your use this site at any time. The App reserves the right to block, delete or stop the uploading of materials and communications that it, in its sole discretion, finds unacceptable for any reason. If your use of this site is terminated you shall make no further use of Eyecare Amsler App or any information obtained from it. If you find that you have been terminated, you may contact Eyecare Amsler App Customer Support at&nbsp;</span><a
                href="mailto:admin@amslerapp.com"><span
                    style='font-family:"Helvetica",sans-serif;'>admin@amslerapp.com</span></a><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;or eyecareapp@gmail.com for further information.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>APPROPRIATENESS OF CONTENT</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The App is not intended for independent use by children under the age of 18. Children under 18 must be enrolled on the site under their parent or guardian&#39;s account as a dependent. Parents or guardians are solely responsible for being present with their minor children when using the App.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>NO LIABILITY FOR COMPUTERS AND NETWORKS USED TO ACCESS YOUR ACCOUNT</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We are only responsible for the security of the computer systems we own and operate. The App shall have no liability for information about you stored or recorded by any computer or mobile device or any network, whether public or private, that you may use to access the App</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>ACCEPTABLE USE</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The Eyecare Amsler App website and application available for the sole purpose of facilitating communications between health services-providers and consumers who chose to use the online service. &nbsp;You agree not to access or use the App for an unlawful or illegitimate purpose. You shall not attempt to disrupt the operation of the services or the App&rsquo;s system. You shall not attempt to gain unauthorized access to any user accounts or computer systems or networks.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>OPERATION AND RECORD RETENTION</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Eyecare Amsler App reserves complete and sole discretion with respect to the operation of the App. The App may withdraw, suspend or discontinue any functionality or feature of the services. The App reserves the right to maintain, delete or destroy all communications and materials posted or uploaded to the online system pursuant to its internal record retention and/or destruction policies.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>INTELLECTUAL PROPERTY</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>All of the content available on or through the Eyecare Amsler Application is the property of the App or their licensors, and is protected by copyright, trademark, patent, trade secret and other intellectual property laws. We give you permission to display, download, store, and print the content only for your personal, non-commercial use. You agree not to reproduce, retransmit, distribute, disseminate, sell, publish, broadcast, or circulate the content received using the App to anyone. All software and accompanying documentation made available for download Eyecare Amsler App is the copyrighted work of the App or their licensors.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>All Eyecare Amsler App trade and service names are trademarks of Eyecare, Inc. All other brands and names, including &quot;Eyecare,&quot; are the property of their respective owners. Nothing contained on the App should be construed as granting any license or right to use any trademark displayed on this site without the express written permission of the App, or any other party that may own the trademark.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Subject to these terms, the App hereby grants you a limited, revocable, non-transferable and non-exclusive license to use the software, network facilities, content and documentation on and in Eyecare Amsler App to the extent, and only to the extent, necessary to access and use the App for your personal use.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>DIGITAL MILLENIUM COPYRIGHT ACT</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The App respects the intellectual property of others and expects its users to do the same. If you believe that your copyrighted work has been copied in a way that constitutes copyright infringement and is accessible on this site or through this service, you must provide the following information when providing notice of the claimed infringement to the App:</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; A physical or electronic signature of a person authorized to act on behalf of the copyright owner and identification of the copyrighted work that is infringed;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Information reasonably sufficient to permit the App to contact you, such as an address, telephone number and/or electronic mail address;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; A statement that you have a good faith belief that the use of the material in the manner complained of is not authorized by the copyright owner, its agent or law;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; A statement that the information in the notification is accurate and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The above information must be submitted as a written notification to the App through The App Customer Service at eyecareapp@gmail.com or by writing us at 9301 Golf Road, Des Plaines, IL 60611, ATTENTION: LEGAL DEPARTMENT COMPLAINT. This information should not be construed as legal advice. For further details on the information required for valid DMCA notifications, see 17 U.S.C. 512(c)(3).</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>LIABILITY OF THE APP AND ITS LICENSORS AND SUPPLIERS</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>No Liability. THE APP AND ITS LICENSORS AND SUPPLIERS (INCLUDING, FOR THE PURPOSES OF THIS ENTIRE SECTION, ALL PROVIDERS OF CONTENT FOR EYECARE AMSLER APP) SHALL NOT BE LIABLE TO YOU, UNDER ANY CIRCUMSTANCES OR UNDER ANY THEORY OF LIABILITY OR INDEMNITY, FOR ANY DAMAGES OR PENALTIES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, INCIDENTAL INDIRECT, EXEMPLARY, PUNITIVE AND CONSEQUENTIAL DAMAGES, LOST PROFITS, OR DAMAGES RESULTING FROM LOST DATA OR BUSINESS INTERRUPTION) IN CONNECTION WITH THE USE OR INABILITY TO USE EYECARE AMSLER APP OR THE CONTENT, EVEN IF ANY OF THEM HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE APP SHALL BE LIABLE TO YOU ONLY TO THE EXTENT OF ACTUAL DAMAGES INCURRED BY YOU. THE REMEDIES STATED FOR YOU IN THESE TERMS AND CONDITIONS ARE EXCLUSIVE AND ARE LIMITED TO THOSE EXPRESSLY PROVIDED FOR IN THESE TERMS AND CONDITIONS.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The App and its licensors and suppliers are not responsible for any claims you may have against any medical professionals, suppliers of products or other persons, institutions or entities identified in whole or in part through Eyecare Amsler App.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>No Warranties. Eyecare Amsler App, AND ITS CONTENT AND INFORMATION ARE PROVIDED &quot;AS IS.&quot; THE APP, ITS LICENSORS AND SUPPLIERS DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT OF THIRD PARTIES RIGHTS OR FITNESS FOR A PARTICULAR PURPOSE.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>THE APP, ITS LICENSORS AND SUPPLIERS MAKE NO REPRESENTATIONS OR WARRANTIES THAT THE INFORMATION PROVIDED ON THE APP IS COMPLETE, EXHAUSTIVE, RELIABLE, CURRENT OR ACCURATE. NO CLAIMS OR ENDORSEMENTS ARE MADE FOR ANY DRUG, HERB, SUPPLEMENT, COMPOUND, THERAPY, OR TREATMENT.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>INDEMNIFICATION</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Without limiting the generality or effect of other provisions of these terms, as a condition of use, you agree to indemnify and hold harmless the App and its licensors and suppliers and their parents, subsidiaries, affiliates, suppliers and their officers, directors, affiliates, subcontractors, agents and employees (collectively, &quot;Indemnified Parties&quot; and each, individually, an &quot;Indemnified Party&quot;) against all costs, expenses, liabilities and damages (including attorney&#39;s fees) incurred by any Indemnified Party in connection with any third party claims arising out of: (i) your use of the App and/or your receipt of services; (ii) your failure to comply with any applicable laws and regulations; and (iii) your breach of any of your obligations set forth here. You shall not settle any such claim without the written consent of the Applicable Indemnified Party.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>CHANGES TO THIS NOTICE</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The App may revise, modify or amend the information contained on this page at any time. Any such revision, modification or amendment shall be effective immediately upon either posting it to this web site or otherwise notifying you.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>MISCELLANEOUS</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The App is based in Des Plaines, Illinois in the United States of America and makes no claims that the content and information included on the App is appropriate or may be downloaded outside of the United States. Access to the content and information may not be legal by certain persons or in certain countries. If you access the App from outside the United States, you do so at your own risk and are solely responsible for compliance with the laws of your jurisdiction and any other applicable laws.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>These terms of use shall be governed and construed in accordance with the laws of the State of Illinois without regard to the choice of law provisions of any jurisdiction. The App may without notice to you assign its rights and duties hereunder to any party at any time. Failure to enforce or insist on strict performance of any provision of these terms shall not be construed as a waiver of any provision or right. You agree that any legal action or proceeding between the App and you in any way related to these terms of use shall be brought exclusively in a court of a competent jurisdiction sitting in Chicago, Illinois. Any cause of action or claim you may have against or involving the App must be commenced within one year after the claim or cause of action arises. Neither the course of conduct between the parties nor trade practice shall modify these terms. The invalidity or unenforceability of any provision shall not in any way affect the validity or enforceability of the rest of these terms.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>NOTICES</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>To the extent that the law and/or any applicable regulation allows for the provision of notice electronically, your use of the App constitutes your agreement to receive all such notices electronically, including but not limited to notices required by state or federal privacy laws, and all financial information pertinent to, or required in connection with the operation of the App.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>QUESTIONS</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>If you have questions or comments or if you believe that your confidentiality has been breached or that any of your communications have been intercepted, or you wish to notify us regarding a suspected violation of these terms, please contact Eyecare Amsler App Support by email at&nbsp;</span><a
                href="mailto:eyecareapp@gmail.com"><span style='font-family:"Helvetica",sans-serif;'>eyecareapp@gmail.com</span></a><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;immediately.</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>PRIVACY POLICY</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>The Eyecare Amsler App (&quot;Eycare,&quot; &quot;we,&quot; &quot;us,&quot; &ldquo;the App,&rdquo; or &quot;our&quot;) recognizes the importance of protecting the privacy of your personal information, and we have prepared this Privacy Policy to provide you with important information about the privacy practices applicable to the App and any website or service that links to or refers to this Privacy Policy (collectively, the &quot;Services&quot;). &nbsp;In addition, individually identifiable information that you provide to us for purposes of obtaining medical care from Online Care Group (&quot;OCG&quot;) and other providers on the App platform. Such information is also referred to as &quot;Protected Health Information&quot; or &quot;PHI&quot;) will also be subject to OCG&#39;s Health Insurance Portability and Accountability Act Notice of Privacy Practices (the &quot;HIPAA Notice&quot;), which is available here. &nbsp;The HIPAA Notice describes how OCG and we can use and share your PHI and also describes your rights with respect to your PHI. &nbsp;To the extent that there is a conflict between this Privacy Policy and the HIPAA Notice with respect to PHI, the HIPAA Notice will prevail. &nbsp;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;I. COLLECTION OF INFORMATION</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We may collect the following kinds of information when you use the Services:</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Information you provide directly to us. For certain activities, such as when you register, use our telemedicine services, subscribe to our alerts, or contact us directly, we may collect some or all of the following types of information:</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Contact information, such as your full name, email address, mobile phone number, and address;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Username and password;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Payment information, such as your credit card number, expiration date, and credit card security code;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; For health care providers, information about your employment, such as your job title, practice area, primary specialty, and medical license status, gender, date of birth, languages spoken, educational background, address, photograph, social security number, Tax ID, NPI number, professional license information and bank account information;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Personal health information, including information about your diagnosis, previous treatments, general health, and health insurance; and</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Any other information you provide to us.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We may combine such information with information we already have about you.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Information we collect automatically. We may collect certain information automatically when you use our Services, such as your Internet protocol (IP) address, device and advertising identifiers, browser type, operating system, Internet service provider, pages that you visit before and after using the Services, the date and time of your visit, information about the links you click and pages you view within the Services, and other standard server log information. &nbsp;We may also collect certain location information when you use our Services, such as your computer&#39;s IP address, your mobile device&#39;s GPS signal, or information about nearby WiFi access points and cell towers.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We may use cookies, pixel tags, Local Shared Objects, and similar technologies to automatically collect this information. &nbsp;Cookies are small bits of information that are stored by your computer&#39;s web browser. &nbsp;Pixel tags are very small images or small pieces of data embedded in images, also known as &quot;web beacons&quot; or &quot;clear GIFs,&quot; that can recognize cookies, the time and date a page is viewed, a description of the page where the pixel tag is placed, and similar information from your computer or device. &nbsp;Local Shared Objects (sometimes referred to as &quot;Cookies&quot;) are similar to standard cookies except that they can be larger and are downloaded to a computer or mobile device. &nbsp;By using the Services, you consent to our use of cookies and similar technologies.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We may also collect technical data to address and fix technical problems and improve our Services, including the memory state of your device when a system or app crash occurs while using our Services. &nbsp;Your device or browser settings may permit you to control the collection of this technical data. &nbsp;This data may include parts of a document you were using when a problem occurred, or the contents of your communications. &nbsp;By using the Services, you are consenting to the collection of this technical data.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Information we obtain from your health care providers and other sources. In connection with your use of the App, we may collect medical records from your past, current, and future health care-providers. &nbsp;This may include information about your diagnosis, previous treatments, general health, laboratory and pathology test results and reports, social histories, any family history of illness, and records about phone calls and emails related to your illness.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We may also receive information about you from other sources, including through third-party services and organizations. &nbsp;For example, if you access third-party services, such as Facebook, Google, or Twitter, through the Services to login to the Services or to share information about your experience on the Services with others, we may collect information from these third-party services.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>II. USE OF INFORMATION</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We generally use the information we collect online to:</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Provide and improve the Services;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Contact you;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Fulfill your requests for products, services, and information;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Send you information about optional surveys regarding your experience, additional clinical services and/or general wellness from us or on behalf of our affiliates and trusted third-party partners using certain information you provide to us and your use of the Service;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Note: Your participation is voluntary and your treatment, payment and health care enrollment is not conditioned upon your participation. &nbsp;If you do not wish to receive these outreach in the future please contact us as noted below.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Analyze the use of the Services and user data to understand and improve the Services;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Customize the content you see when you use the Services;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Conduct research using your information, which may be subject to your separate written authorization;</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Prevent potentially prohibited or illegal activities and otherwise in accordance with our Terms of Use (which can be found at&nbsp;</span><a
                href="https://startlivehealthonline.com/landing.htm" target="_blank"><span
                    style='font-family:"Helvetica",sans-serif;color:black;text-decoration:none;'>https://startlivehealthonline.com/landing.htm</span></a><span
                style='font-family:"Helvetica",sans-serif;color:black;'>); and</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; For any other purposes disclosed to you at the time we collect your information or pursuant to your consent.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>III. SHARING OF INFORMATION</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We are committed to maintaining your trust, and we want you to understand when and with whom we may share the information we collect.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Healthcare-providers, insurance companies, and other healthcare-related entities. We may share your information with other health care-providers, laboratories, government agencies, insurance companies, organ procurement organizations, or medical examiners. and other entities relevant to providing you with treatment options and support.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Authorized third-party vendors and service providers. We may share your information with third-party vendors and service-providers that help us with specialized services, including billing, payment processing, customer service, email deployment, business analytics, marketing (including but not limited to advertising, attribution, deep-linking, mobile marketing, optimization and retargeting) advertising, performance monitoring, hosting, and data processing. These third-party vendors and service providers may not use your information for purposes other than those related to the services they are providing to us.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Research partners. We may share your information with our research partners to conduct health-related research; such sharing may be subject to your separate written authorization.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Corporate affiliates. We may share your information with our corporate affiliates that are subject to this policy.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Business transfers. We may share your information in connection with a substantial corporate transaction, such as the sale of a website, a merger, consolidation, asset sale, or in the unlikely event of bankruptcy.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; Legal purposes. We may disclose information to respond to subpoenas, court orders, legal process, law enforcement requests, legal claims or government inquiries, and to protect and defend the rights, interests, health, safety, and security of Eyecare, Inc., our affiliates, users, or the public. If we are legally compelled to disclose information about you to a third party, we will attempt to notify you by sending an email to the email address in our records unless doing so would violate the law or unless you have not provided your email address to us.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&middot; &nbsp; &nbsp; &nbsp; &nbsp; With your consent or at your direction. We may share information for any other purposes disclosed to you at the time we collect the information or pursuant to your consent or direction.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>If you access third-party services, such as Facebook, Google, or Twitter, through the Services to login to the Services or to share information about your experience on the Services with others, these third-party services may be able to collect information about you, including information about your activity on the Site, and they may notify your connections on the third-party services about your use of the Site, in accordance with their own privacy policies.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>If you choose to engage in public activities on the Site or third party sites that we link to, you should be aware that any information you share there can be read, collected, or used by other users of these areas. &nbsp;You should use caution in disclosing personal information while participating in these areas. We are not responsible for the information you choose to submit in these public areas</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>IV. SECURITY</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We use reasonable measures to help protect information from loss, theft, misuse and unauthorized access, disclosure, alteration and destruction. &nbsp;You should understand that no data storage system or transmission of data over the Internet or any other public network can be guaranteed to be 100 percent secure. &nbsp;Please note that information collected by third parties may not have the same security protections as information you submit to us, and we are not responsible for protecting the security of such information.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>V. YOUR CHOICES</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>You may opt out of receiving general health and wellness or treatment options that may be relevant to you by emailing us at&nbsp;</span><a
                href="mailto:eyecareapp@gmail.com"><span style='font-family:"Helvetica",sans-serif;'>eyecareapp@gmail.com</span></a><span
                style='font-family:"Helvetica",sans-serif;color:black;'>. You may also request that we delete your personal information by sending us an email at&nbsp;</span><a
                href="mailto:eyecareapp@gmail.com"><span style='font-family:"Helvetica",sans-serif;'>eyecareapp@gmail.com</span></a><span
                style='font-family:"Helvetica",sans-serif;color:black;'>.</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>You may be able to refuse or disable cookies by adjusting your web browser settings. &nbsp;Because each web browser is different, please consult the instructions provided by your web browser (typically in the &quot;help&quot; section). &nbsp;Please note that you may need to take additional steps to refuse or disable Local Shared Objects and similar technologies. &nbsp;For example, Local Shared Objects can be controlled through the instructions on Adobe&#39;s Setting Manager page. &nbsp;If you choose to refuse, disable, or delete these technologies, some of the functionality of the Services may no longer be available to you.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>California residents are entitled once a year, free of charge, to request and obtain certain information regarding our disclosure, if any, of certain categories of personal information to third parties for their direct marketing purposes in the preceding calendar year. We do not share personal information with third parties for their own direct marketing purposes.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>VI. THIRD-PARTY ADVERTISING, LINKS, AND CONTENT</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Some of the Services may contain links to content maintained by third parties that we do not control. &nbsp;We allow third parties, including business partners, advertising networks, and other advertising service providers, to collect information about your online activities through cookies, pixels, local storage, and other technologies. These third parties may use this information to display advertisements on our Services and elsewhere online tailored to your interests, preferences, and characteristics. &nbsp;We are not responsible for the privacy practices of these third parties, and the information practices of these third parties are not covered by this Privacy Policy.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Some third parties collect information about users of our Services to provide interest-based advertising on our Services and elsewhere, including across browsers and devices. &nbsp;These third parties may use the information they collect on our Services to make predictions about your interests in order to provide you ads (from us and other companies) across the internet. &nbsp;Some of these third parties may participate in an industry organization that gives users the opportunity to opt out of receiving ads that are tailored based on your online activities. &nbsp;Due to differences between using apps and websites on mobile devices, you may need to take additional steps to disable targeted ad technologies in mobile apps. &nbsp;Many mobile devices allow you to opt out of targeted advertising for mobile apps using the settings within the mobile app or your mobile device. &nbsp;For more information, please check your mobile settings. &nbsp;You also may uninstall our apps using the standard uninstall process available on your mobile device or app marketplace.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>To opt out of interest-based advertising across browsers and devices from companies that participate in the Digital Advertising Alliance or Network Advertising Initiative opt-out programs, please visit their respective websites. &nbsp;You may also be able to opt out of interest-based advertising through the settings within the mobile app or your mobile device, but your opt-out choice may apply only to the browser or device you are using when you opt out, so you should opt out on each of your browsers and devices if you want to disable all cross-device linking for interest-based advertising. &nbsp;If you opt out, you will still receive ads but they may not be as relevant to you and your interests, and your experience on our Services may be degraded.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>Do-Not-Track Signals and Similar Mechanisms. Some web browsers transmit &quot;do-not-track&quot; signals to websites. Because of differences in how web browsers incorporate and activate this feature, it is not always clear whether users intend for these signals to be transmitted, or whether they even are aware of them. We currently do not take action in response to these signals.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>VII. CHILDREN</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We do not knowingly allow individuals under the age of 18 to create accounts that allow access to our Services.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>VIII. CHANGES TO THE PRIVACY POLICY</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>We may update this Privacy Policy from time to time. When we update the Privacy Policy, we will revise the &quot;Effective Date&quot; date above and post the new Privacy Policy. &nbsp;We recommend that you review the Privacy Policy each time you visit the Services to stay informed of our privacy practices.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>IX. QUESTIONS?</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>If you have any questions about this Privacy Policy or our practices, please email us at&nbsp;eyecareapp@gmail.com.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span style="color:black;">&nbsp;</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>GEOGRAPHIC RESTRICTIONS</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-size:18px;font-family:"Helvetica",sans-serif;color:black;'>I hereby certify that I am physically in my current state of practice, and agree to only interact with a provider licensed in that state through the App while I am present in that state. I acknowledge that my ability to access and use these services is conditional upon the truthfulness of the certifications I make at the time of accessing a provider and the Providers I access are relying upon this certification in order to interact with me.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-family:"Helvetica",sans-serif;color:black;'>AGE REQUIREMENTS</span></p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span
                style='font-size:18px;font-family:"Helvetica",sans-serif;color:black;'>I hereby certify that I am at least 18 years of age and am qualified under the laws of my state to make medical decisions on my own behalf. I acknowledge that my ability to access and use the App and information is conditional upon the truthfulness of my certification of age.</span>
    </p>
    <p style='margin:0in;font-size:16px;font-family:"Calibri",sans-serif;'><span style="color:black;">&nbsp;</span></p>


    </span></p>

    <p class=MsoPlainText><span style='font-size:14.0pt;font-family:"Lato Regular"'>&nbsp;</span></p>

</div>

</body>

</html>
