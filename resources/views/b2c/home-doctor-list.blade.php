<style>
    .titlehead {
        color: #0096d6;
        font-family: "Lato", Verdana, Geneva, sans-serif;
        font-size: 18px;
        line-height: 35px;
    }

    .doctitle {
        color: #0096d6;
        font-family: "Lato", Verdana, Geneva, sans-serif;
        font-size: 14px;
        font-weight: bold;
    }

    .speciality {
        color: #8c7777;
        font-family: "Lato", Verdana, Geneva, sans-serif;
        font-size: 12px;
    }

    .lg-btn-2 {
        background-color: #008000;
        border: medium none;
        color: #ffffff;
        font-family: "Lato", Verdana, Geneva, sans-serif;
        font-size: 12px;
        font-weight: normal;
        margin-top: -4px;
        padding: 5px 8px;
        text-decoration: none;
        border-radius: 4px;
    }
</style>
<table width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td class='titlehead'>Doctor List</td>
    </tr>

    <tr>
        <td>
            <table width="100%" class="table-border" border="0" cellspacing="10">
                @foreach ($doctors as $doctor)
                    <tr class="Hrnormal" onmouseover="this.className = 'Hrhover';"
                        onmouseout="this.className = 'Hrnormal';">
                        <td align="center" width="10%" style=" border-bottom: 3px solid #5DADF6;">
                            @if(empty($doctor['imageURL']) || ($doctor['imageURL'] == " "))
                                <a href="{{asset('admin/uploads/doctorlist/noImage.gif') }}" target="_blank">
                                    <img src="{{asset('admin/uploads/doctorlist/noImage.gif') }}" width="80"
                                         height="80"/>&nbsp;
                                </a>
                            @else
                                <a href="{{asset($doctor['imageURL'])}}" target="_blank">
                                    <img src="{{$doctor['imageURL']}}" width="80"
                                         height="80"/>&nbsp;
                                </a>
                            @endif

                        </td>
                        <td class="doctitle" width="90%" valign="top" style=" border-bottom: 3px solid #5DADF6;">
                            <table width="100%" border="0" cellspacing="2">
                                <tr>
                                    <td class="doctitle" width="70%">
                                        {{$doctor['doctorName']}}
                                    </td>
                                    <td style="text-align: right" width="30%">
                                        @if($doctor['website'])
                                            <a href="{{$doctor['website']}}" target="_blank"
                                               class="btn btn-success lg-btn-2 ">Visit Website</a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="speciality" colspan="2"><b>Speciality: </b>
                                        {{$doctor['speciality']}}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="speciality" colspan="2"><b>Address: </b>
                                        {{$doctor['address']}} <br/> {{$doctor['city']}}
                                        , {{$doctor['state']}} {{$doctor['zip']}}
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                @endforeach
                @if(!$doctors)
                    <tr>
                        <td colspan="2" class="error_msg" align="center">Record was not found</td>
                    </tr>
                @endif
            </table>
        </td>
    </tr>

    <tr>
        <td>&nbsp;</td>
    </tr>
</table>
