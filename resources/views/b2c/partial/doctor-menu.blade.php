<div id="wrapper">
    <div id="header">
        <div class="menu_left"></div>
        <ul id="nav">
            <li class="level1-a" style="width:130px; "><a href="/doctor"
                                                          style="color:#ffffff;text-align:center">
                    <i class="fa fa-home"></i>Home</a>
            </li>
            <li class="level1-a" style="width:130px; "><a href="{{URL::route('doctor-edit-profile-form')}}"
                                                          style="color:#ffffff;text-align:center">
                    <i class="fa fa-edit"></i>Edit Profile</a>
            </li>
            <li class="level1-a" style="width:150px; "><a href="{{URL::route('doctor-payment-cards')}}"
                                                          style="color:#ffffff;text-align:center">
                    <i class="fa fa-money"></i>Payment Cards</a>
            </li>
            <li class="level1-a" style="width:150px; "><a href="{{URL::route('doctor-eye-test-list')}}"
                                                          style="color:#ffffff;text-align:center">
                    <i class="fa fa-eye"></i> Manage Eye Test</a>
            </li>
            <li class="level1-a" style="width:150px; "><a href="{{URL::route('doctor-patients-list')}}"
                                                          style="color:#ffffff;text-align:center">
                    <i class="fa fa-user-circle"></i> Manage Patients</a>
            </li>
            <li class="level1-a" style="width:160px; "><a href="{{URL::route('doctor-change-password-form')}}"
                                                          style="color:#ffffff;text-align:center">
                    <i class="fa fa-key"></i> Change Password</a>
            </li>
            <li class="level1-a" style="width:118px; "></li>
        </ul>
        <div class="menu_right"></div>
    </div>
</div>
