<table width="1002" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top"
                        style="background-image: url('{{asset('admin/images/background-bg.jpg')}}'); repeat-x scroll 0 0 #EEEEEE;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="55%">&nbsp;<a href="/admin/home"><img
                                                src="{{asset('admin/images/logo.png')}}" alt="" border="0"
                                                height="120" width="120"/></a></td>
                                <td width="45%" valign="top" style=" padding-right:20px;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="right" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td><img src="{{asset('admin/images/top-link-left.png')}}"
                                                                 alt="" width="27"
                                                                 height="29"/></td>
                                                        <td class="toplink">
                                                            @if(Auth::user()->getAuthenticationType() == 'patient' )
                                                                <a href="/patient">
                                                                    Home
                                                                </a> |
                                                                <a href="{{URL::route('patient-logout')}}">
                                                                    Logout
                                                                </a>
                                                            @elseif(Auth::user()->getAuthenticationType() == 'doctor')
                                                                <a href="/doctor">
                                                                    Home
                                                                </a> |
                                                                <a href="{{URL::route('doctor-logout')}}">
                                                                    Logout
                                                                </a>
                                                            @endif
                                                        </td>
                                                        <td align="right"><img
                                                                    src="{{asset('admin/images/top-link-right.png')}}"
                                                                    alt=""
                                                                    width="29" height="29"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height:58px;"></td>
                                        </tr>
                                        <tr>
                                            <td class="abovenavigation" valign="bottom">Welcome
                                                @if(Auth::user()->getAuthenticationType() == 'patient' )
                                                    {{ Auth::user()->getFirstName() }} {{ Auth::user()->getLastName() }}
                                                @elseif(Auth::user()->getAuthenticationType() == 'doctor' )
                                                    {{ Auth::user()->getDoctorName() }}
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="pagebg">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    @if(Auth::user()->getAuthenticationType() == 'patient' )
                                        @include('b2c/partial/menu')
                                    @else
                                        @include('b2c/partial/doctor-menu')
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="padding-left:20px; padding-right:20px;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" style="padding-top:50px;">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="10" valign="top"><img
                                                                    src="{{asset('admin/images/data-area-1-1.png')}}"
                                                                    alt="" width="10" height="10"/>
                                                        </td>
                                                        <td valign="top" class="boxtopmid"><img
                                                                    src="{{asset('admin/images/data-area-1-2.png')}}"
                                                                    alt="" width="1"
                                                                    height="10"/></td>
                                                        <td width="10" align="right" valign="top"><img
                                                                    src="{{asset('admin/images/data-area-1-3.png')}}"
                                                                    alt="" width="10"
                                                                    height="10"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="boxvarleft">&nbsp;</td>