<div id="wrapper">
    <div id="header">
        <div class="menu_left"></div>
        <ul id="nav">
            <li class="level1-a" style="width:130px; ">
                <a href="/patient/"
                   style="color:#ffffff;text-align:center">
                    <i class="fa fa-home"></i>Home
                </a>
            </li>
            <li class="level1-a" style="width:150px; "><a href="{{URL::route('patient-edit-profile-form')}}"
                                                          style="color:#ffffff;text-align:center">
                    <i class="fa fa-edit"></i> Edit Profile</a>
            </li>
            <li class="level1-a" style="width:150px; "><a href="{{URL::route('patient-eye-test-list')}}"
                                                          style="color:#ffffff;text-align:center">
                    <i class="fa fa-eye"></i> Manage Eye Test</a>
            </li>
            <li class="level1-a" style="width:158px; "><a href="{{URL::route('patient-change-password-form')}}"
                                                          style="color:#ffffff;text-align:center">
                    <i class="fa fa-key"></i> Change Password</a>
            </li>
            <li class="level1-a" style="width:400px; "></li>
        </ul>
        <div class="menu_right"></div>
    </div>
</div>
