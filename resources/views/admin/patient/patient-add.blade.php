@extends('admin/admin-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        @if($action == 'add')
            {!!  Form::open(array('route' => 'patient-create','id'=>'frmPageDetails')) !!}
        @else
            {!!  Form::open(array('route' => ['patient-update',$patient->getId()],'method'=>'PUT','id'=>'frmPageDetails')) !!}
        @endif
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class='loginhead'>@if($action == 'add')  Add @else Edit @endif Patient Detail</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="redrequirdbig" colspan="2" align="right">* Indicates required field</td>
            </tr>
            <tr>
                <td align="left" colspan="2" style="padding-left:10px;"
                    class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }} </td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">First Name: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="firstName" id="firstname" class="validate[required]"
                               value="{{$patient->getFirstName()}}" size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Last Name: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="lastName" id="lastname" class="validate[required]"
                               value="{{$patient->getLastName()}}" size="50"/>
                    </label></td>
            </tr>

            <tr>
                <td class="fontcaption" width="20%">Email: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="email" id="email" class="validate[required,custom[email]]"
                               value="{{$patient->getEmail()}}" size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Password: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="password" name="password" id="password" class="validate[required]" maxlength="20"
                               size="50" value="{{$patient->getPassword()}}"/>
                    </label></td>
            </tr>

            <tr>
                <td class="fontcaption" width="20%">Address:</td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <textarea type="textarea" name="address" id="address" cols="45" rows="3"
                        >{{$patient->getAddress()}}</textarea>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">City:</td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="city" id="city" value="{{$patient->getCity()}}"
                               size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">State:</td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="state" id="state" value="{{$patient->getState()}}"
                               size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Zipcode: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="zip" id="zip" value="{{$patient->getZip()}}" class="validate[required]"
                               size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Phone: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="phone" id="phone" value="{{$patient->getPhone()}}"
                               class="validate[required]" size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">
                    Physician: <span class="redrequird">*</span>
                    <i class="fa fa-search" style=""></i>
                </td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <select id="doctorId" name="doctorId" style="width:250px;" class="validate[required]">
                            <option>**Select Doctor**</option>
                            @foreach ($doctors as $doctor)
                                @if($patient->getDoctor() and ($doctor->getId() == $patient->getDoctor()->getId()))
                                    <option value="{{$doctor->getId()}}" selected>
                                @else
                                    <option value="{{$doctor->getId()}}">
                                        @endif
                                        {{$doctor->getDoctorName()}} - ({{$doctor->getDoctorCode()}})
                                    </option>
                                    @endforeach
                        </select>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td>
                                            <input type="submit" name="Submit" value="Submit" class="buttonmidle"
                                                   onclick="beforeSubmit();"/>
                                        </td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td><input type="button" name="reset" value="Cancel" class="buttonmidle"
                                                   onclick="window.location.href='/admin/patient'"/></td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>

    {!! Form::close() !!}
@endsection
@push('scripts')
    <script>
        let pageForm = "frmPageDetails";

        function beforeSubmit() {
            $.validationEngine.beforeSubmit(pageForm);
        }
    </script>
@endpush