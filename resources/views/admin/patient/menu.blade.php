<div id="wrapper">
    <div id="header">
        <div class="menu_left"></div>
        <ul id="nav">
            <li class="level1-a" style="width:130px; "><a href="/admin/home" style="color:#ffffff;text-align:center">Home</a>
            </li>
            <li class="level1-a" style="width:150px; "><a href="#" style="color:#ffffff;text-align:center">Manage
                    Doctors</a>
                <ul>
                    <p class="sp_space"></p>
                    <li><a href="{{URL::route('doctor-add')}}">Add Doctor</a></li>
                    <li><a href="{{URL::route('doctor-list')}}">Doctor List</a></li>
                    <li><a href="{{URL::route('doctor-upload-sheet')}}">Upload Doctor Sheet</a></li>
                    <p class="sp_space"></p>
                    <li class="sp_box"></li>
                </ul>
            </li>
            <li class="level1-a" style="width:150px; "><a href="#" style="color:#ffffff;text-align:center">Manage
                    Patients</a>
                <ul>
                    <p class="sp_space"></p>
                    <li><a href="{{URL::route('patient-add')}}">Add Patient</a></li>
                    <li><a href="{{URL::route('patient-list')}}">Patient List</a></li>
                    <p class="sp_space"></p>
                    <li class="sp_box"></li>
                </ul>
            </li>
            <li class="level1-a" style="width:150px; "><a href="#" style="color:#ffffff;text-align:center">Manage Eye
                    Test</a>
                <ul>
                    <p class="sp_space"></p>
                    <li><a href="{{URL::route('eye-test-add')}}">Add Eye Test</a></li>
                    <li><a href="{{URL::route('eye-test-list')}}">Eye Test List</a></li>
                    <p class="sp_space"></p>
                    <li class="sp_box"></li>
                </ul>
            </li>
            <li class="level1-a" style="width:128px; "><a href="#" style="color:#ffffff;text-align:center">Settings</a>
                <ul>
                    <p class="sp_space"></p>
                    <li><a href="change-password.php">Change Password</a></li>
                    <li><a href="{{URL::route('admin-settings')}}">Settings</a></li>

                    <p class="sp_space"></p>
                    <li class="sp_box"></li>
                </ul>
            </li>
            <li class="level1-a" style="width:280px; "></li>
        </ul>
        <div class="menu_right"></div>
    </div>
</div>
