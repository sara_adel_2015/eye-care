@extends('admin/admin-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {!!  Form::open(array('route' => 'eye-test-create','files' => true ,'id'=> 'frmPageDetails')) !!}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class='loginhead'>Add Eyetest Detail</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="redrequirdbig" colspan="2" align="right">* Indicates required field</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td align="left" class="{{ Session::get('flash_type') }}">
                    {{ Session::get('flash_message') }}
                </td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">
                    Patient Name: <span class="redrequird">*</span>
                </td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input style="width: 330px;" id="patientId" name="patientId" placeholder="Search For Patients">
                        <i class="fa fa-search" style="margin-left: -17px"></i>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Type: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <select style="width: 330px" id="imageType" name="imageType" class="validate[required]">
                            <option value="leftimage">Left</option>
                            <option value="rightimage">Right</option>
                        </select>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Upload Iphone Image:</td>
            </tr>

            <tr>
                <td width="80%">
                    <label>
                        <input type="file" name="imageFile" id="imageFile" size="30"
                               value=""/>
                    </label>
                    <font class="comments">[Note: Upload only jpg, gif, png file type]</font> <br/>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td>
                                            <input type="submit" name="Submit" value="Submit" class="buttonmidle"
                                                   onclick="beforeSubmit(this);"/>
                                        </td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td><input type="button" name="reset" value="Cancel" class="buttonmidle"
                                                   onclick="window.location.href='/admin/doctor'"/></td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
    {!! Form::close() !!}
@endsection
@push('scripts')

    <script type="text/javascript" src="{{asset('admin/js/doctor-add-edit.js')}}"></script>
    <script>
        $(function () {
            csrf_token = "{{ csrf_token() }}";
            $("#patientId").autocomplete({
                source: "/admin/patient/autocomplete",
                select: function (event, ui) {
                    $("#patientId").val(ui.item.value)
                },
                classes: {
                    "ui-autocomplete": "highlight"
                }
            });

        });
    </script>


@endpush

@prepend('scripts')
    <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery-ui.min.css')}}">
    <script>
        let pageForm = "frmPageDetails";

        function beforeSubmit() {
            $.validationEngine.beforeSubmit(pageForm);
        }
    </script>
@endprepend