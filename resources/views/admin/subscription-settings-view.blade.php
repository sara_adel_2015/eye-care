@extends('admin/admin-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {!!  Form::open(array('route' => ['subscription-settings-update',$settings->getId()],'method'=>'PUT','id'=>'frmPageDetails')) !!}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class='loginhead'><i class="fa fa-money"></i>
                                Subscription Settings
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="redrequirdbig" colspan="2" align="right"></td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td align="left" colspan="2" style="padding-left:10px;"
                    class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }} </td>
            </tr>
            <tr>

                <td class="fontcaption" width="25%">Stripe Product ID: <span class="redrequird">*</span></td>
                <td><input style="width:200px;" type="text" name="subscriptionProductId" id="subscriptionProductId"
                           class="validate[required]"
                           value="{{env('STRIPE_PRODUCT_ID')}}" disabled/></td>
            </tr>

            <tr>
                <td class="fontcaption" width="25%">Stripe Plan ID: <span class="redrequird">*</span></td>
                <td><input style="width:200px;" type="text" name="subscriptionProductId" id="subscriptionPlanId"
                           class="validate[required]"
                           value="{{$settings->getSubscriptionPlanId()}}" disabled/></td>
            </tr>
            <tr>
                <td class="fontcaption">Doctor Charge Recurring Amount: <span class="redrequird">*</span>
                </td>
                <td><input style="width:50px;" type="text" name="doctorChargeRecurringAmount"
                           id="doctorChargeRecurringAmount" class="validate[required]"
                           value="{{$settings->getDoctorChargeRecurringAmount()}}"/>
                    <!-- Currency -->
                    <span class="fontcaption">Currency:  </span><span class="redrequird">*</span>
                    <input style="width:75px;" type="text" name="doctorChargeRecurringCurrency" value="USD" disabled/>
                    <!-- Each -->
                    <span class="fontcaption">Recurring Interval:  </span><span class="redrequird">*</span>
                    <input style="width:100px;" type="text" name="doctorChargeRecurringInterval"
                           id="doctorChargeRecurringInterval"
                           value="Monthly" disabled=""/>
                    <br/>
                    <br/>
                    <font class="comments">
                        <span class="redrequird"> [Note: Upgrading and Downgrading Subscription reflects STRIPE Directly]
                        </span>
                    </font>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td>
                                            <input type="submit" name="Submit" value="Submit" class="buttonmidle"
                                                   onclick="beforeSubmit();"/>
                                        </td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td><input type="button" name="reset" value="Cancel" class="buttonmidle"
                                                   onclick="window.location.href='/admin/settings'"/></td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
    {!! Form::close() !!}
@endsection

@prepend('scripts')
    <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery-ui.min.css')}}">
@endprepend

@push('scripts')
    <script>
        let pageForm = "frmPageDetails";
        $(function () {

        });

        function beforeSubmit() {
            $.validationEngine.beforeSubmit(pageForm);
        }
    </script>
@endpush
