<td align="center" class="fontsmall">
    @if ($paginator->hasPages())
        <nav>
            <ul class="pagination">
                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                    <span class="pagenav">&lt;&lt; First</span>
                @else
                    <li>
                        <a class="paginate-arrows" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                        >&lt; Prev
                        </a>
                    </li>
                @endif

                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="active" aria-current="page"><span>{{ $page }}</span></li>
                            @else
                                <li><a href="{{ $url }}"><strong>{{ $page }}</strong></a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li>
                        <a class="paginate-arrows" href="{{ $paginator->nextPageUrl() }}" rel="next"
                           aria-label="@lang('pagination.next')">Next
                            &rsaquo;</a>
                    </li>
                @else
                    <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                        <span aria-hidden="true">&rsaquo;</span>
                    </li>
                @endif
            </ul>
        </nav>
    @endif
    <div id="paginate-limit-options">
        <font class="pagenav">Record Per Page : </font>
        <select id="pageSize" name="pageSize" class="combo"
                onchange="onchangePageSize(this)" size="1">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="20">20</option>
            <option value="25">25</option>
            <option value="30">30</option>
            <option value="50" selected="selected">50</option>
            <option value="100">100</option>
        </select>
    </div>
</td>
@push('scripts')
    <script>
        let limit = 50;
        $(function () {
            let searchParams = new URLSearchParams(window.location.search);
            if (searchParams.has('limit')) {
                $('#pageSize').val(searchParams.get('limit'))
            }
        });

        function onchangePageSize(event) {
            limit = event.value;
            return replaceSearch('limit', limit);
        }

        function replaceSearch(name, value) {
            var str = location.search;
            if (new RegExp("[&?]" + name + "([=&].+)?$").test(str)) {
                str = str.replace(new RegExp("(?:[&?])" + name + "[^&]*", "g"), "")
            }
            str += "&";
            str += name + "=" + value;
            str = "?" + str.slice(1);
            // there is an official order for the query and the hash if you didn't know.
            location.assign(location.origin + location.pathname + str + location.hash)
        };
    </script>
@endpush