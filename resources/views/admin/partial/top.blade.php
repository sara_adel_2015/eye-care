<table width="1002" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top"
                        style="background-image: url('{{asset('admin/images/background-bg.jpg')}}'); repeat-x scroll 0 0 #EEEEEE;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="55%">&nbsp;<a href="/admin/home"><img
                                                src="{{asset('admin/images/logo.png')}}" alt="" border="0"
                                                height="120" width="120"/></a></td>
                                <td width="45%" valign="top" style=" padding-right:20px;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="right" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td><img src="{{asset('admin/images/top-link-left.png')}}"
                                                                 alt="" width="27"
                                                                 height="29"/></td>
                                                        <td class="toplink"><a href="/admin/home">
                                                                Home
                                                            </a> | <a href="{{route('admin-logout')}}">
                                                                Logout
                                                            </a></td>
                                                        <td align="right"><img
                                                                    src="{{asset('admin/images/top-link-right.png')}}"
                                                                    alt=""
                                                                    width="29" height="29"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height:58px;"></td>
                                        </tr>
                                        <tr>
                                            <td class="abovenavigation" valign="bottom">Welcome
                                                {{Auth::user()->getUserName()}}
                                                ,
                                                Last Login
                                                :
                                                {{Auth::user()->getLastVisited()->format('yy-m-d h:i:s')}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="pagebg">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>@include('admin/partial/menu')</td>
                            </tr>
                            <tr>
                                <td valign="top" style="padding-left:20px; padding-right:20px;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" style="padding-top:50px;">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="10" valign="top"><img
                                                                    src="{{asset('admin/images/data-area-1-1.png')}}"
                                                                    alt="" width="10" height="10"/>
                                                        </td>
                                                        <td valign="top" class="boxtopmid"><img
                                                                    src="{{asset('admin/images/data-area-1-2.png')}}"
                                                                    alt="" width="1"
                                                                    height="10"/></td>
                                                        <td width="10" align="right" valign="top"><img
                                                                    src="{{asset('admin/images/data-area-1-3.png')}}"
                                                                    alt="" width="10"
                                                                    height="10"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="boxvarleft">&nbsp;</td>