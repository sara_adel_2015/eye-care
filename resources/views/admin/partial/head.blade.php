<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Control Panel :: Welcome to : Eyecare Amsler Grid Eye Test</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('admin/images/favicon.ico')}}">
    <link href="{{asset('admin/css/all.css')}}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{asset('admin/css/stylesheet.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/css/stylesheet-master.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/validationEngine.jquery.css')}}">
    <script type="text/javascript" src="{{asset('admin/js/jquery-3.4.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/jquery.validationEngine.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/jquery.validationEngine-en.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/common-functions.js')}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @stack('stylesheets')
    @stack('scripts')
</head>