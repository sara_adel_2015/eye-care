<div id="wrapper">
    <div id="header">
        <div class="menu_left"></div>
        <ul id="nav">
            <li class="level1-a" style="width:100px; ">
                <a href="/admin/home" style="color:#ffffff;text-align:center">
                    <i class="fa fa-home"></i>Home
                </a>
            </li>
            <li class="level1-a" style="width:220px; ">
                <a href="#" style="color:#ffffff;text-align:center">
                    <i class="fa fa-folder"></i>Manage Doctors Directory
                </a>
                <ul>
                    <p class="sp_space"></p>
                    <li></i><a href="{{URL::route('dictionary-doctor-add')}}"><i class="fa fa-plus"></i>Add Doctor</a>
                    </li>
                    <li><a href="{{URL::route('dictionary-doctor-list')}}"><i class="fa fa-list"></i>Doctor List</a>
                    </li>
                    <li><a href="{{URL::route('dictionary-doctor-upload-sheet')}}"><i class="fa fa-upload"></i>Upload
                            Sheet</a></li>
                    <p class="sp_space"></p>
                    <li class="sp_box"></li>
                </ul>
            </li>
            <li class="level1-a" style="width:200px; ">
                <a href="#" style="color:#ffffff;text-align:center">
                    <i class="fa fa-user"></i>Manage Subscribed Doctors
                </a>
                <ul>
                    <p class="sp_space"></p>
                    {{--<li><a href="{{URL::route('doctor-add')}}"><i class="fa fa-plus"></i>Add Doctor</a></li>--}}
                    <li><a href="{{URL::route('doctor-list')}}"><i class="fa fa-list"></i>Doctor List</a></li>
                    <li><a href="{{URL::route('doctor-payment-history')}}"><i class="fa fa-history"></i>Payment History</a>
                    </li>
                    <li><a href="{{URL::route('cancel-subscription-requests')}}"><i class="fa fa-remove"></i>Cancel
                            Subscription Requests</a></li>
                    <p class="sp_space"></p>
                    <li class="sp_box"></li>
                </ul>
            </li>
            <li class="level1-a" style="width:150px; ">
                <a href="#" style="color:#ffffff;text-align:center">
                    <i class="fa fa-eye"></i>Manage Patients
                </a>
                <ul>
                    <p class="sp_space"></p>
                    <li><a href="{{URL::route('patient-add')}}"><i class="fa fa-plus"></i>Add Patient</a></li>
                    <li><a href="{{URL::route('patient-list')}}"><i class="fa fa-list"></i>Patient List</a></li>
                    <p class="sp_space"></p>
                    <li class="sp_box"></li>
                </ul>
            </li>
            <li class="level1-a" style="width:150px; ">
                <a href="#" style="color:#ffffff;text-align:center">
                    <i class="fa fa-eyedropper"></i>Manage Eye Test
                </a>
                <ul>
                    <p class="sp_space"></p>
                    <li><a href="{{URL::route('eye-test-add')}}"><i class="fa fa-plus"></i>Add Eye Test</a></li>
                    <li><a href="{{URL::route('eye-test-list')}}"><i class="fa fa-list"></i>Eye Test List</a></li>
                    <p class="sp_space"></p>
                    <li class="sp_box"></li>
                </ul>
            </li>
            <li class="level1-a" style="width:168px; ">
                <a href="#" style="color:#ffffff;text-align:center">
                    <i class="fa fa-ban"></i>Settings
                </a>
                <ul>
                    <p class="sp_space"></p>
                    <li><a href="{{URL::route('admin-change-password')}}"><i class="fa fa-key"></i>Change Password</a>
                    </li>
                    <li><a href="{{URL::route('admin-settings')}}"><i class="fa fa-cogs"></i>Settings</a></li>
                    <li><a href="{{URL::route('view-subscription-settings')}}"><i class="fa fa-money"></i>Subscription
                            Settings</a></li>
                    <p class="sp_space"></p>
                    <li class="sp_box"></li>
                </ul>
            </li>
            <li class="level1-a" style="width:0px;"></li>
        </ul>
        <div class="menu_right"></div>
    </div>
</div>
