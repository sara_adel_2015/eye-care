@extends('admin/admin-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {{--<form name="frmCmsPageList" id="frmCmsPageList" method="post" action="doctorlist.php">--}}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td class='loginhead'>
                    <span><i class="fa fa-money" aria-hidden="true"></i>Payment Recurreing History</span>
                </td>
            </tr>
            <tr>
                <td align="right" style="padding-right:20px;">
                </td>
            </tr>
            <tr>
                <td align="left" class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}</td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="table-border" border="0" cellspacing="2">
                        <tr>
                            <td align="center" class="list-title" width="15%">Doctor Name</td>
                            <td align="center" class="list-title" width="15%">Payment Transaction ID</td>
                            <td align="center" class="list-title" width="15%">Plan ID</td>
                            <td align="center" class="list-title" width="15%">Amount</td>
                            <td align="center" class="list-title" width="15%">Status</td>
                            <td align="center" class="list-title" width="15%">Date</td>
                            <td align="center" class="list-title" width="15%">View Invoice</td>
                        </tr>
                        <?php $index = 0;?>

                        @foreach ($list as $history)
                            <tr class="Hrnormal" onmouseover="this.className='Hrhover';"
                                onmouseout="this.className='Hrnormal';">
                                <td align="center" class="list-contents">
                                    @if($history['doctorName'])
                                        <a target="_blank"
                                           href="{{ URL::route('doctor-edit', $history[0]->getDoctorId()) }}"
                                           title="{{$history[0]->getCustomerReference()}}">
                                            {{$history['doctorName']}}
                                        </a>
                                    @else
                                        <span title="{{$history[0]->getCustomerReference()}}"> Anonymous</span>
                                    @endif
                                </td>
                                <td align="center" class="list-contents">{{$history[0]->getChargeId()}}</td>
                                <td align="center" class="list-contents">{{$history[0]->getSubscriptionPlanId()}}</td>
                                <td align="center" class="list-contents">{{$history[0]->getAmount()}}</td>
                                <td align="center" class="list-contents">
                                    @if($history[0]->getIsSuccess())
                                        <span style="color: forestgreen">
                                               <i class="fa fa-check"></i>Success
                                        </span>
                                    @else <span style="color: red">Failed</span>
                                    @endif
                                </td>
                                <td align="center"
                                    class="list-contents">{{$history[0]->getCreatedAt()->format('yy-m-d h:i:s')}}</td>
                                <td align="center" class="list-contents">
                                    <a href="{{$history[0]->getInvoicePdfUrl()}}"
                                       target="_blank"
                                       title="Download Invoice">
                                        <i class="fa fa-cloud-download"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                {{--**Pagination here**--}}
                {{ $list->appends(request()->input())->links('admin/partial/paginator') }}
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        {{--</form>--}}
    </td>
@endsection
@push('scripts')
    <script>
        csrf_token = "{{ csrf_token() }}";
        pageForm = "frmPageDetails";
    </script>
@endpush
