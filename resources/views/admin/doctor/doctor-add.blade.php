@extends('admin/admin-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        @if($action == 'add')
            {!!  Form::open(array('route' => 'doctor-create','files' => true ,'id'=> 'frmPageDetails')) !!}
        @else
            {!!  Form::open(array('route' => ['doctor-update',$doctor->getId()],'method'=>'PUT','files' => true,'id'=> 'frmPageDetails')) !!}
        @endif
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class='loginhead'>@if($action == 'add')  Add @else Edit @endif Doctor Detail</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="redrequirdbig" colspan="2" align="right">* Indicates required field</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td align="left" class="{{ Session::get('flash_type') }}">
                    {{ Session::get('flash_message') }}
                </td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">
                    Doctor Name: <span class="redrequird">*</span>
                </td>
            </tr>
            <tr>
                <td width="80%">
                    <input type="text" name="doctorName" id="doctorname" class="validate[required]" size="50"
                           maxlength="100" value="{{$doctor->getDoctorName()}}"/>
                </td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Speciality: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                    <textarea type="textarea" name="speciality" id="speciality" cols="45" rows="5"
                              class="validate[required]">{{$doctor->getSpeciality()}}</textarea>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Address: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                    <textarea type ="textarea" name="address" id="address" cols="45" rows="3"
                              class="validate[required]">{{$doctor->getAddress()}}</textarea>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">City: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="city" id="city" value="{{$doctor->getCity()}}"
                               class="validate[required]"
                               size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">State: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="state" id="state" value="{{$doctor->getState()}}"
                               class="validate[required]"
                               size="50"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Zipcode: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="zip" id="zip" value="{{$doctor->getZip()}}" class="validate[required]"
                               size="50" onblur="return checkAddress();"/>
                    </label>&nbsp;<a href="javascript:;" onclick="return checkAddress();"
                                     style="text-decoration:none;"><br/><font color="#0033CC">Check latitude and
                            longitude
                            value</font></a></td>
            </tr>
            <tr>
                <td class="fontcaption" id="divMsg"></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Latitude: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="latitude" id="latitude" class="validate[required]]" size="50"
                               maxlength="20" value="{{$doctor->getLatitude()}}"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Longitude: <span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="longitude" id="longitude" class="validate[required]" size="50"
                               maxlength="100" value="{{$doctor->getLongitude()}}"/>
                    </label></td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">Weblink:</td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="weblink" id="weblink" value="{{$doctor->getWeblink()}}" size="50"/>
                    </label></td>
            </tr>


            <tr>
                <td class="fontcaption" width="20%">Phone:<span class="redrequird">*</span></td>
            </tr>
            <tr>
                <td width="80%"><label>
                        <input type="text" name="phone" id="Phone" value="{{$doctor->getPhone()}}" size="50" class="validate[required]"/>
                    </label></td>
            </tr>
            <tr>
                <td width="80%">
                    <label>
                        <input type="file" name="iphoneImage" id="iphoneImage" size="30"
                               value="{{$doctor->getIphoneImage()}}"/>
                    </label>
                    <font class="comments">[Note: Upload only jpg, gif, png file type]</font> <br/>
                    @if($action == 'edit')
                        @if(empty($doctor->getIphoneImage()) or ($doctor->getIphoneImage() == " "))
                            <img src="{{asset('admin/uploads/doctorlist/noImage.gif') }}" width="80" height="80"/>&nbsp;
                            &nbsp;&nbsp;&nbsp;
                        @else
                            <img src="{{asset('admin/uploads/doctorlist/'.$doctor->getIphoneImage())}}" width="80"
                                 height="80"/>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="javascript:void(0)" onClick="return deletePhoto({{$doctor->getId()}});"
                               class="links">
                                <img src="{{asset('admin/images/cancel_s.png')}}" alt="Delete"
                                     title="Delete iphone image"
                                     width="16"
                                     height="16" border="0"/>
                            </a>
                        @endif
                    @endif
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td>
                                            <input type="submit" name="Submit" value="Submit" class="buttonmidle"
                                                   onclick="beforeSubmit(this);"/>
                                        </td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td><input type="button" name="reset" value="Cancel" class="buttonmidle"
                                                   onclick="window.location.href='/admin/doctor'"/></td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
    {{--{!! Form::close() !!}--}}
@endsection
@push('scripts')
    <script>
        csrf_token = "{{ csrf_token() }}";
    </script>
    <script type="text/javascript" src="{{asset('admin/js/doctor-add-edit.js')}}"></script>
@endpush

@prepend('scripts')
    <script>
        let pageForm = "frmPageDetails";

        function beforeSubmit() {
            $.validationEngine.beforeSubmit(pageForm);
        }
    </script>
@endprepend