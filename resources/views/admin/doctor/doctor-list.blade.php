@extends('admin/admin-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {{--<form name="frmCmsPageList" id="frmCmsPageList" method="post" action="doctorlist.php">--}}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td class='loginhead'>Doctor List</td>
            </tr>
            <tr>
                <td align="right" style="padding-right:20px;"><a href="{{URL::route('doctor-add')}}"
                                                                 class="links"><img
                                width="20" height="20" src="{{asset('admin/images/add_small.png')}}" border="0"
                                alt="Add New"
                                title="Add New"/></a>
                </td>
            </tr>
            <tr>
                <td align="left" class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}</td>
            </tr>
            <tr>
                <td align="left"><span id="ajax_msg"></span></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="table-border" border="0" cellspacing="2">
                        <tr>
                            <td align="center" class="list-title" width="10%">Sr.No</td>
                            <td align="center" class="list-title" width="20%">Image</td>
                            <td align="center" class="list-title" width="25%">Doctor Name</td>
                            <td align="center" class="list-title" width="25%">Email</td>
                            <td align="center" class="list-title" width="25%">Sort Order
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td><input type="submit" name="strorderSave" value="Save Ordering"
                                                   class="buttonmidle" id="strorderSave" onclick="updateStrOrders();"/>
                                        </td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" class="list-title" width="10%">Status</td>
                            <td align="center" class="list-title" width="10%">Subscription Status</td>
                            <td class="list-title" colspan="3" width="15%" align="center">Action</td>
                        </tr>
                        <?php $index = 0;?>
                        @foreach ($doctors as $doctor)
                            <?php $index++;?>
                            <tr id="{{$doctor->getId()}}" class="row-data Hrnormal">
                                <td align="center" class="list-contents">{{$doctor->getId()}}</td>
                                <td align="center" class="list-contents">
                                    @if(empty($doctor->getIphoneImage()) or ($doctor->getIphoneImage() == " "))
                                        <a href="{{asset('admin/uploads/doctorlist/noImage.gif') }}"
                                           target="_blank"><img width="80" height="80"
                                                                src="{{asset('admin/uploads/doctorlist/noImage.gif') }}"/>
                                        </a>
                                    @else
                                        <a href="{{asset('admin/uploads/doctorlist/'.$doctor->getIphoneImage()) }}"
                                           target="_blank"><img width="80" height="80"
                                                                src="{{asset('admin/uploads/doctorlist/'.$doctor->getIphoneImage()) }}"/>
                                        </a>
                                    @endif

                                </td>
                                <td align="center" class="list-contents">{{$doctor->getDoctorName()}}</td>
                                <td align="center" class="list-contents">{{$doctor->getEmail()}}</td>
                                <td align="center" class="list-contents">
                                    <input type="text" id="strorder{{$doctor->getId()}}"
                                           name="strorder{{$doctor->getId()}}"
                                           value="{{$doctor->getStrorder()}}"
                                           size="4"
                                           maxlength="10"
                                           onkeypress="updateStrOrder('{{$doctor->getId()}}',event)"
                                           class="validate[required,custom[onlyNumber]]"/>
                                </td>
                                <td align="center" class="list-contents">
                                    @if ($doctor->getStatus() == 'Active')
                                        <a href="javascript:void(0)"
                                           onclick="updateStatus('{{$doctor->getId()}}','IN_ACTIVE');"><img
                                                    alt="{{$doctor->getStatus()}}" title="{{$doctor->getStatus()}}"
                                                    src="{{asset('admin/images/true.gif')}}"
                                                    al border="0"/></a>
                                    @elseif ($doctor->getStatus() == 'Inactive')
                                        <a href="javascript:void(0)"
                                           onclick="updateStatus('{{$doctor->getId()}}','ACTIVE');"><img
                                                    alt="{{$doctor->getStatus()}}"
                                                    title="{{$doctor->getStatus()}}"
                                                    src="{{asset('admin/images/false.gif')}}"
                                                    al border="0"/></a>
                                    @endif
                                </td>
                                <td align="center" class="list-contents">
                                    @if ($doctor->getIsSubscriptionCancelled())
                                        Cancelled
                                    @else
                                        Active
                                    @endif
                                </td>
                                <td width="3%" align="center" class="list-contents">

                                    <a href="{{URL::route('doctor-payment-history',['doctorId'=>$doctor->getId()])}}"
                                       title="View Payment History">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                                <td width="5%" align="center" class="list-contents">
                                    <a href="{{URL::route('doctor-edit',$doctor->getId())}}" class="links">
                                        <img src="{{asset('admin/images/edit.jpg')}}"
                                             alt="Edit"
                                             title="Edit"
                                             border="0"/>
                                    </a>
                                </td>
                                <td width="4%" align="center" class="list-contents">
                                    <a href="javascript:void(0)" onclick="deleteDoctor({{$doctor->getId()}});"
                                       class="links">
                                        <img src="{{asset('admin/images/delete.gif')}}"
                                             alt="Delete"
                                             title="Delete"
                                             width="16" height="16"
                                             border="0"/>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                {{--**Pagination here**--}}
                {{ $doctors->appends(request()->input())->links('admin/partial/paginator') }}
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        {{--</form>--}}
    </td>
@endsection
@push('scripts')
    <script>
        csrf_token = "{{ csrf_token() }}";
        pageForm = "frmPageDetails";
    </script>
    <script type="text/javascript" src="{{asset('admin/js/doctor-list.js')}}"></script>
@endpush
