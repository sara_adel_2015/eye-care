@extends('admin/admin-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {{--<form name="frmCmsPageList" id="frmCmsPageList" method="post" action="doctorlist.php">--}}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td class='loginhead'><i class="fa fa-remove"></i>Cancel Subscription Requests</td>
            </tr>
            <tr>
                <td align="right" style="padding-right:20px;">
                </td>
            </tr>
            <tr>
                <td align="left" class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }}</td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="table-border" border="0" cellspacing="2">
                        <tr>
                            <td align="center" class="list-title" width="10%">Sr.No</td>
                            <td align="center" class="list-title" width="25%">Doctor Name</td>
                            <td class="list-title" colspan="3" width="15%" align="center">Action</td>
                        </tr>
                        <?php $index = 0;?>
                        @foreach ($doctors as $doctor)
                            <?php $index++;?>
                            <tr id="{{$doctor->getId()}}" class="row-data Hrnormal">
                                <td align="center" class="list-contents">{{$doctor->getId()}}</td>
                                <td align="center" class="list-contents">{{$doctor->getDoctorName()}}</td>
                                <td width="3%" align="center" class="list-contents">
                                    {!!  Form::open(array('route' => ['confirm-cancel-subscription',$doctor->getId()] ,'method'=>"POST",'id'=> 'confirm-cancel-subscription-form')) !!}
                                    <a href="javascript:void(0);"
                                       onclick="confirmCancelSubscription({{$doctor->getId()}})"
                                       title="Confirm Cancel Subscription">
                                        <i class="fa fa-check"></i>
                                    </a>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                {{--**Pagination here**--}}
                {{ $doctors->appends(request()->input())->links('admin/partial/paginator') }}
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        {{--</form>--}}
    </td>
@endsection
@prepend('scripts')
    <script>
        function confirmCancelSubscription(doctorId) {
            if (confirm('Are you Sure that you want cancel subscription For Doctor ID ' + doctorId)) {
                $('#confirm-cancel-subscription-form').submit();
            }
        }
    </script>
@endprepend
@push('scripts')
    <script>
        csrf_token = "{{ csrf_token() }}";
        pageForm = "frmPageDetails";

    </script>
    <script type="text/javascript" src="{{asset('admin/js/doctor-list.js')}}"></script>
@endpush
