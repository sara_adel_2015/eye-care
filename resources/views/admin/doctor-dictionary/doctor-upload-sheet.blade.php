@extends('admin/admin-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        {!!  Form::open(array('route' => 'dictionary-doctor-import-sheet','files' => true ,'method'=>'POST','id'=> 'frmPageDetails')) !!}
        <table width="100%" border="0" cellpadding="4" cellspacing="0">

            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class='loginhead'>Upload Doctor Sheet file</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <H2 class="redrequirdbig">CSV Example:</H2>
                    <img src="{{asset('admin/images/import-doctor-sample.png')}}" height="80px" width="900px"
                         style="margin-bottom: 10px;margin-top: 20px">
                </td>
            </tr>
            <tr>
                <td class="redrequirdbig" colspan="2" align="right">* Indicates required field</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td align="left" class="{{ Session::get('flash_type') }}">
                    {{ Session::get('flash_message') }}
                </td>
            </tr>

            <tr>
                <td width="80%">
                    <label>
                        <input type="file" name="file" id="file" size="30"
                               value=""/>
                    </label>
                    <font class="comments">[Note: Upload only xls,xlsx,csv file type]</font> <br/>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td>
                                            <input type="submit" name="Submit" value="Submit" class="buttonmidle"
                                                   onclick="beforeSubmit(this);"/>
                                        </td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td><input type="button" name="reset" value="Cancel" class="buttonmidle"
                                                   onclick="window.location.href='/admin/doctor'"/></td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
    {!! Form::close() !!}
@endsection
