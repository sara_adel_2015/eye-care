@extends('admin/admin-base')
@section('content')
    <td valign="top" class="formboxdatabg">
        @if($action == 'add')
            {!!  Form::open(array('route' => 'settings-create','id'=>'frmPageDetails')) !!}
        @else
            {!!  Form::open(array('route' => ['settings-update',$settings->getId()],'method'=>'PUT','id'=>'frmPageDetails')) !!}
        @endif
        <table width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class='loginhead'> Settings</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="redrequirdbig" colspan="2" align="right"></td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td align="left" colspan="2" style="padding-left:10px;"
                    class="{{ Session::get('flash_type') }}">{{ Session::get('flash_message') }} </td>
            </tr>
            <tr>

                <td class="fontcaption" width="25%">Allowed Distance Ratio: <span class="redrequird">*</span></td>
                <td><input style="width:200px;" type="text" name="allowedCompareRatio" id="allowedCompareRatio"
                           class="validate[required]"
                           value="{{$settings->getAllowedCompareRatio()}}"/></td>
            </tr>

            <tr>
                <td class="fontcaption" width="25%">Allowed Similarity Percentage: <span class="redrequird">*</span>
                </td>
                <td><input style="width:200px;" type="text" name="allowedSimilitryPercentage"
                           id="allowed_similarity_percentage" class="validate[required]"
                           value="{{$settings->getAllowedSimilitryPercentage()}}"/>
                </td>
            </tr>

            {{--<tr>--}}
            {{--<td class="fontcaption" width="25%">Notification Email if matched: <span class="redrequird">*</span>--}}
            {{--</td>--}}
            {{--<td><input style="width:200px;" type="text" name="notificationEmailMatched"--}}
            {{--id="notificationEmailMatched" class="validate[required]"--}}
            {{--value="{{$settings->getNotificationEmailMatched()}}"/></td>--}}
            {{--</tr>--}}
            <tr>
                <td class="fontcaption" width="30%">Notification Email if matched Reception : <span
                            class="redrequird">*</span>
                </td>
                <td>
                    <input type="radio" name="matchedResultReception" class="matchedResultReception"
                           value="patient"
                           id="is_patient_matched"
                           @if ($settings->getMatchedResultReception() == 'patient')  checked='checked'
                            @endif />
                    <label>Patient</label>
                    <input type="radio" name="matchedResultReception" class="matchedResultReception"
                           value="custom"
                           id="is_custom_matched" @if ($settings->getMatchedResultReception() == 'custom')
                           checked='checked' @endif />
                    <label>Custom</label>
                </td>
            </tr>
            <tr>
                <td class="fontcaption" width="25%">Not Matched Result Reception : <span class="redrequird">*</span>
                </td>
                <td>
                    <input type="radio" name="differentResultReception" class="differentResultReception"
                           value="patient"
                           id="is_patient_not_matched"
                           @if ($settings->getDifferentResultReception() == 'patient')  checked='checked'
                            @endif />
                    <label>Patient</label>
                    <input type="radio" name="differentResultReception" class="different_result_recieption"
                           value="custom"
                           id="is_custom_not_matched" @if ($settings->getDifferentResultReception() == 'custom')
                           checked='checked' @endif />
                    <label>Custom</label>
                </td>
            </tr>
            <tr id="differentResultReceptionEmail"
                @if($settings->getDifferentResultReception() == 'patient' && $settings->getMatchedResultReception() == 'patient')
                style='display:none;' @endif >
                <td class="fontcaption" width="25%">Please Enter your Custom Email you want to send patient eye tests
                    to:
                </td>
                <td>

                    <input style="width:200px;" type="text" name="differentResultReceptionEmail"
                           id="differentResultReceptionEmailTxt"
                           value="{{$settings->getDifferentResultReceptionEmail()}}"/>
                    <span class="redrequird">Note: This email will be sent if you choose custom email if not matched OR custom email if matched</span>
                </td>
            </tr>
            <tr>
                <td class="fontcaption" width="20%">
                    Default Physician: <span class="redrequird">*</span>
                    <i class="fa fa-search" style=""></i>
                </td>
                <td width="80%"><label>
                        <input type="hidden" id="defaultPhysicianId" name="defaultPhysicianId"
                               value="{{$settings->getDefaultPhysician()}}">
                        <select id="defaultPhysicianId" name="defaultPhysicianId" style="width:250px;">
                            @foreach ($doctors as $doctor)
                                @if($doctor->getId() == $settings->getDefaultPhysician())
                                    <option value="{{$doctor->getId()}}" selected>
                                @else
                                    <option value="{{$doctor->getId()}}">
                                        @endif
                                        {{$doctor->getDoctorName()}} - ({{$doctor->getDoctorCode()}})
                                    </option>
                                    @endforeach
                        </select>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td>
                                            <input type="submit" name="Submit" value="Submit" class="buttonmidle"
                                                   onclick="beforeSubmit();"/>
                                        </td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top"><img src="{{asset('admin/images/button-left.png')}}" alt=""
                                                              width="14" height="33"/>
                                        </td>
                                        <td><input type="button" name="reset" value="Cancel" class="buttonmidle"
                                                   onclick="window.location.href='/admin/settings'"/></td>
                                        <td align="right" valign="top"><img
                                                    src="{{asset('admin/images/button-right.png')}}"
                                                    alt="" width="15"
                                                    height="33"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
    {!! Form::close() !!}
@endsection

@prepend('scripts')
    <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery-ui.min.css')}}">
@endprepend

@push('scripts')
    <script>
        let pageForm = "frmPageDetails";
        $(function () {
            $('#is_custom_matched,#is_custom_not_matched').click(function () {
                $('#differentResultReceptionEmail').show();
                $('#differentResultReceptionEmailTxt').addClass('validate[required]');

            });

            $('#is_patient_matched,is_patient_not_matched').click(function () {
                $('#differentResultReceptionEmail').hide();
                $('#differentResultReceptionEmailTxt').removeClass('validate[required]');

            });
        });

        function beforeSubmit() {
            $.validationEngine.beforeSubmit(pageForm);
        }

        //search for doctors
        $(function () {
            csrf_token = "{{ csrf_token() }}";
            $("#defaultDoctor").autocomplete({
                source: "/admin/doctor/autocomplete",
                select: function (event, ui) {
                    $("#defaultDoctor").val(ui.item.value),
                        $("#defaultPhysicianId").val(ui.item.id)
                },
                classes: {
                    "ui-autocomplete": "highlight"
                }
            });

        });
    </script>
@endpush
