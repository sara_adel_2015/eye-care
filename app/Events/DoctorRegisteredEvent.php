<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 5/11/2020
 * Time: 1:37 AM
 */

namespace App\Events;

use App\Entities\Doctor;
use Illuminate\Queue\SerializesModels;

class DoctorRegisteredEvent
{
    use SerializesModels;
    /**
     * The authenticated user.
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    public $user;

    public $request;

    /**
     * DoctorRegisteredEvent constructor.
     * @param Doctor $user
     */
    public function __construct(Doctor $user)
    {
        $this->user = $user;
    }
}