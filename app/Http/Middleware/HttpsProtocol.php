<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 8/28/2020
 * Time: 3:59 PM
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class HttpsProtocol
{
    public function handle($request, Closure $next)
    {
        if (!$request->secure() && App::environment() === 'production') {
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }
}