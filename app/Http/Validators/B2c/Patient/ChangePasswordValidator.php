<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\B2c\Patient;

use App\Http\Validators\BaseRequestValidator;

class ChangePasswordValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'current-password' => 'required|string',
            'new-password' => 'required|string|min:5|max:20',
            //'new-password_confirmation' => 'string|min:5|max:20',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [];

    }
}