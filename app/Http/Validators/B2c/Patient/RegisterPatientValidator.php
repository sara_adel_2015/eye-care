<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\B2c\Patient;

use App\Entities\Doctor;
use App\Http\Validators\BaseRequestValidator;
use App\Entities\Patient;

class RegisterPatientValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'firstName' => ['required', 'string', 'max:255'],
            'lastName' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:40', 'unique:' . Patient::class],
            'password' => ['required', 'string'],
            'address' => ['nullable', 'string', 'max:255'],
            'city' => ['nullable', 'string', 'max:255'],
            'state' => ['nullable', 'string', 'max:255'],
            'zip' => ['required'],
            'phone' => ['numeric'],
            'doctorId' => 'exists:' . Doctor::class . ',id',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
            'email.unique' => 'Already exist!'
        ];
    }
}