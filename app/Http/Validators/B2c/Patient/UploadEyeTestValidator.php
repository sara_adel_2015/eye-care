<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\B2c\Patient;

use App\Http\Validators\BaseRequestValidator;
use Auth;

class UploadEyeTestValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'imageType' => [
                'required',
            ],
            'deviceType' => [
                'required'
            ],
            'comparePattern' => [
                'nullable',
                'string'
            ],
            'imageFile' => [
                'required',
                'image',
                'mimes:jpeg,jpg,png,gif'
            ]

        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
            'imageFile.mimes' => 'Invalid file type. Upload only jpg, gif, and png file type',
            'imageFile.required' => 'Please select file to upload'
        ];

    }
}