<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\B2c\Patient;

use App\Entities\Patient;
use App\Http\Validators\BaseRequestValidator;
use Auth;

class ResetPasswordValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email',
                'exists:' . Patient::class . ',email',

            ]
        ];
    }

    //put your code here
    public function messages(): array
    {
        return ['email.exists' => 'Email is not found'];

    }
}