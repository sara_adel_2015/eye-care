<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\B2c\Doctor;

use App\Entities\Doctor;
use App\Http\Validators\BaseRequestValidator;
use Omnipay\Common\Helper;

class RegisterDoctorValidator extends BaseRequestValidator
{

    public function rules(): array
    {
        return [
            'firstName' => ['required', 'string', 'max:255'],
            'lastName' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:40', 'unique:' . Doctor::class],
            'password' => ['required', 'string'],
            'address' => ['string', 'required', 'max:255'],
            'city' => ['string', 'required', 'max:255'],
            'state' => ['string', 'required', 'max:255'],
            'zip' => ['required'],
            'phone' => ['numeric'],
            'speciality' => ['required'],
            'token' => ['required']
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
            'email.unique' => 'Already exist!'
        ];
    }
}