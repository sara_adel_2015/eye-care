<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\B2c\Doctor;

use App\Http\Validators\BaseRequestValidator;
use Omnipay\Common\Helper;

class AddNewPaymentCardValidator extends BaseRequestValidator
{

    public function rules(): array
    {
        return [
            'token' => ['required']
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [];
    }
}