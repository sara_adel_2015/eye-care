<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\B2c\Doctor;


use App\Entities\DoctorPaymentCards;
use App\Http\Validators\BaseRequestValidator;

class UpdateDefaultPaymentCardValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'id' => 'exists:' . DoctorPaymentCards::class . ',id',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
        ];
    }
}