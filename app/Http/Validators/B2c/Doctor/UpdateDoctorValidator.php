<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\B2c\Doctor;


use App\Http\Validators\BaseRequestValidator;
use App\Entities\Doctor;

class UpdateDoctorValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'id' => 'exists:' . Doctor::class . ',id',
            'doctorName' => 'required|string',
            'speciality' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'longitude' => 'required',
            'phone' => 'required',
        ];
    }

    public function messages(): array
    {
        return [];
    }
}