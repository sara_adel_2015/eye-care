<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\Doctor;


use App\Http\Validators\BaseRequestValidator;

class AddDoctorValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [

            'doctorName' => 'required|string',
            'speciality' => 'required',
            'speciality' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'longitude' => 'required',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [];
    }
}