<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\Doctor;


use App\Http\Validators\BaseRequestValidator;
use App\Entities\Doctor;

class UpdateDoctorOrderValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'id' => 'exists:' . Doctor::class . ',id',
            'strOrder' => 'required|integer',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
        ];
    }
}