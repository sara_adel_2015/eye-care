<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\Doctor;


use App\Http\Validators\BaseRequestValidator;
use App\Entities\Doctor;

class CheckAddressValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'address' => 'required',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
        ];
    }
}