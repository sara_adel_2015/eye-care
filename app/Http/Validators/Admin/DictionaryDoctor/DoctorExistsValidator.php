<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\DictionaryDoctor;


use App\Entities\DictionaryDoctor;
use App\Http\Validators\BaseRequestValidator;

class DoctorExistsValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'id' => 'exists:' . DictionaryDoctor::class . ',id',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
        ];
    }
}