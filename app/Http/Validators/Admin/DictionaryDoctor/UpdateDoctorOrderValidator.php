<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\DictionaryDoctor;


use App\Entities\DictionaryDoctor;
use App\Http\Validators\BaseRequestValidator;

class UpdateDoctorOrderValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'id' => 'exists:' . DictionaryDoctor::class . ',id',
            'strOrder' => 'required|integer',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
        ];
    }
}