<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\DictionaryDoctor;


use App\Entities\DictionaryDoctor;
use App\Http\Validators\BaseRequestValidator;

class UpdateDoctorValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'id' => 'exists:' . DictionaryDoctor::class . ',id',
            'doctorName' => 'required|string',
            'speciality' => 'required',
            'speciality' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'longitude' => 'required',
            'phone' => 'required',
        ];
    }

    public function messages(): array
    {
        return [];
    }
}