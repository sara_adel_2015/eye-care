<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\DictionaryDoctor;


use App\Http\Validators\BaseRequestValidator;

class ImportDoctorSheetValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'file' => [
                'required',
                'mimes:xls,csv,tsv,xlsx'
            ]
        ];
    }

    public function messages(): array
    {
        return [];
    }
}