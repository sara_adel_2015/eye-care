<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin;


use App\Entities\Setting;
use App\Http\Validators\BaseRequestValidator;

class UpdateSettingsValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        $rules = [
            'id' => 'exists:' . Setting::class . ',id',
            'allowedCompareRatio' => 'required|string',
            'allowedSimilitryPercentage' => 'required',
            'matchedResultReception' => 'required',
            //'notificationEmailNotMatched' => 'required',
        ];
        if ($this->request->get('differentResultReception') == 'custom') {
            $rules['differentResultReceptionEmail'] = 'required';
        }
        return $rules;
    }

    //put your code here
    public function messages(): array
    {
        return [];
    }
}