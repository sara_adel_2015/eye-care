<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\EyeTest;


use App\Entities\Patient;
use App\Http\Validators\BaseRequestValidator;

class AddEyeTestValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'patientId' => [
                'required',
                'exists:' . Patient::class . ',id',
            ],
            'imageType' => [
                'required',
            ],
            'imageFile' => [
                'required',
                'image',
                'mimes:jpeg,jpg,png,gif'
            ]
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [];
    }
}