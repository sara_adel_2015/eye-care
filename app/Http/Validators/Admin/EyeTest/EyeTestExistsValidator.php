<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\EyeTest;


use App\Entities\EyeTest;
use App\Http\Validators\BaseRequestValidator;

class EyeTestExistsValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'id' => 'exists:' . EyeTest::class . ',id',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
        ];
    }
}