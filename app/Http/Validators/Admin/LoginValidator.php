<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin;

use App\Http\Validators\BaseRequestValidator;
use App\Entities\Patient;

class LoginValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'email' => 'required|string',
            'password' => 'required|string',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [];

    }
}