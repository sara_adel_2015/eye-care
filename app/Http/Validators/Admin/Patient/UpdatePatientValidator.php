<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\Patient;


use App\Entities\Doctor;
use App\Http\Validators\BaseRequestValidator;
use App\Entities\Patient;

class UpdatePatientValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'id' => 'exists:' . Patient::class . ',id',
            'firstName' => 'required|string',
            'lastName' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'address' => 'nullable|string',
            'city' => 'nullable|string',
            'state' => 'nullable|string',
            'zip' => 'required',
            'phone' => 'required',
            'doctorId' => ['required', 'exists:' . Doctor::class . ',id'],

        ];
    }

    public function messages(): array
    {
        return [];
    }
}