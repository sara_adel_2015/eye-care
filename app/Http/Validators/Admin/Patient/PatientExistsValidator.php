<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\Patient;


use App\Http\Validators\BaseRequestValidator;
use App\Entities\Patient;

class PatientExistsValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'id' => 'exists:' . Patient::class . ',id',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
        ];
    }
}