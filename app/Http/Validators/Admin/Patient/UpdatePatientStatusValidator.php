<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\Patient;


use App\Entities\Patient;
use App\Enums\PatientStatusEnum;
use App\Http\Validators\BaseRequestValidator;

class UpdatePatientStatusValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'id' => 'exists:' . Patient::class . ',id',
            'status' => 'required|in:' . implode(',', PatientStatusEnum::keys()),
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [
            'id.exists' => 'Id not exists before',
            'status.required'=>'Invalid status'
        ];
    }
}