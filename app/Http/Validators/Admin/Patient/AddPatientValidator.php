<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:10 AM
 */

namespace App\Http\Validators\Admin\Patient;


use App\Http\Validators\BaseRequestValidator;

class AddPatientValidator extends BaseRequestValidator
{
    public function rules(): array
    {
        return [
            'firstName' => 'required|string',
            'lastName' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'phone' => 'required',
        ];
    }

    //put your code here
    public function messages(): array
    {
        return [];
    }
}