<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:09 AM
 */

namespace App\Http\Validators;


use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

abstract class BaseRequestValidator
{

    protected $request;

    /**
     * @param Request $request
     * @return array|bool
     */
    public function validate(Request $request)
    {
        $this->request = $request;
        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if ($validator->fails()) {
            return (['error'=>$validator->errors()]);
        }

        return false;
    }

    /*
     * @return type
     */
    public function messages()
    {
        return [];
    }

}
