<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\B2c;


use App\Http\Controllers\BaseController;
use App\Services\B2c\DictionaryDoctorService;
use App\Services\B2c\DoctorService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AdminController extends BaseController
{
    public function __construct(DictionaryDoctorService $service)
    {
        parent::__construct($service);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function listAll(Request $request)
    {
        return $this->service->getDoctorsForApi();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getDoctors(Request $request)
    {
        $doctors = $this->service->getDoctorsForApi();
        return view('b2c/home-doctor-list', ['doctors' => $doctors]);

    }

    public function contactUs(Request $request)
    {
        $when = now()->addMinutes(env('SEND_MAIL_QUEUE_EVERY_MINUTES', 10));
        $fullName = $request->get('fullname');
        $email = $request->get('email');
        $subject = $request->get('subject');
        $comment = $request->get('comment');
        $to = env('CONTACT_US_EMAIL', 'eyecareapp@gmail.com');
        Mail::to($to)->later($when, new \App\Mail\ContactUs($fullName, $email, $subject, $comment));
        return $this->formatSuccessResponse('mailsent');

    }

    public function getLearnContent(Request $request)
    {
        return view('b2c/cms/learn');
    }

    public function getPrivacyContent(Request $request)
    {
        return view('b2c/cms/privacy');
    }

    public function getTermsContent(Request $request)
    {
        return view('b2c/cms/terms');
    }

    public function getDoctorTermsContent(Request $request)
    {
        return view('b2c/cms/doctor-terms');
    }


}