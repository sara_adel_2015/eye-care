<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\B2c\Doctor;


use App\Entities\Patient;
use App\Http\Controllers\BaseController;
use App\Http\Validators\B2c\Doctor\UpdateDefaultPaymentCardValidator;
use App\Http\Validators\B2c\Doctor\UpdateDoctorValidator;
use App\Http\Validators\B2c\Doctor\ChangePasswordValidator;
use App\Services\B2c\DoctorService;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use LaravelDoctrine\ORM\Facades\EntityManager;


class DoctorController extends BaseController
{
    public function __construct(DoctorService $service)
    {
        parent::__construct($service);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        /**
         * @var $user Doctor
         */
        $user = $this->service->repo->find(Auth::id());
        return view('b2c/doctor/edit-profile', ['doctor' => $user]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {
        $request->request->add(['email' => Auth::user()->getEmail()]);//because email is readonly
        $request->request->add(['id' => Auth::user()->getId()]);
        $errors = (new UpdateDoctorValidator())->validate($request);
        if ($errors) {
            $this->addFlashErrorMessage(implode(', ', $errors));
            return redirect()->route('doctor-edit-profile-form');
        }
        $this->service->updateDoctor($request->except(['Submit', '_token', '_method']), 'update');
        $this->addFlashSuccessMessage('Record has been updated sucessfully');
        return redirect()->route('doctor-edit-profile-form');
    }

    public function showChangePasswordForm(Request $request)
    {
        return view('b2c.doctor.change-password', ['url' => 'doctor']);
    }

    public function changePassword(Request $request)
    {
        $errors = (new ChangePasswordValidator())->validate($request);
        if ($errors) {
            $this->addFlashErrorMessage(implode(', ', $errors));
        } elseif (!(Hash::check($request->get('current-password'), Auth::user()->getPassword()))) {
            // The passwords matches
            $this->addFlashErrorMessage('Your current password does not matches with the password you provided. Please try again.');
        } elseif (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            $this->addFlashErrorMessage('New Password cannot be same as your current password. Please choose a different password.');
        } else {
            $this->service->changePassword($request->user(), $request->get('new-password'));
            $this->addFlashSuccessMessage('Password Changed Successfully, Check your email');
            return redirect()->route('doctor-login-form');
        }
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('doctor-login-form');
    }

    public function updateDefaultPaymentCard(Request $request)
    {
        $errors = (new UpdateDefaultPaymentCardValidator())->validate($request);
        if ($errors) {
            $this->addFlashErrorMessage(implode(', ', $errors));
        }
        $response = $this->service->updateDefaultPaymentCard($request->get('paymentCardId'));
        if ($response['error'] == '1') {
            $this->addFlashErrorMessage($response['message']);

        } else {
            $this->addFlashSuccessMessage('Current Payment Card has been updated sucessfully');

        }
        return redirect()->route('doctor-payment-cards');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getPatients(Request $request)
    {

        /**
         * @var $user Doctor
         */
        $patientRepo = EntityManager::getRepository(Patient::class);
        $patients = $patientRepo->getDoctorPatients(Auth::id(), 1, $request->search);
        return view('b2c/doctor/patient-list', ['patients' => $patients]);
    }
}