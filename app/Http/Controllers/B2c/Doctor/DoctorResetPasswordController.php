<?php

namespace App\Http\Controllers\B2c\Doctor;

use App\Http\Controllers\BaseController;
use App\Services\B2c\DoctorService;
use Illuminate\Http\Request;
use Auth;

class DoctorResetPasswordController extends BaseController

{
    /**
     * DoctorResetPasswordController constructor.
     * @param DoctorService $service
     */
    public function __construct(DoctorService $service)
    {
        parent::__construct($service);
    }

    public function execute(Request $request)
    {
        $errors = (new \App\Http\Validators\B2c\Doctor\ResetPasswordValidator())->validate($request);
        if ($errors) {
            $this->addFlashErrorMessage(implode(',', $errors));
            return redirect()->route('doctor-reset-password-form');
        } else {
            $doctorEntity = $this->service->repo->findOneByEmail($request->email);
            $this->service->resetPassword($doctorEntity);
            $this->addFlashSuccessMessage("Password is changed successfully, check your email");
            return redirect()->route('doctor-login-form');
        }
    }

    public function getResetPasswordForm(Request $request)
    {
        return view('b2c/doctor/reset-password');

    }

}
