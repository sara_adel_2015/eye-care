<?php

namespace App\Http\Controllers\B2c\Doctor;

use App\Http\Controllers\BaseController;
use App\Http\Validators\B2c\Doctor\RegisterDoctorValidator;
use App\Services\Admin\AdminService;
use App\Services\B2c\DoctorService;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

class DoctorRegisterController extends BaseController
{

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/doctor';

    public function __construct(DoctorService $service)
    {
        /**
         * @var $service DoctorService
         */
        $this->service = $service;
        $this->middleware(['guest'])->except('logout');
    }


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegisterForm()
    {
        $stripeApiKey = env('STRIPE_API_PUBLIC_KEY');
        $adminService = App::make(AdminService::class);
        $settings=$adminService->getSettings()['settings'];
        $subscriptionAmount=$settings->getDoctorChargeRecurringAmount();
        return view('b2c.doctor.register', [
            'url' => 'doctor',
            'apiKey' => $stripeApiKey,
            'subscriptionAmount' => $subscriptionAmount
        ]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Symfony\Component\CssSelector\Exception\InternalErrorException
     */
    public function register(Request $request)
    {
        $errors = (new RegisterDoctorValidator())->validate($request);
        if (!empty($errors)) {
            $this->addFlashErrorMessage(implode(', ', $errors));
            return redirect()->back()->withInput($request->input());
        } else {
            $response = $this->service->addDoctor($request->all());
            if ($response['error'] == '1') {
                $this->addFlashErrorMessage($response['message']['stripeError']);
                return redirect()->back()->withInput($request->input());
            } else {
                $this->addFlashSuccessMessage("Doctor was registered successfully,Please login to continue");
                return redirect()->route('doctor-login');
            }
        }

    }


}
