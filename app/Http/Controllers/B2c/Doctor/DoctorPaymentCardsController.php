<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 5/25/2020
 * Time: 11:24 PM
 */

namespace App\Http\Controllers\B2c\Doctor;


use App\Entities\Doctor;
use App\Http\Controllers\BaseController;
use App\Http\Validators\B2c\Doctor\AddNewPaymentCardValidator;
use App\Services\B2c\DoctorPaymentCardsService;
use Illuminate\Http\Request;
use Auth;
use LaravelDoctrine\ORM\Facades\EntityManager;

class DoctorPaymentCardsController extends BaseController
{
    public function __construct(DoctorPaymentCardsService $service)
    {
        parent::__construct($service);
    }

    public function getPaymentCards(Request $request)
    {
        $paymentCards = Auth::user()->getPaymentCards();
        $currentPaymentCard = Auth::user()->getCurrentPaymentCardId();
        return view('b2c/doctor/payment-cards',
            [
                'paymentCards' => $paymentCards,
                'currentPaymentCard' => $currentPaymentCard
            ]);
    }

    public function deletePaymentCard(Request $request)
    {
        dD("here I will delete the card");
    }

    public function showAddNewPaymentCardForm(Request $request)
    {
        $stripeApiKey = env('STRIPE_API_PUBLIC_KEY');
        return view('b2c.doctor.add-payment-card',
            [
                'url' => 'doctor',
                'apiKey' => $stripeApiKey,
            ]);
    }

    public function saveNewPaymentCard(Request $request)
    {
        $errors = (new AddNewPaymentCardValidator())->validate($request);
        if (!empty($errors)) {
            $this->addFlashErrorMessage(implode(', ', $errors));
            return redirect()->back()->withInput($request->input());
        } else {
            $response = $this->service->addNewCard($request->all());
            if ($response['error'] == '1') {
                $this->addFlashErrorMessage($response['message']);
                return redirect()->back()->withInput($request->input());
            } else {
                $this->addFlashSuccessMessage("New Payment Card was added successfully");
                return redirect()->route('doctor-payment-cards');
            }
        }
    }

    public function getPaymentHistory(Request $request, $id)
    {
        $history = $this->service->getPaymentCardHistory($id);
        return view('b2c/doctor/payment-cards-history',
            [
                'list' => $history,
            ]
        );
    }

    public function requestCancelSubscription(Request $request)
    {
        /**
         * @var $doctor Doctor
         */
        $doctor = $request->user();
        $doctor->setIsRequestToCancelSubscription(true);
        EntityManager::flush();
        $this->addFlashSuccessMessage("Successfully Send Request to cancel the subscription");
        return redirect()->route('doctor-payment-cards');
    }

}