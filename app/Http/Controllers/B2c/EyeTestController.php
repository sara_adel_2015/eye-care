<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\B2c;

use App\Http\Controllers\BaseController;
use App\Http\Validators\Admin\EyeTest\EyeTestExistsValidator;
use App\Services\B2c\EyeTestService;
use Illuminate\Http\Request;

class EyeTestController extends BaseController
{
    public function __construct(EyeTestService $service)
    {
        parent::__construct($service);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function listAll(Request $request)
    {
        $pageNo = ($request->page) ? $request->page : 1;
        $list = $this->service->getAllEyeTest((int)$pageNo);
        return view('b2c/eye-test/list', $list);
    }

    public function delete(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new EyeTestExistsValidator())->validate($request);
        if ($errors) {
            $message = "Error deleting eyetest Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
            return $this->formatSuccessResponse(false);
        } else {
            $this->service->delete($id);
            $this->addFlashSuccessMessage('Record has been deleted sucessfully');
            return $this->formatSuccessResponse(true);
        }

    }

    public function download(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new EyeTestExistsValidator())->validate($request);
        if ($errors) {
            $message = "Error downloading file Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
            return redirect()->back();
        } else {
            $eyeTest = $this->service->repo->find($id);
            return response()->download(public_path() . '/admin/uploads/eyecareImages/' . $eyeTest->getImageName());
        }
    }

}