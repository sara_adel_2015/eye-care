<?php

namespace App\Http\Controllers\B2c\Patient;

use App\Http\Controllers\BaseController;
use App\Services\B2c\PatientService;
use Illuminate\Http\Request;
use Auth;

class PatientResetPasswordController extends BaseController

{
    /**
     * PatientResetPasswordController constructor.
     * @param PatientService $service
     */
    public function __construct(PatientService $service)
    {
        parent::__construct($service);
    }

    public function execute(Request $request)
    {
        $errors = (new \App\Http\Validators\B2c\Patient\ResetPasswordValidator())->validate($request);
        if ($errors) {
            $this->addFlashErrorMessage(implode(',', $errors));
            return redirect()->route('patient-reset-password-form');
        } else {
            $patientEntity = $this->service->repo->findOneByEmail($request->email);
            $this->service->resetPassword($patientEntity);
            $this->addFlashSuccessMessage("Password is changed successfully, check your email");
            return redirect()->route('patient-login-form');
        }
    }

    public function getResetPasswordForm(Request $request)
    {
        return view('b2c/patient/reset-password');

    }

}
