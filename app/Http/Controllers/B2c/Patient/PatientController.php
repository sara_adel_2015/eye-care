<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\B2c\Patient;


use App\Entities\Patient;
use App\Enums\DoctorStatusEnum;
use App\Http\Controllers\BaseController;
use App\Http\Validators\B2c\Patient\UpdatePatientValidator;
use App\Services\B2c\DoctorService;
use App\Services\B2c\PatientService;
use Illuminate\Http\Request;
use Auth;

class PatientController extends BaseController
{
    public function __construct(PatientService $service)
    {
        parent::__construct($service);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function edit(Request $request)
    {
        /**
         * @var $user Patient
         */
        $user = $this->service->repo->find(Auth::id());
        $doctorList = app(DoctorService::class)->repo->findBy(['status' => DoctorStatusEnum::ACTIVE]);
        return view('b2c/patient/edit-profile', ['user' => $user, 'doctors' => $doctorList]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {
        $request->request->add(['email' => Auth::user()->getEmail()]);//because email is readonly
        $request->request->add(['id' => Auth::user()->getId()]);
        $errors = (new UpdatePatientValidator())->validate($request);
        if ($errors) {
            $this->addFlashErrorMessage(implode(', ', $errors));
            return redirect()->route('patient-edit-profile-form');
        }
        $this->service->addOrUpdatePatient($request->except(['Submit', '_token', '_method']), 'update');
        $this->addFlashSuccessMessage('Record has been updated sucessfully');
        return redirect()->route('patient-edit-profile-form');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('patient-login-form');
    }
}