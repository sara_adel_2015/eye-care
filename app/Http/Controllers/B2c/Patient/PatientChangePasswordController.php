<?php

namespace App\Http\Controllers\B2c\Patient;

use App\Http\Controllers\BaseController;
use App\Services\B2c\PatientService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Http\Validators\B2c\Patient\ChangePasswordValidator;

class PatientChangePasswordController extends BaseController
{


    /**
     * PatientChangePasswordController constructor.
     * @param PatientService $service
     */
    public function __construct(PatientService $service)
    {
        parent::__construct($service);
    }

    public function showChangePasswordForm(Request $request)
    {
        return view('b2c.patient.change-password', ['url' => 'patient']);
    }

    public function changePassword(Request $request)
    {
        $errors = (new ChangePasswordValidator())->validate($request);
        if ($errors) {
            $this->addFlashErrorMessage(implode(', ', $errors));
        } elseif (!(Hash::check($request->get('current-password'), Auth::user()->getPassword()))) {
            // The passwords matches
            $this->addFlashErrorMessage('Your current password does not matches with the password you provided. Please try again.');
        } elseif (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            $this->addFlashErrorMessage('New Password cannot be same as your current password. Please choose a different password.');
        } else {
            $this->service->changePassword($request->user(), $request->get('new-password'));
            $this->addFlashSuccessMessage('Password Changed Successfully');
            return redirect()->route('patient-login-form');
        }
        return redirect()->back();
    }
}
