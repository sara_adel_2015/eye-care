<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\Admin;

use App\Entities\Doctor;
use App\Enums\DoctorStatusEnum;
use App\Http\Controllers\BaseController;
use App\Http\Validators\Admin\DictionaryDoctor\AddDoctorValidator;
use App\Http\Validators\Admin\DictionaryDoctor\CheckAddressValidator;
use App\Http\Validators\Admin\DictionaryDoctor\DoctorExistsValidator;
use App\Http\Validators\Admin\DictionaryDoctor\UpdateDoctorStatusValidator;
use App\Http\Validators\Admin\DictionaryDoctor\UpdateDoctorValidator;
use App\Imports\DoctorsImport;
use App\Services\Admin\DictionaryDoctorService;
use Illuminate\Http\Request;
use App\Http\Validators\Admin\DictionaryDoctor\UpdateDoctorOrderValidator;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Validators\Admin\DictionaryDoctor\ImportDoctorSheetValidator;
use App;

class DictionaryDoctorController extends BaseController
{
    public function __construct(DictionaryDoctorService $service)
    {
        parent::__construct($service);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function listAll(Request $request)
    {
        $doctors = $this->service->getDoctors($request->page);
        return view('admin/doctor-dictionary/doctor-list', $doctors);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $doctor = new Doctor();
        return view('admin/doctor-dictionary/doctor-add', ['doctor' => $doctor, 'action' => 'add']);
    }

    public function create(Request $request)
    {
        try {
            $errors = (new AddDoctorValidator())->validate($request);
            if ($errors) {
                $errorMsg = implode(",\n", $errors);
                $this->addFlashErrorMessage($errorMsg);
                return redirect()->back();
            } else {
                $this->service->addOrUpdateDoctor($request->except(['Submit', '_token']), 'add');
                $this->addFlashSuccessMessage('Record has been inserted sucessfully');

            }
        } catch (\Exception $ex) {
            $this->addFlashErrorMessage($ex->getMessage());
        }
        return redirect()->route('dictionary-doctor-list');
    }

    public function updateOrder(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new UpdateDoctorOrderValidator())->validate($request);
        if ($errors) {
            $message = "Error Updating Order Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
        } else {
            $this->service->updateStrOrder($id, $request['strOrder']);
            $this->addFlashSuccessMessage("Status has been updated sucessfully");
        }
        return $this->formatSuccessResponse(true);
    }

    public function updateStatus(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new UpdateDoctorStatusValidator())->validate($request);
        if ($errors) {
            $message = "Error Updating Status Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
            return $this->formatSuccessResponse(false);
        } else {
            $statusKey = $request['status'];
            $this->service->updateStatus($id, DoctorStatusEnum::$statusKey());
            $this->addFlashSuccessMessage('Sort ordering has been updated sucessfully');
            return $this->formatSuccessResponse(true);
        }

    }

    public function updateOrders(Request $request)
    {
        if (!empty($request->doctorsOrders)) {
            foreach ($request->doctorsOrders as $doctorsOrder) {
                $this->service->updateStrOrder($doctorsOrder['id'], $doctorsOrder['strOrder']);
            }
            $this->addFlashSuccessMessage("Status has been updated sucessfully");
            return $this->formatSuccessResponse(true);
        } else {
            return $this->formatSuccessResponse(false);
        }
    }

    public function delete(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new DoctorExistsValidator())->validate($request);
        if ($errors) {
            $message = "Error deleting doctor Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
            return $this->formatSuccessResponse(false);
        } else {
            $response = $this->service->delete($id);
            if ($response['error'] == '0') {
                $this->addFlashSuccessMessage($response['message']);
                return $this->formatSuccessResponse(true);
            } else {
                return $this->formatErrorResponse([$response['message']]);
            }


        }

    }

    public function edit(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new DoctorExistsValidator())->validate($request);
        if ($errors) {
            $this->addFlashErrorMessage("Not Valid ID");
            return redirect()->route('dictionary-doctor-list');
        } else {
            $doctor = $this->service->findDoctor($id);
            return view('admin/doctor-dictionary/doctor-add', ['doctor' => $doctor, 'action' => 'edit']);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $errors = (new UpdateDoctorValidator())->validate($request);
            if ($errors) {
                $this->addFlashErrorMessage(implode(', ', $errors));
                return redirect()->back();
            } else {
                $this->service->addOrUpdateDoctor($request->except(['Submit', '_token']), 'update');
                $this->addFlashSuccessMessage('Record has been updated sucessfully');

            }
        } catch (\Exception $ex) {
            $this->addFlashErrorMessage($ex->getMessage());
        }
        return redirect()->route('dictionary-doctor-list');
    }

    public function deleteImage(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new DoctorExistsValidator())->validate($request);
        if ($errors) {
            $message = "Error deleting doctor Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
            return $this->formatSuccessResponse(false);
        } else {
            $isDeleted = $this->service->deleteDoctorImage($id);
            $this->addFlashSuccessMessage('Image has been deleted sucessfully');
            return $this->formatSuccessResponse($isDeleted);
        }
    }

    public function checkAddress(Request $request)
    {
        $errors = (new CheckAddressValidator())->validate($request);
        if ($errors) {
            $message = "Error deleting doctor Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
            return $this->formatSuccessResponse(false);
        } else {
            $address = str_replace('|', '+', $request->address);
            $response = $this->service->checkAddress($address);
            if ($response->results) {
                return $this->formatSuccessResponse($response);
            } else {
                $code = ($response->status) == 'REQUEST_DENIED' ? 403 : 400;
                return $this->formatErrorResponse([$response->error_message], $code);
            }
        }
    }

    public function uploadSheet(Request $request)
    {
        return view('admin/doctor-dictionary/doctor-upload-sheet');

    }

    public function import(Request $request)
    {
        $errors = (new ImportDoctorSheetValidator())->validate($request);
        if ($errors) {
            $errorMsg = implode(",\n", $errors);
            $this->addFlashErrorMessage($errorMsg);
            return redirect()->route('dictionary-doctor-upload-sheet');
        } else {
            $importClass = new DoctorsImport();
            $importClass->importFileName = $request->file('file')->getClientOriginalName();
            $importClass->importFileExtension = $request->file('file')->getClientOriginalExtension();
            try {
                $request['file']->storeAs($importClass->importFolderName, $importClass->importFileName, 'uploads');
                Excel::import($importClass, $importClass->importFolderName . '/' . $importClass->importFileName, 'uploads', \Maatwebsite\Excel\Excel::XLSX);
                if ($importClass->isFileEmpty) {
                    $this->addFlashErrorMessage('File Is Empty');
                    return redirect()->route('dictionary-doctor-upload-sheet');
                } else if (!$importClass->isHeaderValid) {
                    $this->addFlashErrorMessage('Heading sheet is not valid, Please make sure the file contains the following header ' . implode(', ', $importClass->headings()));
                    return redirect()->route('dictionary-doctor-upload-sheet');
                } else {
                    $this->addFlashSuccessMessage('Data has been inserted successfully');
                    if ($importClass->isExportFile) {
                        return response()->download(public_path() . '/admin/uploads/' . $importClass->exportFilePath);
                    }
                }
            } catch (\Exception $e) {
                $this->addFlashErrorMessage('Some thing happened while importing file with reason ' . $e->getMessage() . ' Please make sure the file contains the following header ' . implode(', ', $importClass->headings()));
                return redirect()->route('doctor-upload-sheet');
            }

            return redirect()->route('dictionary-doctor-list');
        }
    }
}