<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\Admin;

use App\Entities\Doctor;
use App\Http\Controllers\BaseController;
use App\Http\Validators\Admin\EyeTest\AddEyeTestValidator;
use App\Http\Validators\Admin\EyeTest\EyeTestExistsValidator;
use App\Services\Admin\EyeTestService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EyeTestController extends BaseController
{
    public function __construct(EyeTestService $service)
    {
        parent::__construct($service);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function listAll(Request $request)
    {
        $pageNo = ($request->page) ? $request->page : 1;
        $list = $this->service->getAllEyeTest($request->patientId, (int)$pageNo);
        return view('admin/eye-test/list', $list);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        return view('admin/eye-test/add');
    }

    public function create(Request $request)
    {
        try {
            $errors = (new AddEyeTestValidator())->validate($request);
            if ($errors) {
                $errorMsg = implode(",\n", $errors);
                $this->addFlashErrorMessage($errorMsg);
                return redirect()->back();
            } else {
                $this->service->addEyeTest($request->except(['Submit', '_token']));
                $this->addFlashSuccessMessage('Record has been inserted sucessfully');

            }
        } catch (\Exception $ex) {
            $this->addFlashErrorMessage($ex->getMessage());
        }
        return redirect()->route('eye-test-list');
    }


    public function delete(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new EyeTestExistsValidator())->validate($request);
        if ($errors) {
            $message = "Error deleting eyetest Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
            return $this->formatSuccessResponse(false);
        } else {
            $this->service->delete($id);
            $this->addFlashSuccessMessage('Record has been deleted sucessfully');
            return $this->formatSuccessResponse(true);
        }

    }

    public function download(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new EyeTestExistsValidator())->validate($request);
        if ($errors) {
            $message = "Error downloading file Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
            return redirect()->back();
        } else {
            $eyeTest = $this->service->repo->find($id);
            return response()->download(public_path() . '/admin/uploads/eyecareImages/' . $eyeTest->getImageName());
        }

    }

}