<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\Admin;

use App\Entities\Patient;
use App\Enums\DoctorStatusEnum;
use App\Enums\PatientStatusEnum;
use App\Http\Controllers\BaseController;
use App\Http\Validators\Admin\Patient\UpdatePatientValidator;
use App\Http\Validators\Admin\Patient\AddPatientValidator;
use App\Http\Validators\Admin\Patient\PatientExistsValidator;
use App\Http\Validators\Admin\Patient\UpdatePatientStatusValidator;
use App\Services\Admin\PatientService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Services\Admin\DoctorService;

class PatientController extends BaseController
{
    public function __construct(PatientService $service)
    {
        parent::__construct($service);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function listAll(Request $request)
    {
        $patients = $this->service->getPatients($request->search, $request->page);
        return view('admin/patient/patient-list', $patients);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getPatientsForAutocomplete(Request $request)
    {
        $jsonList = [];
        $patients = $this->service->getPatients($request->term);
        if ($patients['count'] > 0) {
            $jsonList = array_map(function (Patient $patient) {
                return [
                    'label' => $patient->getFirstName() . ' ' . $patient->getLastName(),
                    'value' => $patient->getId()
                ];
            }, $patients['patients']->items());
        }
        return (new JsonResponse($jsonList, 200))->header('Accept-encoding', ['gzip', 'deflate']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $patient = new Patient();
        $doctorList = app(DoctorService::class)->repo->findBy(['status' => DoctorStatusEnum::ACTIVE]);
        return view('admin/patient/patient-add', ['patient' => $patient, 'action' => 'add', 'doctors' => $doctorList]);
    }

    public function create(Request $request)
    {
        try {
            $errors = (new AddPatientValidator())->validate($request);
            if ($errors) {
                return $this->formatErrorResponse($errors);
            } else {
                $this->service->addOrUpdatePatient($request->except(['Submit', '_token']), 'add');
                $this->addFlashSuccessMessage('Record has been inserted sucessfully');

            }
        } catch (\Exception $ex) {
            $this->addFlashErrorMessage($ex->getMessage());
        }
        return redirect()->route('patient-list');
    }

    public function updateStatus(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new UpdatePatientStatusValidator())->validate($request);
        if ($errors) {
            $message = "Error Updating Status Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
            return $this->formatSuccessResponse(false);
        } else {
            $statusKey = $request['status'];
            $this->service->updateStatus($id, PatientStatusEnum::$statusKey());
            $this->addFlashSuccessMessage('Sort ordering has been updated sucessfully');
            return $this->formatSuccessResponse(true);
        }

    }

    public function delete(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $errors = (new PatientExistsValidator())->validate($request);
        if ($errors) {
            $message = "Error deleting patient Reason: " . $this->formateValidationMessage($errors);
            $this->addFlashErrorMessage($message);
            return $this->formatSuccessResponse(false);
        } else {
            $this->service->delete($id);
            $this->addFlashSuccessMessage('Record has been deleted sucessfully');
            return $this->formatSuccessResponse(true);
        }

    }

    public function edit(Request $request, $id)
    {
        $request->request->add(['id' => $id]);
        $doctorList = app(DoctorService::class)->repo->findBy(['status' => DoctorStatusEnum::ACTIVE]);
        $errors = (new PatientExistsValidator())->validate($request);
        if ($errors) {
            $this->addFlashErrorMessage("Not Valid ID");
            return redirect()->route('patient-list');
        } else {
            $patient = $this->service->findPatient($id);
            return view('admin/patient/patient-add', ['patient' => $patient, 'action' => 'edit', 'doctors' => $doctorList]);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $request->request->add(['id' => $id]);
            $errors = (new UpdatePatientValidator())->validate($request);
            if ($errors) {
                $errorMsg = implode(",\n", $errors);
                $this->addFlashErrorMessage($errorMsg);
                return redirect()->route('patient-edit', ['id' => $id]);
            } else {
                $this->service->addOrUpdatePatient($request->except(['Submit', '_token']), 'update');
                $this->addFlashSuccessMessage('Record has been updated sucessfully');

            }
        } catch (\Exception $ex) {
            $this->addFlashErrorMessage($ex->getMessage());
        }
        return redirect()->route('patient-list');
    }
}