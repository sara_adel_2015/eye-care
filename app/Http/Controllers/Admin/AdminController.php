<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\Admin;


use App\Entities\Admin;
use App\Entities\Setting;
use App\Enums\DoctorStatusEnum;
use App\Http\Controllers\BaseController;
use App\Http\Validators\Admin\AddSettingsValidator;
use App\Http\Validators\Admin\UpdateSettingsValidator;
use App\Services\Admin\AdminService;
use App\Services\Admin\DoctorService;
use App\Services\Admin\SubscriptionSettingsService;
use Illuminate\Http\Request;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Auth;
use App\Http\Validators\Admin\ChangePasswordValidator;
use Illuminate\Support\Facades\Hash;

class AdminController extends BaseController
{
    public function __construct(AdminService $service)
    {
        parent::__construct($service);
    }

    public function viewSettings(Request $request)
    {
        $doctorList = app(DoctorService::class)->repo->findBy(['status' => DoctorStatusEnum::ACTIVE]);
        $settings = $this->service->getSettings();
        $settings['doctors'] = $doctorList;
        return view('admin/settings', $settings);
    }

    public function createSettings(Request $request)
    {
        try {
            $errors = (new AddSettingsValidator())->validate($request);
            if ($errors) {
                return $this->formatErrorResponse($errors);
            } else {
                $settings = $this->service->addOrUpdateSettings($request->except(['Submit', '_token']), 'add');
                $this->addFlashSuccessMessage('Record has been inserted sucessfully');

            }
        } catch (\Exception $ex) {
            $this->addFlashErrorMessage($ex->getMessage());
        }
        return view('admin/settings', ['settings' => $settings, 'action' => 'add']);
    }

    public function updateSettings(Request $request, $id)
    {
        $doctorList = app(DoctorService::class)->repo->findBy(['status' => DoctorStatusEnum::ACTIVE]);
        $settings = new Setting();
        try {
            $request->request->add(['id' => $id]);
            $errors = (new UpdateSettingsValidator())->validate($request);
            if ($errors) {
                return $this->formatErrorResponse($errors);
            } else {
                $settings = $this->service->addOrUpdateSettings($request->except(['Submit', '_token']), 'update');
                $this->addFlashSuccessMessage('Record has been updated sucessfully');
            }
        } catch (\Exception $ex) {
            $this->addFlashErrorMessage($ex->getMessage());
            redirect()->route('admin-settings');
        }
        return view('admin/settings', ['settings' => $settings, 'doctors' => $doctorList, 'action' => 'update']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request)
    {
        /**
         * @var $admin Admin
         */
        $admin = Auth::user();
        $admin->setLastVisited(new \DateTime());
        EntityManager::flush();
        Auth::logout();
        return redirect()->route('admin-login-form');
    }

    public function showChangePasswordForm(Request $request)
    {
        return view('admin.change-password', ['url' => 'admin']);
    }

    public function changePassword(Request $request)
    {
        $errors = (new ChangePasswordValidator())->validate($request);
        if ($errors) {
            $this->addFlashErrorMessage(implode(', ', $errors));
        } elseif (!(Hash::check($request->get('current-password'), Auth::user()->getPassword()))) {
            // The passwords matches
            $this->addFlashErrorMessage('Your current password does not matches with the password you provided. Please try again.');
        } elseif (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            $this->addFlashErrorMessage('New Password cannot be same as your current password. Please choose a different password.');
        } else {
            $this->service->changePassword($request->user(), $request->get('new-password'));
            $this->logout($request);
            $this->addFlashSuccessMessage('Password Changed Successfully');
            return redirect()->route('admin-login-form');
        }
        return redirect()->back();
    }

    public function viewSubscriptionSettings(Request $request)
    {
        $settings = $this->service->getSettings();
        return view('admin/subscription-settings-view', $settings);
    }

    public function updateSubscriptionSettings(Request $request, $id)
    {
        try {
            $response = app(SubscriptionSettingsService::class)->updateSubscriptionSettings($id, $request->except(['Submit', '_token']));
            if (!empty($response['updatedDoctors'])) {
                $contents = implode("\r\n", $response['updatedDoctors']);
                $filename = 'updated-doctor-subscriptions-report.txt';
                return response()->streamDownload(function () use ($contents) {
                    echo $contents;
                }, $filename);
            }
            if ($response['error'] == '1') {
                $this->addFlashErrorMessage($response['message']);
            } else {
                $this->addFlashSuccessMessage(implode('\n', $response['updatedDoctors']));
            }
        } catch (\Exception $ex) {
            $this->addFlashErrorMessage($ex->getMessage());
        }
        return redirect()->route('view-subscription-settings');
    }
}