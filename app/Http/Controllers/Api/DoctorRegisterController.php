<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\BaseController;
use App\Http\Validators\B2c\Doctor\RegisterDoctorValidator;
use App\Services\B2c\DoctorService;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;

/**
 * Class PatientController
 * @package App\Http\Controllers\Api
 */
class DoctorRegisterController extends ApiBaseController
{
    use RegistersUsers;

    public function __construct(DoctorService $service)
    {
        parent::__construct($service);
        $this->middleware('guest');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $errors = (new RegisterDoctorValidator())->validate($request);
        if ($errors) {
            return $this->formatErrorResponse($errors);
        } else {
            $doctor = $this->service->addDoctor($request->all());
            if ($doctor['error'] == '0') {
                return $this->formatSuccessResponse($doctor['doctor']);
            } else {
                return $this->formatErrorResponse([$doctor['message']]);
            }

        }
    }


}