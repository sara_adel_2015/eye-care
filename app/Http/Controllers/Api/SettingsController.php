<?php

namespace App\Http\Controllers\Api;

use App\Entities\Patient;
use App\Enums\PatientStatusEnum;
use App\Services\Admin\AdminService;
use App\Services\B2c\PatientService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class SettingsController extends ApiBaseController
{


    /**
     * SettingsController constructor.
     * @param AdminService $service
     */
    public function __construct(AdminService $service)
    {
        parent::__construct($service);
    }

    public function getSettings()
    {
        $settings = $this->service->getSettings()['settings'];
        $doctorChargeRecurringAmount = $settings->getDoctorChargeRecurringAmount();
        $defaultPhysician = $settings->getDefaultPhysician();
        $dataFormatted = [
            'doctorChargeRecurringAmount' => $doctorChargeRecurringAmount,
            'defaultPhysician' => $defaultPhysician,
            'STRIPE_PUBLIC_KEY' => env('STRIPE_API_PUBLIC_KEY')
        ];
        return $this->formatSuccessResponse($dataFormatted);
    }

}
