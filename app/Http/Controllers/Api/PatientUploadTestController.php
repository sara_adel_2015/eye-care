<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Validators\B2c\Patient\UploadEyeTestValidator;
use App\Services\B2c\PatientUploadEyeTestService;
use Illuminate\Http\Request;
use Auth;

class PatientUploadTestController extends ApiBaseController
{


    /**
     * PatientUploadTestController constructor.
     * @param PatientUploadEyeTestService $service
     */
    public function __construct(PatientUploadEyeTestService $service)
    {
        $this->middleware('auth-api:api');
        parent::__construct($service);
    }

    public function execute(Request $request)
    {
        $errors = (new UploadEyeTestValidator())->validate($request);
        if ($errors) {
            return $this->formatErrorResponse($errors);
        } else {
            $response = $this->service->execute($request->all(), $request->user());
            if (!empty($response['error']) && $response['error'] == '1') {
                unset($response['error']);
                return $this->formatErrorResponse($response['message']);
            } else {
                unset($response['error']);
                return $this->formatSuccessResponse($response);
            }
        }
    }
}
