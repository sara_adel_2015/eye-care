<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Validators\B2c\Patient\UpdatePatientValidator;
use App\Services\B2c\PatientService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Http\Validators\B2c\Patient\ChangePasswordValidator;

class PatientUpdateController extends ApiBaseController
{


    /**
     * PatientChangePasswordController constructor.
     * @param PatientService $service
     */
    public function __construct(PatientService $service)
    {
        $this->middleware('auth-api:api');
        parent::__construct($service);
    }

    public function execute(Request $request)
    {
        $errors = (new UpdatePatientValidator())->validate($request);
        if ($errors) {
            return $this->formatErrorResponse($errors);
        } else {
            $user = $this->service->addOrUpdatePatient($request->all(),'update');
            return $this->formatSuccessResponse($user);
        }
    }
}
