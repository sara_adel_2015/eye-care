<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Validators\B2c\Patient\ResetPasswordValidator;
use App\Services\B2c\PatientService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Http\Validators\B2c\Patient\ChangePasswordValidator;

class PatientResetPasswordController extends ApiBaseController
{


    /**
     * PatientChangePasswordController constructor.
     * @param PatientService $service
     */
    public function __construct(PatientService $service)
    {
        parent::__construct($service);
    }

    public function execute(Request $request)
    {
        $errors = (new ResetPasswordValidator())->validate($request);
        if ($errors) {
            return $this->formatErrorResponse($errors);
        } else {
            $patientEntity =$this->service->repo->findOneByEmail($request->email);
            $patient = $this->service->resetPassword($patientEntity);
            return $this->formatSuccessResponse($patient);
        }
    }
}