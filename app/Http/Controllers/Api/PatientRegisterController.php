<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\Api;


use App\Services\B2c\PatientService;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use App\Http\Validators\B2c\Patient\RegisterPatientValidator;
use Auth;

/**
 * Class PatientController
 * @package App\Http\Controllers\Api
 */
class PatientRegisterController extends ApiBaseController
{
    use RegistersUsers;

    public function __construct(PatientService $service)
    {
        parent::__construct($service);
        $this->middleware('guest');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $errors = (new RegisterPatientValidator())->validate($request);
        if ($errors) {
            return $this->formatErrorResponse($errors);
        } else {
            $user = $this->service->addOrUpdatePatient($request->all());
            event(new Registered($user));
            return $this->formatSuccessResponse($user);
        }
    }


}