<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:25 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\BaseController;
use App\Services\B2c\DoctorService;
use Illuminate\Http\Request;

class DoctorController extends ApiBaseController
{
    public function __construct(DoctorService $service)
    {
       // $this->middleware('auth-api:api');
        parent::__construct($service);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function listAll(Request $request)
    {
        return $this->formatSuccessResponse($this->service->getDoctorsForApi());
    }
}