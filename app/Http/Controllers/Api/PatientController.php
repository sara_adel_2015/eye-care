<?php

namespace App\Http\Controllers\Api;

use App\Entities\Patient;
use App\Enums\PatientStatusEnum;
use App\Services\B2c\PatientService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class PatientController extends ApiBaseController
{


    /**
     * PatientController constructor.
     * @param PatientService $service
     */
    public function __construct(PatientService $service)
    {
        $this->middleware('auth-api:api');
        parent::__construct($service);
    }

    public function getDetails()
    {
        $patient = $this->service->getDetails();
        return $this->formatSuccessResponse($patient);
    }

}
