<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Services\B2c\PatientService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Http\Validators\B2c\Patient\ChangePasswordValidator;

class PatientChangePasswordController extends ApiBaseController
{


    /**
     * PatientChangePasswordController constructor.
     * @param PatientService $service
     */
    public function __construct(PatientService $service)
    {
        $this->middleware('auth-api:api');
        parent::__construct($service);
    }

    public function execute(Request $request)
    {
        $errors = (new ChangePasswordValidator())->validate($request);
        if ($errors) {
            return $this->formatErrorResponse($errors);
        } elseif (!(Hash::check($request->get('current-password'), Auth::user()->getPassword()))) {
            // The passwords matches
            return $this->formatErrorResponse(["current-password" => 'Your current password does not matches with the password you provided. Please try again.']);
        } elseif (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return $this->formatErrorResponse(["new-password" => 'New Password cannot be same as your current password. Please choose a different password.']);
        } else {
            $patient = $this->service->changePassword($request->user(), $request->get('new-password'));
            return $this->formatSuccessResponse($patient);
        }
    }
}
