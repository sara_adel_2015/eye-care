<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 10:37 PM
 */

namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\BaseController;

abstract class ApiBaseController extends BaseController
{

    /**
     * @param array $errors
     * @param int $code
     * @return JsonResponse
     */
    protected function formatErrorResponse(array $errors, int $code = 400): JsonResponse
    {

        if (isset($errors['error'])) {
            $formattedErrors = [];
            foreach ($errors['error']->getMessages() as $errorKey => $errorValues) {
                $formattedErrors[$errorKey] = implode(', ', $errorValues);
            }
        } else {
            $formattedErrors = $errors;
        }
        return $this->createJsonResponse(['status' => $code, 'data' => null, 'errors' => $formattedErrors], $code);
    }


}