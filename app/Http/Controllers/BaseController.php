<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 10:37 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Session;

abstract class BaseController extends Controller
{
    protected $service;

    public function __construct($service)
    {
        $this->service = $service;
    }

    /**
     * @param array $errors
     * @param int $code
     * @return JsonResponse
     */
    protected function formatErrorResponse(array $errors, int $code = 400): JsonResponse
    {
        return $this->createJsonResponse(['status' => $code,'data' => null, 'errors' => $errors], $code);
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    protected function formatSuccessResponse($data): JsonResponse
    {
        return $this->createJsonResponse(['status' => 200, 'data' => $data, 'errors' => null], 200);
    }

    /**
     * @param array $body
     * @param int $statusCode
     * @return JsonResponse
     */
    protected function createJsonResponse(array $body, int $statusCode): JsonResponse
    {
        return (new JsonResponse($body, $statusCode))->header('Accept-encoding', ['gzip', 'deflate']);
    }

    protected function addFlashSuccessMessage($message)
    {
        Session::flash('flash_message', $message);
        Session::flash('flash_type', 'success_msg');
    }

    protected function addFlashErrorMessage($message)
    {
        Session::flash('flash_message', $message);
        Session::flash('flash_type', 'error_msg');
    }

    protected function formateValidationMessage($errors)
    {
        return implode(',', $errors);
    }

}