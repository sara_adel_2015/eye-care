<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 6/8/2020
 * Time: 11:36 PM
 */

namespace App\Http\RequestEntities;


class CardRequestEntity
{
    public $name;
    public $firstName;
    public $lastName;
    public $email;
    public $billingFirstName;
    public $billingLastName;
    public $billingPhone;
    public $billingAddress1;
    public $billingCity;
    public $billingState;
    public $billingCountry;
    public $shippingPhone;
    public $shippingAddress1;
    public $shippingCity;
    public $shippingState;
    public $shippingCountry;
    public $description = 'New Customer Created';

    public function __construct($request)
    {
        $this->firstName = $request['firstName'];
        $this->lastName = data_get($request, 'lastName', '');
        $this->name = $this->firstName . ' ' . $this->lastName;
        $this->billingFirstName = $request['firstName'];
        $this->billingLastName = data_get($request, 'lastName', '');
        $this->billingAddress1 = $request['address'];
        $this->billingCity = $request['city'];
        $this->billingState = $request['state'];
        $this->billingCountry = 'United states';
        $this->billingPhone = data_get($request, 'phone', '');
        $this->shippingAddress1 = $request['address'];
        $this->shippingCity = $request['city'];
        $this->shippingState = $request['state'];
        $this->shippingCountry = 'United states';
        $this->shippingPhone = data_get($request, 'phone', '');
        $this->email = $request['email'];
    }
}