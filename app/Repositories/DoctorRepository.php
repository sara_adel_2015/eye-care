<?php

namespace App\Repositories;

use App\Enums\DoctorStatusEnum;

/**
 * DoctorListRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DoctorRepository extends AbstractRepository
{
    public function getDoctors($isActive = true, $page = 1, $search = '')
    {
        $queryBuilder = $this->createQueryBuilder('d')
            ->select('d');
        if ($isActive) {
            $queryBuilder->where('d.status = :status')
                ->setParameter('status', DoctorStatusEnum::ACTIVE);
        }
        if ($search) {
            $queryBuilder->where('d.doctorName LIKE :search')
                ->orWhere('d.email LIKE :search')
                ->setParameter('search', '%' . $search . '%');
        }
        $query = $queryBuilder->orderBy('d.id', 'desc')->getQuery();
        return $this->paginate($query, self::$limit, $page);

    }


    public function getCancelSubscriptionRequests($page = 1)
    {
        $queryBuilder = $this->createQueryBuilder('d')
            ->select('d');
        $queryBuilder->where('d.isRequestToCancelSubscription = :isRequestToCancelSubscription')
            ->setParameter('isRequestToCancelSubscription', true);
        $queryBuilder->andWhere($queryBuilder->expr()->orX(
            $queryBuilder->expr()->eq('d.isSubscriptionCancelled', ':isSubscriptionCancelled'),
            $queryBuilder->expr()->isNull('d.isSubscriptionCancelled')
        ));
        $queryBuilder->setParameter('isSubscriptionCancelled', false);
        $query = $queryBuilder->orderBy('d.id', 'desc')->getQuery();
        return $this->paginate($query, self::$limit, $page);

    }

    public function getDoctorsForApi($isActive = true)
    {
        $columns = [
            'd.id',
            'd.doctorName',
            'd.email',
            'd.speciality',
            'd.address',
            'd.city',
            'd.state',
            'd.zip',
            'd.latitude',
            'd.longitude',
            'd.weblink AS website',
            'd.iphoneImage AS imageURL',
            'd.dateAdded',
            'd.doctorCode',
            'd.phone'];
        $queryBuilder = $this->createQueryBuilder('d')
            ->select($columns);
        if ($isActive) {
            $queryBuilder->where('d.status = :status')
                ->setParameter('status', DoctorStatusEnum::ACTIVE);
        }
        return $queryBuilder->orderBy('d.dateAdded', 'desc')->getQuery()->execute();
    }

    public function getDoctorsForNewPlanSubscription()
    {
        $queryBuilder = $this->createQueryBuilder('d')
            ->select('d');
        $queryBuilder->where('d.subscriptionId IS NOT NULL');
        $queryBuilder->andWhere('d.customerReference IS NOT NULL');
        $queryBuilder->andWhere($queryBuilder->expr()->orX(
            $queryBuilder->expr()->eq('d.isSubscriptionCancelled', ':isSubscriptionCancelled'),
            $queryBuilder->expr()->isNull('d.isSubscriptionCancelled')
        ));
        $queryBuilder->setParameter('isSubscriptionCancelled', false);
        $query = $queryBuilder->orderBy('d.id', 'desc')->getQuery();
        return $query->execute();
    }
}
