<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 5/15/2020
 * Time: 3:03 AM
 */

namespace App\Repositories;


use App\Entities\Doctor;
use Auth;
use App\Entities\DoctorPaymentCards;

class DoctorPaymentRecurringHistoryRepository extends AbstractRepository
{
    public function getAdminPaymentRecurringHistory($page = 1, $doctorId = 0)
    {
        try {
            $queryBuilder = $this->createQueryBuilder('dr')->select('dr');
            $queryBuilder->leftJoin(Doctor::class, 'd', 'with', 'd.id = dr.doctorId');
            $queryBuilder->addSelect('d.doctorName');
            if ($doctorId) {
                $queryBuilder->where('dr.doctorId = :doctorId')
                    ->setParameter('doctorId', $doctorId);
            }

            $query = $queryBuilder->orderBy('dr.id', 'desc')
                ->getQuery();
            return $this->paginate($query, self::$limit, $page);
        } catch (\Exception $ex) {
            return null;
        }

    }

    public function getRecurringHistoryByCardId($paymentCardId, $page = 1)
    {
        $queryBuilder = $this->createQueryBuilder('h')->select('h');
        $queryBuilder->innerJoin(Doctor::class, 'd', 'with', 'd.id = h.doctorId');
        $queryBuilder->where('h.doctorId = :doctorId');
        $queryBuilder->innerJoin(DoctorPaymentCards::class, 'pc', 'with', 'pc.id = h.paymentCardId');
        $queryBuilder->andWhere('h.paymentCardId = :paymentCardId');
        $queryBuilder->setParameters(['paymentCardId' => $paymentCardId, 'doctorId' => Auth::user()->getId()]);
        $query = $queryBuilder->orderBy('h.id', 'desc')
            ->getQuery();
        return $this->paginate($query, self::$limit, $page);

    }
}