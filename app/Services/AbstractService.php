<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 10:49 PM
 */

namespace App\Services;


abstract class AbstractService
{
    public $repo;

    public function __construct($repo)
    {
        $this->repo = $repo;
    }

}