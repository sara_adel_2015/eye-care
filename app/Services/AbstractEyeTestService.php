<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 10:49 PM
 */

namespace App\Services;

use App\Entities\Patient;
use Illuminate\Support\Facades\App;
use LaravelDoctrine\ORM\Facades\EntityManager;
use App\Entities\EyeTest;
use App\Entities\Doctor;
use PhpParser\Comment\Doc;

abstract class AbstractEyeTestService extends AbstractService
{
    protected $imageName;
    /**
     * @var $defaultPhysician Doctor
     */
    protected $defaultPhysician = null;
    static $storeImageFolder = 'eyecareImages';

    abstract function saveEyeTest($request);

    public function __construct()
    {
        $this->repo = EntityManager::getRepository(EyeTest::class);
        /**
         * @var $settingService \App\Services\Admin\AdminService
         */
        $settingService = App::make(\App\Services\Admin\AdminService::class);
        /**
         * @var $settings Setting
         */
        $settings = $settingService->getSettings();
        $doctorRepo = EntityManager::getRepository(Doctor::class);
        $this->defaultPhysician = $doctorRepo->find($settings['settings']->getDefaultPhysician());
    }

    /**
     * Get Random Image
     * @param $length
     * @return string
     */
    function genRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $string = '';
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $string;
    }

    /**
     * Store Eye Test Image in folder
     * @param $image
     * @return mixed
     */
    protected function storeEyeTestImage($image)
    {
        return $image->storeAs(self::$storeImageFolder, $this->imageName, 'uploads');
    }

    /**
     * @param $imageFile
     * @return string
     */
    protected function setImageName($imageFile)
    {
        $randNo = $this->genRandomString(6);
        $this->imageName = time() . "_" . $randNo . "." . $imageFile->extension();
        return $this->imageName;
    }

    public function getPatientDoctor(Patient $patient)
    {
        $doctor = $patient->getDoctor();
        if (!$doctor) {
            $doctor = $this->defaultPhysician;
        }
        return $doctor;
    }

}