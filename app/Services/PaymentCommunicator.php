<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 6/8/2020
 * Time: 11:26 PM
 */

namespace App\Services;

use App\Http\RequestEntities\CardRequestEntity;
use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;

class PaymentCommunicator
{
    public static $paymentCommunicator = 'Stripe';
    public $paymentGateway;

    public function __construct()
    {
        //$this->paymentGateway = Omnipay::create(self::$paymentCommunicator);
        //$this->paymentGateway = Omnipay::create('\Omnipay\NewStripe\Gateway');
        //dd($this->paymentGateway);
        $this->paymentGateway = new \Omnipay\NewStripe\Gateway();
        $this->paymentGateway->setApiKey(env('STRIPE_API_KEY'));
    }

    public function createCustomer(CardRequestEntity $card, $token)
    {
        $creditCard = new CreditCard(json_decode(json_encode($card), true));
        $cardData = [
            'token' => $token,
            'card' => $creditCard,
            'email' => $card->email,
            'name' => $card->name,
            'description' => $card->description
        ];
        return $this->paymentGateway->createCustomer($cardData)->send();

    }

    public function addNewCardToCustomer(CardRequestEntity $card, $token, $customerReference)
    {
        $creditCard = new CreditCard(json_decode(json_encode($card), true));
        $cardData = [
            'token' => $token,
            'card' => $creditCard,
            'customerReference' => $customerReference,
            'email' => $card->email,
            'name' => $card->name,
            'description' => $card->description
        ];
        return $this->paymentGateway->createCard($cardData)->send();
    }

    public function createSubscription($planId, $customerId)
    {
        return $this->paymentGateway->createSubscription([
            'plan' => $planId,
            'customerReference' => $customerId
        ])->send();
    }

    public function cancelSubscription($customerReference, $subscriptionId)
    {
        try {
            return $this->paymentGateway->cancelSubscription(
                [
                    'customerReference' => $customerReference,
                    'subscriptionReference' => $subscriptionId
                ]
            )->send();
        } catch (\Exception $ex) {
            return ['error' => '1', 'message' => $ex->getMessage()];
        }
    }

    public function setDefaultCustomerCard($customerReference, $cardReference)
    {
        $cardData = [
            'customerReference' => $customerReference,
            'source' => $cardReference,
        ];
        $response = $this->paymentGateway->updateCustomer($cardData)->send();
        return $response;
    }

    public function getSubscription($subscriptionId, $customerReference)
    {
        return $this->paymentGateway->fetchSubscription(
            [
                'subscriptionReference' => $subscriptionId,
                'customerReference' => $customerReference,
            ]
        )->send();
    }

    public function updateSubscription($subscriptionId, $customerReference, $planId)
    {
        return $this->paymentGateway->updateSubscription(
            [
                'subscriptionReference' => $subscriptionId,
                'customerReference' => $customerReference,
                'plan' => $planId,
            ]
        )->send();
    }

    public function createPlan($productId, $amount)
    {
        return $this->paymentGateway->createPlan(
            [
                'id' => $productId,
                'amount' => $amount,
                'currency' => 'USD',
                'interval' => 'month',
            ]
        )->send();
    }

}