<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:21 PM
 */

namespace App\Services\Admin;


use App\Entities\DictionaryDoctor;
use App\Services\AbstractDoctorService;
use App\Entities\Doctor;
use Illuminate\Support\Facades\Storage;
use LaravelDoctrine\ORM\Facades\EntityManager;

class DictionaryDoctorService extends AbstractDoctorService
{
    CONST ACTION_ADD = 'add';
    CONST ACTION_UPDATE = 'update';

    public function __construct()
    {
        $this->repo = EntityManager::getRepository(DictionaryDoctor::class);
    }

    public function getDoctors($page = 1, $term = '')
    {
        $doctors = $this->repo->getDoctors(false, $page, $term);
        return ['doctors' => $doctors];
    }

    public function findDoctor($id)
    {
        return $this->repo->find($id);
    }

    public function addOrUpdateDoctor($request, $action = 'add', $isCheckAddress = false, $isFlush = true)
    {
        if ($action == self::ACTION_ADD) {
            $doctorEntity = new DictionaryDoctor();
        } else {
            $doctorEntity = $this->findDoctor($request['id']);

        }
        $doctorEntity->setDoctorName($request['doctorName']);
        $doctorEntity->setSpeciality($request['speciality']);
        $doctorEntity->setAddress($request['address']);
        $doctorEntity->setCity($request['city']);
        $doctorEntity->setState($request['state']);
        $doctorEntity->setZip($request['zip']);
        if (!$isCheckAddress) {
            $doctorEntity->setLatitude($request['longitude']);
            $doctorEntity->setLongitude($request['longitude']);
        } else {
            $address = $doctorEntity->getAddress() . '+' . $doctorEntity->getCity() . '+' . $doctorEntity->getState() . '+' . $doctorEntity->getZip();
            $checkAddressResponse = $this->checkAddress($address);
            if ($checkAddressResponse->status == 'SUCCESS') {
                $doctorEntity->setLatitude($checkAddressResponse->results->lat);
                $doctorEntity->setLongitude($checkAddressResponse->results->lng);
            } else {
                $doctorEntity->setLatitude(0.00);
                $doctorEntity->setLongitude(0.00);

            }
        }

        $doctorEntity->setWeblink($request['weblink']);
        $doctorEntity->setPhone($request['phone']);
        $doctorEntity->setStrorder($this->repo->getMAxStrOrder() + 1);
        if ($action == self::ACTION_ADD) {
            EntityManager::persist($doctorEntity);
        }
        //save image here

        if (!empty($request['iphoneImage'])) {
            if (is_string($request['iphoneImage'])) {
                $contents = file_get_contents($request['iphoneImage']);
                $fileName = $request['doctorName'] . '.png';
                Storage::disk('uploads')->put('doctorlist/' . $fileName, $contents);
            } else {
                $fileName = "iphone" . time() . "." . $request['iphoneImage']->extension();
                $request['iphoneImage']->storeAs('doctorlist', $fileName, 'uploads');
            }
            $doctorEntity->setIphoneImage($fileName);
        } elseif ($action == self::ACTION_UPDATE && empty($request['iphoneImage'])) {
            $doctorEntity->setIphoneImage($doctorEntity->getIphoneImage());
        } else {
            $doctorEntity->setIphoneImage(" ");
        }
        if ($isFlush) {
            EntityManager::flush();
        }
        return $doctorEntity;
    }

    public function updateStatus($id, $status)
    {
        $doctor = $this->repo->find($id);
        $doctor->setStatus($status);
        return EntityManager::flush();
    }


    public function updateStrOrder($id, $order)
    {
        $doctor = $this->repo->find($id);
        $doctor->setStrOrder($order);
        return EntityManager::flush();
    }

    public function delete($id)
    {
        /**
         * @var $doctor Doctor
         */
        $doctor = $this->repo->find($id);
        EntityManager::remove($doctor);
        EntityManager::flush();
        return ['error' => '0', 'message' => 'Successfully Deleted the doctor'];
    }

    public function deleteDoctorImage($id)
    {
        try {
            $doctor = $this->findDoctor($id);
            if (!empty($doctor->getIphoneImage())) {
                unlink(public_path() . '/admin/uploads/doctorlist/' . $doctor->getIphoneImage());
                $doctor->setIphoneImage("");
                EntityManager::flush();
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }

    }
}