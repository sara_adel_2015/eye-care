<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:20 PM
 */

namespace App\Services\Admin;


use App\Entities\Setting;
use App\Services\AbstractService;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Auth;
use App\Services\PaymentCommunicator;

class SubscriptionSettingsService extends AbstractService
{
    public function __construct()
    {
        $this->repo = EntityManager::getRepository(Setting::class);
    }


    public function updateSubscriptionSettings($id, $request)
    {
        /** @var  $settingsEntity Setting */
        $settingsEntity = $this->repo->find($id);
        $doctorChargeRecurringAmount = floatval($request['doctorChargeRecurringAmount']);
        if ($settingsEntity->getDoctorChargeRecurringAmount() !== $doctorChargeRecurringAmount) {
            $settingsEntity->setDoctorChargeRecurringAmount($doctorChargeRecurringAmount);
            $paymentGateWay = new PaymentCommunicator();
            $createPlanResponse = $paymentGateWay->createPlan(env('STRIPE_PRODUCT_ID'), $doctorChargeRecurringAmount);
            if ($createPlanResponse->isSuccessful()) {
                $newPlanId = $createPlanResponse->getData()['id'];
                $settingsEntity->setSubscriptionPlanId($newPlanId);
                $subscribedDoctorService = app(DoctorService::class);
                $response = $subscribedDoctorService->updateDoctorsForNewPlanSubscription($newPlanId);
                EntityManager::flush();
                return $response;
            } else {
                return ['error' => '1', 'message' => 'Failed to create plan'];
            }
        }

        return ['error' => '1', 'message' => 'No Changes to Update'];
    }
}