<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:21 PM
 */

namespace App\Services\Admin;


use App\Entities\Doctor;
use App\Services\AbstractService;
use App\Entities\Patient;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PatientService extends AbstractService
{
    CONST ACTION_ADD = 'add';
    CONST ACTION_UPDATE = 'update';

    public function __construct()
    {
        $this->repo = EntityManager::getRepository(Patient::class);
    }

    public function getPatients($search = '', $page = 1)
    {
        $patients = $this->repo->getPatients(false, $search, $page);
        return ['patients' => $patients];
    }

    public function findPatient($id)
    {
        return $this->repo->find($id);
    }

    public function addOrUpdatePatient($request, $action = 'add')
    {
        if ($action == self::ACTION_ADD) {
            $patientEntity = new Patient();
        } else {
            $patientEntity = $this->findPatient($request['id']);
        }
        $patientEntity->setFirstName($request['firstName']);
        $patientEntity->setLastName($request['lastName']);
        $patientEntity->setEmail($request['email']);
        $hashedPassword = Hash::make($request['password']);
        $patientEntity->setPassword($hashedPassword);
        $patientEntity->setAddress($request['address']);
        $patientEntity->setCity($request['city']);
        $patientEntity->setState($request['state']);
        $patientEntity->setZip($request['zip']);
        $patientEntity->setPhone($request['phone']);
        $patientEntity->setStrorder($this->repo->getMAxStrOrder() + 1);
        $patientEntity->setApiToken(Str::random(60));
        if (!empty($request['doctorId'])) {
            $doctorRepo = EntityManager::getRepository(Doctor::class);
            $doctor = $doctorRepo->find($request['doctorId']);
            $patientEntity->setDoctor(($doctor) ? $doctor : NULL);
        }
        if ($action == self::ACTION_ADD) {
            EntityManager::persist($patientEntity);
        }
        return EntityManager::flush();
    }

    public
    function updateStatus($id, $status)
    {
        $patient = $this->repo->find($id);
        $patient->setStatus($status);
        return EntityManager::flush();
    }


    public
    function delete($id)
    {
        $patient = $this->repo->find($id);
        EntityManager::remove($patient);
        return EntityManager::flush();
    }


}