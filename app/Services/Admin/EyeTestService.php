<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:21 PM
 */

namespace App\Services\Admin;


use App\Entities\EyeTest;
use App\Services\AbstractEyeTestService;
use LaravelDoctrine\ORM\Facades\EntityManager;

class EyeTestService extends AbstractEyeTestService
{

    public function getAllEyeTest($patientId = 0, $page)
    {
        $list = $this->repo->getAllEyeTest($patientId, $page);
        return ['list' => $list];
    }


    public function addEyeTest($request)
    {
        //store image in folder
        $this->setImageName($request['imageFile']);
        if (!$this->storeEyeTestImage($request['imageFile'])) {
            return [
                'error' => 1,
                'message' => 'Image does not upload in folder'
            ];
        }
        return $this->saveEyeTest($request);
    }

    public function saveEyeTest($request)
    { //save eye test in db
        $eyeTestEntity = new EyeTest();
        $eyeTestEntity->setImageType($request['imageType']);
        $eyeTestEntity->setImageName($this->imageName);
        $eyeTestEntity->setDeviceType('Iphone');
        $eyeTestEntity->setComparePattern(' ');
        $eyeTestEntity->setPatientId($request['patientId']);
        EntityManager::persist($eyeTestEntity);
        EntityManager::flush();

    }

    public function delete($id)
    {
        $doctor = $this->repo->find($id);
        EntityManager::remove($doctor);
        return EntityManager::flush();
    }

    public function download($id)
    {
        try {
            return true;
        } catch (\Exception $ex) {
            return false;
        }

    }


}