<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:21 PM
 */

namespace App\Services\Admin;


use App\Entities\DoctorPaymentRecurringHistory;
use App\Entities\EyeTest;
use App\Entities\Patient;
use App\Repositories\DoctorPaymentRecurringHistoryRepository;
use App\Services\AbstractDoctorService;
use App\Entities\Doctor;
use App\Services\PaymentCommunicator;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Illuminate\Support\Facades\App;

class DoctorService extends AbstractDoctorService
{
    CONST ACTION_ADD = 'add';
    CONST ACTION_UPDATE = 'update';


    public function getDoctors($page = 1, $term = '')
    {
        $doctors = $this->repo->getDoctors(false, $page, $term);
        return ['doctors' => $doctors];
    }

    public function findDoctor($id)
    {
        return $this->repo->find($id);
    }

    public function addOrUpdateDoctor($request, $action = 'add', $isCheckAddress = false, $isFlush = true)
    {
        if ($action == self::ACTION_ADD) {
            $doctorEntity = new Doctor();
        } else {
            $doctorEntity = $this->findDoctor($request['id']);

        }
        $doctorEntity->setDoctorName($request['doctorName']);
        $doctorEntity->setSpeciality($request['speciality']);
        $doctorEntity->setAddress($request['address']);
        $doctorEntity->setCity($request['city']);
        $doctorEntity->setState($request['state']);
        $doctorEntity->setZip($request['zip']);
        if (!$isCheckAddress) {
            $doctorEntity->setLatitude($request['longitude']);
            $doctorEntity->setLongitude($request['longitude']);
        } else {
            $address = $doctorEntity->getAddress() . '+' . $doctorEntity->getCity() . '+' . $doctorEntity->getState() . '+' . $doctorEntity->getZip();
            $checkAddressResponse = $this->checkAddress($address);
            if ($checkAddressResponse->status == 'SUCCESS') {
                $doctorEntity->setLatitude($checkAddressResponse->results->lat);
                $doctorEntity->setLongitude($checkAddressResponse->results->lng);
            } else {
                $doctorEntity->setLatitude(' ');
                $doctorEntity->setLongitude(' ');

            }
        }

        $doctorEntity->setWeblink($request['weblink']);
        $doctorEntity->setPhone($request['phone']);
        $doctorEntity->setStrorder($this->repo->getMAxStrOrder() + 1);
        if ($action == self::ACTION_ADD) {
            EntityManager::persist($doctorEntity);
        }
        //save image here
        if (!empty($request['iphoneImage'])) {
            $fileName = "iphone" . time() . "." . $request['iphoneImage']->extension();
            $doctorEntity->setIphoneImage($fileName);
            $request['iphoneImage']->storeAs('doctorlist', $fileName, 'uploads');
        } elseif ($action == self::ACTION_UPDATE && empty($request['iphoneImage'])) {
            $doctorEntity->setIphoneImage($doctorEntity->getIphoneImage());
        } else {
            $doctorEntity->setIphoneImage(" ");
        }
        if ($isFlush) {
            EntityManager::flush();
        }
        return $doctorEntity;
    }

    public function updateStatus($id, $status)
    {
        $doctor = $this->repo->find($id);
        $doctor->setStatus($status);
        return EntityManager::flush();
    }


    public function updateStrOrder($id, $order)
    {
        $doctor = $this->repo->find($id);
        $doctor->setStrOrder($order);
        return EntityManager::flush();
    }

    public function delete($id)
    {
        //check if it is set as a default physician then return an error
        $settingService = App::make(\App\Services\Admin\AdminService::class);
        $settings = $settingService->getSettings();
        $defaultPhysicianId = $settings['settings']->getDefaultPhysician();
        $defaultPhysician = ($defaultPhysicianId) ? $this->repo->find($defaultPhysicianId) : null;
        /**
         * @var $settings Setting
         */
        if ($defaultPhysicianId == $id) {
            return ['error' => '1', 'message' => 'Error, This doctor is set as a default Physician'];
        }
        /**
         * @var $doctor Doctor
         */
        $doctor = $this->repo->find($id);
        if ($doctor->getCustomerReference() && $doctor->getSubscriptionId() && !$doctor->getIsSubscriptionCancelled()) {
            $cancelSubscriptionResponse = $this->confirmCancelSubscription($doctor->getId(), $defaultPhysician);
            if ($cancelSubscriptionResponse['success'] == '0') {
                return ['error' => '1', 'message' => $cancelSubscriptionResponse['message']];
            }
        }
        //delete payment cards
        foreach ($doctor->getPaymentCards() as $paymentCard) {
            EntityManager::remove($paymentCard);
        }
        //assign eye tests to the default doctor

        EntityManager::remove($doctor);
        EntityManager::flush();
        return ['error' => '0', 'message' => 'Successfully Deleted the doctor'];
    }

    public function deleteDoctorImage($id)
    {
        try {
            $doctor = $this->findDoctor($id);
            if (!empty($doctor->getIphoneImage())) {
                unlink(public_path() . '/admin/uploads/doctorlist/' . $doctor->getIphoneImage());
                $doctor->setIphoneImage("");
                EntityManager::flush();
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }

    }

    public function getAdminPaymentRecurringHistory($page = 1, $doctorId = 0)
    {
        /**
         * @var $paymentRecurringHistoryRepo DoctorPaymentRecurringHistoryRepository
         */
        $paymentRecurringHistoryRepo = EntityManager::getRepository(DoctorPaymentRecurringHistory::class);
        return $paymentRecurringHistoryRepo->getAdminPaymentRecurringHistory($page, $doctorId);
    }

    public function getCancelSubscriptionRequests($page = 1, $term = '')
    {
        $doctors = $this->repo->getCancelSubscriptionRequests($page);
        return ['doctors' => $doctors];
    }

    public function confirmCancelSubscription($id, $defaultPhysician)
    {
        /**
         * @var $doctor Doctor
         */
        $doctor = $this->repo->find($id);
        $subscriptionId = $doctor->getSubscriptionId();
        $customerReference = $doctor->getCustomerReference();
        $paymentGateWay = new PaymentCommunicator();
        $response = $paymentGateWay->cancelSubscription($customerReference, $subscriptionId);
        if ($response->isSuccessful()) {
            $doctor->setIsSubscriptionCancelled(true);
            $patients = $doctor->getPatients();
            if (!empty($patients)) {
                /**
                 * @var $patient Patient
                 */
                foreach ($patients as $patient) {
                    $patient->setDoctor($defaultPhysician); // the doctor has been deleted from revieweing patient
                }
            };
            /**
             * @var $eyeTest EyeTest
             */
            foreach ($doctor->getEyeTests() as $eyeTest) {
                $eyeTest->setDoctor($defaultPhysician);
            }
            EntityManager::flush();
            return ['success' => 1];
        } else {
            EntityManager::flush();
            return ['success' => 0, 'message' => $response->getMessage()];
        }
    }

    public function updateDoctorsForNewPlanSubscription($newPlanId)
    {
        $updatedDoctors = [];
        $doctors = $this->repo->getDoctorsForNewPlanSubscription();
        if (!empty($doctors)) {
            /** @var  $doctor Doctor */
            foreach ($doctors as $doctor) {
                $response = $this->updateSubscription($doctor->getSubscriptionId(), $doctor->getCustomerReference(), $newPlanId);
                $updatedDoctors[$doctor->getCustomerReference()] =
                    $response['message'];
            }
        }
        return ['error' => '0', 'updatedDoctors' => $updatedDoctors];
    }

    public function updateSubscription($subscriptionId, $customerReference, $planId)
    {
        $paymentGateWay = new PaymentCommunicator();
        $subscriptionResponse = $paymentGateWay->getSubscription($subscriptionId, $customerReference);
        if ($subscriptionResponse->isSuccessful()) {
            $subscriptionUpdateResponse = $paymentGateWay->updateSubscription($subscriptionId, $customerReference, $planId);
            if ($subscriptionUpdateResponse->isSuccessful()) {
                return ['error' => '0',
                    'message' => 'Subscription Updated Successfully for customer: ' . $customerReference,
                ];
            } else {
                return ['error' => '1',
                    'message' => 'Failed update the subscription: ' . $subscriptionUpdateResponse->getMessage()
                ];
            }

        } else {
            return ['error' => '1',
                'message' => 'Failed get subscription: ' . $subscriptionResponse->getMessage()
            ];
        }
    }
}