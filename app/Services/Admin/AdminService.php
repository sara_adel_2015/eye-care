<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:20 PM
 */

namespace App\Services\Admin;


use App\Entities\Admin;
use App\Entities\Setting;
use App\Services\AbstractService;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Illuminate\Support\Facades\Hash;
use Auth;

class AdminService extends AbstractService
{
    public function __construct()
    {
        $this->repo = EntityManager::getRepository(Setting::class);
    }

    public function getSettings()
    {
        $settings = $this->repo->findAll();
        if (!$settings) {
            return ['settings' => new Setting(), 'action' => 'add'];
        } else {
            return ['settings' => $settings[0], 'action' => 'update'];
        }
    }

    public function addOrUpdateSettings($request, $action = 'update')
    {
        if ($action == 'add') {
            $settingsEntity = new Setting();
        } else {
            $settingsEntity = $this->repo->find($request['id']);
        }
        $settingsEntity->setAllowedCompareRatio($request['allowedCompareRatio']);
        $settingsEntity->setAllowedSimilitryPercentage($request['allowedSimilitryPercentage']);
        $settingsEntity->setDifferentResultReception($request['differentResultReception']);
        $settingsEntity->setDefaultPhysician($request['defaultPhysicianId']);
        $settingsEntity->setMatchedResultReception($request['matchedResultReception']);
        if ($request['differentResultReception'] == 'custom') {
            $settingsEntity->setDifferentResultReceptionEmail($request['differentResultReceptionEmail']);
        }
        if ($action == 'add') {
            EntityManager::persist($settingsEntity);
        }
        EntityManager::flush();
        return $settingsEntity;
    }

    public function changePassword(Admin $admin, $newPassword)
    {
        $admin->setPassword(Hash::make($newPassword));
        EntityManager::flush();
        Auth::setUser($admin);//update user
        $when = now()->addMinutes(env('SEND_MAIL_QUEUE_EVERY_MINUTES', 10));
        //Mail::to($admin->getEmail())->later($when, new \App\Mail\PasswordChanged($admin));
        return $admin;
    }

}