<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:21 PM
 */

namespace App\Services\B2c;

use App\Services\AbstractEyeTestService;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Auth;

class EyeTestService extends AbstractEyeTestService
{

    public function getAllEyeTest($page)
    {
        if (Auth::user()->getAuthenticationType() == 'patient') {
            $list = $this->repo->getAllEyeTest(Auth::user()->getId(), $page);
        } elseif (Auth::user()->getAuthenticationType() == 'doctor') {
            $list = $this->repo->getDoctorEyeTest(Auth::user()->getId(), $page,request()->get('patientId'));
        } else {
            $list = [];
        }

        return ['list' => $list];
    }

    public function saveEyeTest($request)
    {

    }

    public function delete($id)
    {
        $doctor = $this->repo->find($id);
        EntityManager::remove($doctor);
        return EntityManager::flush();
    }

}