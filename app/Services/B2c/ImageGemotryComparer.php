<?php

namespace App\Services\B2c;
class ImageGemotryComparer
{

    const GRID_COLUMNS_ROWS_LENGTH = 20;

    private $firstPatternPoints;
    private $secondPatternPoints;
    private $allowedDistanceRatio;

    function __construct($firstPattrenIndexedGridPoints, $seondPattrenIndexedGridPoints, $allowedDistance)
    {

        $this->firstPatternPoints = $firstPattrenIndexedGridPoints;
        $this->secondPatternPoints = $seondPattrenIndexedGridPoints;
        $this->allowedDistanceRatio = $allowedDistance;

    }

    /*
     * Get Geometry Point for any given index
     * Note: function works with index (one based)
     */

    public function getGemoteryPointForIndexGrid($index)
    {
        $index = $index === 0 ? $index : $index - 1;

        if ($index == 0) {
            return array(0, 0);
        } else {
            $divide = floor($index / ImageGemotryComparer::GRID_COLUMNS_ROWS_LENGTH);
            $modular = $index % ImageGemotryComparer::GRID_COLUMNS_ROWS_LENGTH;
            return array($divide, $modular);
        }
    }

    /*
     * Calculate this distance between two gemotry grid points
     * Formula d=\sqrt{(x_{2}-x_{1})^{2}+(y_{2}-y_{1})^{2}}
     */

    public function calculateDistance($firstPoint, $secondPoint)
    {

        if (count($firstPoint) != 2 || count($secondPoint) != 2) {
            return NULL;
        } else {
            $x = pow($secondPoint[0] - $firstPoint[0], 2);
            $y = pow($secondPoint[1] - $firstPoint[1], 2);
            $distance = sqrt($x + $y);
            return $distance;
        }
    }

    public function getSimilarity()
    {
        $matched_count = 0;

        for ($x = 0; $x < count($this->firstPatternPoints); $x++) {
            $point_x = $this->getGemoteryPointForIndexGrid($this->firstPatternPoints[$x]);
            for ($y = 0; $y < count($this->secondPatternPoints); $y++) {
                $point_y = $this->getGemoteryPointForIndexGrid($this->secondPatternPoints[$y]);
                $distance = $this->calculateDistance($point_x, $point_y);
                if ($distance <= $this->allowedDistanceRatio) {
                    $matched_count++;
                    break;
                }
            }
        }
        $countFirstPatternPoints = ((count($this->firstPatternPoints)) == 0) ? 1 : count($this->firstPatternPoints);
        $similarity = ($matched_count / $countFirstPatternPoints) * 100;
        return floor($similarity);
    }

}
