<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:21 PM
 */

namespace App\Services\B2c;

use App\Entities\DictionaryDoctor;
use App\Services\AbstractDoctorService;
use Auth;
use LaravelDoctrine\ORM\Facades\EntityManager;

class DictionaryDoctorService extends AbstractDoctorService
{


    public function __construct()
    {
        $this->repo = EntityManager::getRepository(DictionaryDoctor::class);
    }

    public function getDoctorsForApi()
    {
        $doctors = $this->repo->getDoctorsForApi();
        foreach ($doctors as $index => $doctor) {
            if (!empty($doctor['imageURL']) and $doctor['imageURL'] !== ' ') {
                $doctors[$index]['imageURL'] = env('UPLOAD_DOCTOR_FOLDER') . '/' . $doctor['imageURL'];
            } else {
                $doctors[$index]['imageURL'] = NULL;

            }
        }
        return $doctors;
    }
}
