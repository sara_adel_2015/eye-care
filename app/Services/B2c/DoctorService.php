<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:21 PM
 */

namespace App\Services\B2c;

use App\Entities\DictionaryDoctor;
use App\Entities\Doctor;
use App\Entities\DoctorPaymentCards;
use App\Events\DoctorRegisteredEvent;
use App\Http\RequestEntities\CardRequestEntity;
use App\Services\AbstractDoctorService;
use App\Services\PaymentCommunicator;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use Illuminate\Support\Facades\Mail;

class DoctorService extends AbstractDoctorService
{

    protected $doctorPaymentCard;

    public function getDoctorsForApi()
    {
        $doctors = $this->repo->getDoctorsForApi();
        foreach ($doctors as $index => $doctor) {
            if (!empty($doctor['imageURL']) and $doctor['imageURL'] !== ' ') {
                $doctors[$index]['imageURL'] = env('UPLOAD_DOCTOR_FOLDER') . '/' . $doctor['imageURL'];
            } else {
                $doctors[$index]['imageURL'] = NULL;

            }
        }
        return $doctors;
    }

    public function addDoctor($request)
    {
        try {
            $paymentCommunicator = new PaymentCommunicator();
            $cardEntity = new CardRequestEntity($request);
            $cardResponse = $paymentCommunicator->createCustomer($cardEntity, $request['token']);
            if ($cardResponse->isSuccessful()) {
                $this->addDoctorPaymentCard($cardResponse);
                $doctorEntity = new Doctor();
                $doctorName = $request['firstName'] . ' ' . $request['lastName'];
                $doctorEntity->setDoctorName($doctorName);
                $doctorEntity->setEmail($request['email']);
                $doctorEntity->setAddress(htmlspecialchars($request['address']));
                $doctorEntity->setCity($request['city']);
                $doctorEntity->setState($request['state']);
                $doctorEntity->setZip($request['zip']);
                $doctorEntity->setSpeciality($request['speciality']);
                $address = $doctorEntity->getAddress() . '+' . $doctorEntity->getCity() . '+' . $doctorEntity->getState() . '+' . $doctorEntity->getZip();
                //update doctor address
                $checkAddressResponse = $this->checkAddress($address);
                if ($checkAddressResponse->status == 'SUCCESS') {
                    $doctorEntity->setLatitude($checkAddressResponse->results->lat);
                    $doctorEntity->setLongitude($checkAddressResponse->results->lng);
                } else {
                    $doctorEntity->setLatitude(0.00);
                    $doctorEntity->setLongitude(0.00);
                }
                $doctorEntity->setPhone(data_get($request, 'phone', ''));
                $doctorEntity->setWeblink(data_get($request, 'website', ''));
                $doctorEntity->setStrorder($this->repo->getMAxStrOrder() + 1);
                //save image here
                if (!empty($request['profileImage'])) {
                    $fileName = "iphone" . time() . "." . $request['profileImage']->extension();
                    $doctorEntity->setIphoneImage($fileName);
                    $request['profileImage']->storeAs('doctorlist', $fileName, 'uploads');
                } else {
                    $doctorEntity->setIphoneImage('');
                }
                $doctorEntity->setPassword(Hash::make($request['password']));
                $doctorEntity->setApiToken(Str::random(60));
                $doctorEntity->addPaymentCard($this->getDoctorPaymentCard());
                $doctorEntity->setCustomerReference($this->getDoctorPaymentCard()->getCustomerReference());
                $generateCodeLength = env('DOCTOR_GENERATE_CODE_LENGTH', 5);
                $doctorCode = Str::random($generateCodeLength);
                $doctorEntity->setDoctorCode($doctorCode);
                EntityManager::persist($doctorEntity);
                EntityManager::flush();
                //attach current payment ID
                $doctorEntity->setCurrentPaymentCardId($this->getDoctorPaymentCard()->getId());
                EntityManager::flush();
                //create subscription here
                event(new DoctorRegisteredEvent($doctorEntity));
                return ['error' => 0, 'code' => 200, 'message' => 'success', 'doctor' => Doctor::formatDoctor($doctorEntity)];
            } else {
                return [
                    'error' => '1',
                    'code' => $cardResponse->getCode(),
                    'message' => ['stripeError' => $cardResponse->getMessage()]
                ];
            }
        } catch (\Exception $ex) {
            throw new InternalErrorException('Something went wrong, Please try again later ' . $ex->getMessage());
        }

    }

    public function addDoctorPaymentCard($cardResponse)
    {
        $lastFourDigits = $cardResponse->getData()['sources']['data'][0]['last4'];
        $doctorPaymentCard = new DoctorPaymentCards();
        $doctorPaymentCard->setCardReference($cardResponse->getCardReference());
        $doctorPaymentCard->setCustomerReference($cardResponse->getCustomerReference());
        $doctorPaymentCard->setLastFourDigits($lastFourDigits);
        $this->setDoctorPaymentCard($doctorPaymentCard);
    }


    protected function setDoctorPaymentCard(DoctorPaymentCards $doctorPaymentCard)
    {
        $this->doctorPaymentCard = $doctorPaymentCard;

    }

    protected function getDoctorPaymentCard(): DoctorPaymentCards
    {
        return $this->doctorPaymentCard;
    }


    public function updateDoctor($request)
    {
        /**
         * @var $doctorEntity Doctor
         */
        $doctorEntity = $this->repo->find(Auth::user()->getId());
        $doctorName = $request['doctorName'];
        $doctorEntity->setDoctorName($doctorName);
        $doctorEntity->setEmail($request['email']);
        $doctorEntity->setAddress(htmlspecialchars($request['address']));
        $doctorEntity->setCity($request['city']);
        $doctorEntity->setState($request['state']);
        $doctorEntity->setZip($request['zip']);
        $doctorEntity->setSpeciality($request['speciality']);
        $doctorEntity->setLatitude($request['latitude']);
        $doctorEntity->setLongitude($request['longitude']);
        $doctorEntity->setPhone(data_get($request, 'phone', ''));
        //save image here
        if (!empty($request['profileImage'])) {
            $fileName = "iphone" . time() . "." . $request['profileImage']->extension();
            $doctorEntity->setIphoneImage($fileName);
            $request['profileImage']->storeAs('doctorlist', $fileName, 'uploads');
        }
        EntityManager::flush();
    }

    public function changePassword(Doctor $doctor, $newPassword)
    {
        $doctor->setPassword(Hash::make($newPassword));
        $doctor->setApiToken(Str::random(60));
        EntityManager::flush();
        Auth::setUser($doctor);//update user
        $when = now()->addMinutes(env('SEND_MAIL_QUEUE_EVERY_MINUTES', 10));
        Mail::to($doctor->getEmail())->later($when, new \App\Mail\DoctorPassworodChanged($doctor));
        return Doctor::formatDoctor($doctor);
    }

    public function updateDefaultPaymentCard($paymentCardId)
    {
        $paymentCardRepo = EntityManager::getRepository(DoctorPaymentCards::class);
        /**
         * @var $defaultPaymentCard DoctorPaymentCards
         */
        $defaultPaymentCard = $paymentCardRepo->find($paymentCardId);
        $paymentCommunicator = new PaymentCommunicator();
        $defaultCardResponse = $paymentCommunicator->setDefaultCustomerCard($defaultPaymentCard->getCustomerReference(), $defaultPaymentCard->getCardReference());
        if ($defaultCardResponse->isSuccessful()) {
            /**
             * @var $doctor Doctor
             */
            $doctor = Auth::user();
            $doctor->setCurrentPaymentCardId($paymentCardId);
            EntityManager::flush();
            return ['error' => '0'];
        } else {
            return ['error' => '1', 'message' => $defaultCardResponse->getMessage()];
        }


    }

    public function resetPassword(Doctor $doctor)
    {
        $newPassword = substr(md5(uniqid(mt_rand(), true)), 0, 8);
        $doctor->setPassword(Hash::make($newPassword));
        $doctor->setApiToken(Str::random(60));
        EntityManager::flush();
        Auth::setUser($doctor);//update user
        $when = now()->addMinutes(env('SEND_MAIL_QUEUE_EVERY_MINUTES', 10));
        Mail::to($doctor->getEmail())->later($when, new \App\Mail\DoctorPasswordReseted($doctor, $newPassword));
        return Doctor::formatDoctor($doctor);
    }
}
