<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:21 PM
 */

namespace App\Services\B2c;


use App\Entities\Doctor;
use App\Services\AbstractService;
use App\Entities\Patient;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\App;

class PatientService extends AbstractService
{
    CONST ACTION_ADD = 'add';
    CONST ACTION_UPDATE = 'update';

    public function __construct()
    {
        $this->repo = EntityManager::getRepository(Patient::class);
        $this->doctorRepo = EntityManager::getRepository(Doctor::class);
    }

    public function addOrUpdatePatient($request, $action = 'add')
    {
        if ($action == self::ACTION_ADD) {
            $patientEntity = new Patient();
        } else {
            $patientEntity = Auth::user();
        }
        $patientEntity->setFirstName($request['firstName']);
        $patientEntity->setLastName($request['lastName']);
        $patientEntity->setEmail($request['email']);
        if (!empty($request['password'])) {//optional in case of update
            $hashedPassword = Hash::make($request['password']);
            $patientEntity->setPassword($hashedPassword);
            $patientEntity->setApiToken(Str::random(60));
        }
        $patientEntity->setAddress(data_get($request, 'address', ''));
        $patientEntity->setCity(data_get($request, 'city', ''));
        $patientEntity->setState(data_get($request, 'state', ''));
        $patientEntity->setZip($request['zip']);
        $patientEntity->setPhone(data_get($request, 'phone', ''));
        //attach doctor to the patient
        if (!empty($request['doctorId'])) {
            $doctorRepo = EntityManager::getRepository(Doctor::class);
            $doctor = $doctorRepo->find($request['doctorId']);
            $patientEntity->setDoctor($doctor);
        } else {
            //attach default
            $settingService = App::make(\App\Services\Admin\AdminService::class);
            $settings = $settingService->getSettings();
            $defaultPhysicianId = $settings['settings']->getDefaultPhysician();
            $defaultPhysician = ($defaultPhysicianId) ? $this->doctorRepo->find($defaultPhysicianId) : null;
            $patientEntity->setDoctor($defaultPhysician);
        }
        if ($action == self::ACTION_ADD) {
            $patientEntity->setStrorder($this->repo->getMAxStrOrder() + 1);

            EntityManager::persist($patientEntity);
        }
        EntityManager::flush();
        if ($action == self::ACTION_UPDATE) {
            Auth::setUser($patientEntity);//update user
        }

        return Patient::formatPatient($patientEntity);
    }

    public function changePassword(Patient $patient, $newPassword)
    {
        $patient->setPassword(Hash::make($newPassword));
        $patient->setApiToken(Str::random(60));
        EntityManager::flush();
        Auth::setUser($patient);//update user
        $when = now()->addMinutes(env('SEND_MAIL_QUEUE_EVERY_MINUTES', 10));
        Mail::to($patient->getEmail())->later($when, new \App\Mail\PasswordChanged($patient));
        return Patient::formatPatient($patient);
    }

    public function resetPassword(Patient $patient)
    {
        $newPassword = substr(md5(uniqid(mt_rand(), true)), 0, 8);
        $patient->setPassword(Hash::make($newPassword));
        $patient->setApiToken(Str::random(60));
        EntityManager::flush();
        Auth::setUser($patient);//update user
        $when = now()->addMinutes(env('SEND_MAIL_QUEUE_EVERY_MINUTES', 10));
        Mail::to($patient->getEmail())->later($when, new \App\Mail\PasswordReseted($patient, $newPassword));
        return Patient::formatPatient($patient);
    }

    public function getDetails()
    {
        /** @var  $patient Patient */
        $patient = Auth::user();
        $patientFormatted = [];
        $patientFormatted['firstName'] = $patient->getFirstName();
        $patientFormatted['lastName'] = $patient->getLastName();
        $patientFormatted['email'] = $patient->getEmail();
        $patientFormatted['address'] = $patient->getAddress();
        $patientFormatted['city'] = $patient->getCity();
        $patientFormatted['state'] = $patient->getState();
        $patientFormatted['zip'] = $patient->getZip();
        $patientFormatted['phone'] = $patient->getPhone();
        $patientFormatted['doctor'] = null;
        if ($patient->getDoctor()) {
            $patientFormatted['doctor']['id'] = $patient->getDoctor()->getId();
            $patientFormatted['doctor']['name'] = $patient->getDoctor()->getDoctorName();
            $patientFormatted['doctor']['email'] = $patient->getDoctor()->getEmail();
        }
        return $patientFormatted;
    }
}