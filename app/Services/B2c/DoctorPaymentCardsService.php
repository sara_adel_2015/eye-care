<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:21 PM
 */

namespace App\Services\B2c;

use App\Entities\Doctor;
use App\Entities\DoctorPaymentCards;
use App\Entities\DoctorPaymentRecurringHistory;
use App\Http\RequestEntities\CardRequestEntity;
use App\Services\AbstractService;
use App\Services\PaymentCommunicator;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Auth;
use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;


class DoctorPaymentCardsService extends AbstractService
{
    /**
     * @var $doctorPaymentCard DoctorPaymentCards
     */
    protected $doctorPaymentCard;

    public function __construct()
    {
        $this->repo = EntityManager::getRepository(DoctorPaymentCards::class);
    }

    protected function setDoctorPaymentCard(DoctorPaymentCards $doctorPaymentCard)
    {
        $this->doctorPaymentCard = $doctorPaymentCard;

    }

    protected function getDoctorPaymentCard()
    {
        return $this->doctorPaymentCard;
    }

    public function addNewCard($request)
    {
        /**
         * @var $customer Doctor
         */
        $customer = Auth::user();
        $currentPaymentCard = $customer->getCurrentPaymentCardId();
        $customerReference = ($currentPaymentCard) ? $this->repo->find($currentPaymentCard)->getCustomerReference() : null;
        $paymentCommunicator = new PaymentCommunicator();
        $cardDetails = [
            'name' => $customer->getDoctorName(),
            'firstName' => $customer->getDoctorName(),
            'address' => $customer->getAddress(),
            'city' => $customer->getCity(),
            'state' => $customer->getState(),
            'phone' => $customer->getPhone(),
            'email' => $customer->getEmail(),
        ];
        $cardEntity = new CardRequestEntity($cardDetails);
        $cardResponse = $paymentCommunicator->addNewCardToCustomer($cardEntity, $request['token'], $customerReference);
        if ($cardResponse->isSuccessful()) {
//            if (!$customerReference) {//create
//                $lastFourDigits = $cardResponse->getData()['sources']['data'][0]['last4'];
//            } else {
//                dd($cardResponse->getData());
//                $lastFourDigits = $cardResponse->getData()['last4'];
//            }
            $lastFourDigits = $cardResponse->getData()['last4'];
            $doctorPaymentCard = new DoctorPaymentCards();
            $doctorPaymentCard->setCardReference($cardResponse->getCardReference());
            $doctorPaymentCard->setCustomerReference($cardResponse->getCustomerReference());
            $doctorPaymentCard->setLastFourDigits($lastFourDigits);
            $this->setDoctorPaymentCard($doctorPaymentCard);
            if ($customer) {
                $customer->addPaymentCard($doctorPaymentCard);
                EntityManager::flush();
            }
            return [
                'error' => 0,
                'code' => 200,
                'message' => 'done'
            ];

        } else {
            return [
                'error' => '1',
                'code' => $cardResponse->getCode(),
                'message' => $cardResponse->getMessage()
            ];
        }
    }


    public function updatePaymentCard()
    {

    }

    public function deletePaymentCard()
    {

    }

    public function getPaymentCardHistory($id)
    {
        /**
         * @var $paymentCard DoctorPaymentCards
         */
        $historyRepo = EntityManager::getRepository(DoctorPaymentRecurringHistory::class);
        $history = $historyRepo->getRecurringHistoryByCardId($id);
        return $history;
    }
}