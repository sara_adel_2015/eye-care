<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 6:21 PM
 */

namespace App\Services\B2c;


use App\Entities\EyeTest;
use App\Entities\Patient;
use App\Entities\Setting;
use App\Services\AbstractEyeTestService;
use Illuminate\Support\Facades\App;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PatientUploadEyeTestService extends AbstractEyeTestService
{


    public function execute($request, $patient)
    {
        $this->setImageName($request['imageFile']);
        if (!$this->storeEyeTestImage($request['imageFile'])) {
            return [
                'error' => 1,
                'message' => 'Image does not upload in folder'
            ];
        }
        $latestImageEyeTest = $this->repo->getLatestPatientEyeTest($patient->getId(), $request['imageType']);
        $newImageEyeTest = $this->saveEyeTest($request);
        $when = now()->addMinutes(env('SEND_MAIL_QUEUE_EVERY_MINUTES', 10));
        if ($latestImageEyeTest) {
            $latestImageEyeTestPattern = $latestImageEyeTest->getComparepattern();
            $newImageEyeTestPattern = ($request['comparePattern']) ? $request['comparePattern'] : '';
            $adminService = App::make(\App\Services\Admin\AdminService::class);
            /**
             * @var $settings Setting
             */
            $settings = $adminService->getSettings()['settings'];
            //Get Similarity
            $similarity = $this->getSimilarity($newImageEyeTestPattern, $latestImageEyeTestPattern, $settings->getAllowedCompareRatio());
            //$similarity = 25;
            $attachments = [
                $newImageEyeTest->getImageName(),
                $latestImageEyeTest->getImageName()
            ];
            $imageTypeName = ($newImageEyeTest->getImageType() == 'leftimage') ? 'Left' : 'Right';
            //send same mail to default phisiian as well

            if ($similarity < $settings->getAllowedSimilitryPercentage()) {
                if ($settings->getDifferentResultReception() == 'patient') {
                    $to = $patient->getEmail();
                } else {
                    $to = $settings->getDifferentResultReceptionEmail();
                }
                Mail::to($to)->later($when, new \App\Mail\EyeTestNotPassed($patient, $similarity, $attachments, $imageTypeName));
                $this->sendEyeTestResultEmailToDoctor($patient, $similarity, $attachments, $imageTypeName, $when, false);

            } else {
                if ($settings->getMatchedResultReception() == 'patient') {
                    $to = $patient->getEmail();
                } else {
                    $to = $settings->getDifferentResultReceptionEmail();
                }
                Mail::to($to)->later($when, new \App\Mail\EyeTestPassed($patient, $similarity, $attachments, $imageTypeName));
                $this->sendEyeTestResultEmailToDoctor($patient, $similarity, $attachments, $imageTypeName, $when, true);
            }
            return [
                'error' => '0',
                'imageName' => $this->imageName,
                'imagePath' => asset('admin/uploads/eyecareImages/' . $this->imageName),
                'similarity' => $similarity
            ];
        }
    }

    /**
     * Save Eye Test Image In DB
     * @param $request
     * @return EyeTest
     */
    public function saveEyeTest($request)
    {
        $eyeTestEntity = new EyeTest();
        $eyeTestEntity->setImageType($request['imageType']);
        $eyeTestEntity->setDeviceType($request['deviceType']);
        $eyeTestEntity->setComparePattern(($request['comparePattern']) ? $request['comparePattern'] : '');
        $eyeTestEntity->setImageName($this->imageName);
        $eyeTestEntity->setPatientId(Auth::user()->getId());
        //attach doctor
        $patientRepo = EntityManager::getRepository(Patient::class);
        /**
         * @var $patient Patient
         */
        $patient = $patientRepo->find(Auth::user()->getId());
        $doctor = $this->getPatientDoctor($patient);
        $eyeTestEntity->setDoctor($doctor);
        EntityManager::persist($eyeTestEntity);
        EntityManager::flush();
        return $eyeTestEntity;
    }

    public function getSimilarity($newImageEyeTestPattern, $latestImageEyeTestPattern, $allowedDistanceRatio)
    {
        if ($newImageEyeTestPattern) {
            $newImageEyeTestPattern = explode(",", $newImageEyeTestPattern);
        } else {
            $newImageEyeTestPattern = [];
        }
        if ($latestImageEyeTestPattern) {
            $latestImageEyeTestPattern = explode(",", $latestImageEyeTestPattern);
        } else {
            $latestImageEyeTestPattern = [];
        }
        if (count($newImageEyeTestPattern) == 0 && count($latestImageEyeTestPattern) == 0) {
            $similarity = 100;
        } else {
            $comparer = new ImageGemotryComparer($newImageEyeTestPattern, $latestImageEyeTestPattern, $allowedDistanceRatio);
            $similarity = $comparer->getSimilarity();
        }
        return $similarity;
    }

    public function sendEyeTestResultEmailToDoctor(Patient $patient, $similarity, $attachments, $imageTypeName, $emailTime, $isTestPassed = true)
    {
        if ($isTestPassed) {
            $emailTemplate = new \App\Mail\EyeTestPassed($patient, $similarity, $attachments, $imageTypeName);
        } else {
            $emailTemplate = new \App\Mail\EyeTestNotPassed($patient, $similarity, $attachments, $imageTypeName);
        }
        //send result to default doctor
        if ($this->defaultPhysician && $this->defaultPhysician->getEmail()) {
            Mail::to($this->defaultPhysician->getEmail())->later($emailTime, $emailTemplate);
        }
        //send result to subscribed doctor
        if ($patient->getDoctor() && $patient->getDoctor()->getEmail()) {
            Mail::to($patient->getDoctor()->getEmail())->later($emailTime, $emailTemplate);
        }
    }

}