<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 6/8/2020
 * Time: 11:26 PM
 */

namespace App\Services;


use App\Entities\DoctorPaymentCards;
use App\Entities\DoctorPaymentRecurringHistory;
use LaravelDoctrine\ORM\Facades\EntityManager;

class PaymentWebhooksHandler
{
    static $allowedEventTypes = [
        'invoice.payment_succeeded',
        'invoice.payment_failed'
    ];
    private $eventType;

    public function __construct($eventType)
    {
        $this->eventType = $eventType;

    }

    public function execute($request)
    {
        if (in_array($this->eventType, self::$allowedEventTypes)) {
            $isSuccess = false;
            $data = $request->data['object'];
            $chargeId = $data['charge'];
            $customerReference = $data['customer'];
            $cardReference = data_get($data, 'payment_method', ' ');
            $amount = $data['amount_paid'] / 100;
            $doctorPaymentRecurring = new DoctorPaymentRecurringHistory();
            if ($this->eventType == 'invoice.payment_succeeded') {
                $isSuccess = true;
            }
            $doctorPaymentRecurring->setIsSuccess($isSuccess);
            $doctorPaymentRecurring->setAmount($amount);
            $doctorPaymentRecurring->setChargeId($chargeId);
            $doctorPaymentRecurring->setInvoiceId($data['id']);
            $doctorPaymentRecurring->setInvoicePdfUrl($data['invoice_pdf']);
            $doctorPaymentRecurring->setCustomerReference($customerReference);
            $doctorPaymentRecurring->setCardReference($cardReference);
            $doctorPaymentRecurring->setEventId($request->id);
            $doctorPaymentRecurring->setSubscriptionPlanId($data['lines']['data'][0]['plan']['id']);
            $paymentCardRepo = EntityManager::getRepository(DoctorPaymentCards::class);
            /**
             * @var $paymentCard DoctorPaymentCards
             */
            $paymentCard = $paymentCardRepo->findOneBy(['customerReference' => $customerReference]);
            if ($paymentCard) {
                $doctorPaymentRecurring->setDoctorId($paymentCard->getDoctor()->getId());
                $doctorPaymentRecurring->setPaymentCardId($paymentCard->getId());
            }
            EntityManager::persist($doctorPaymentRecurring);
            EntityManager::flush();
        }
    }
}