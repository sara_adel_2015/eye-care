<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 3/26/2020
 * Time: 10:49 PM
 */

namespace App\Services;

use LaravelDoctrine\ORM\Facades\EntityManager;
use App\Entities\Doctor;
use GuzzleHttp\Client;

abstract class AbstractDoctorService extends AbstractService
{


    public function __construct()
    {
        $this->repo = EntityManager::getRepository(Doctor::class);
    }

    public function checkAddress($address)
    {
        try {
            $client = new Client();
            $res = $client->request('GET', 'https://maps.google.com/maps/api/geocode/json', [
                'query' => [
                    'address' => $address,
                    'key' => env('GOOGLE_API_KEY', 'AIzaSyDmIaO73qOMTMrK4FDPetTMsybAMy-e6hA')
                ]
            ]);
            if ($res->getStatusCode()) {
                $responseContent = json_decode($res->getBody()->getContents());
                if (!$responseContent) {
                    $response = new \stdClass();
                    $response->status = 'REQUEST_FAILED';
                    $response->error_message = 'unable to parse response';
                    $response->results = [];
                } elseif (!empty($responseContent->results)) {
                    $response = new \stdClass();
                    $response->status = 'SUCCESS';
                    $response->error_message = null;
                    $response->results = $responseContent->results[0]->geometry->location;
                } else {
                    $response = $responseContent;//error
                }
            } else {
                $response = new \stdClass();
                $response->status = 'BAD_REQUEST';
                $response->error_message = 'Bad Request';
                $response->results = [];
            }
        } catch (ConnectException $ex) {
            $response = new \stdClass();
            $response->status = 'CONNECTION_ERROR';
            $response->error_message = 'Cannot Connect';
            $response->results = [];
        } catch (\Exception $ex) {
            $response = new \stdClass();
            $response->status = 'INTERNAL_ERROR';
            $response->error_message = $ex->getMessage();
            $response->results = [];
        }
        return $response;
    }
}