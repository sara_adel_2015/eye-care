<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:41 AM
 */

namespace App\Enums;


class TasksEnum extends \MyCLabs\Enum\Enum
{

    const PAYMENT_RECURRING_TASK = [
        'name' => 'Payment Recurring Task',
        'command' => 'doctor:payment-recurring',
    ];
}