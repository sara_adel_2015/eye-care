<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:41 AM
 */

namespace App\Enums;


class EyeTestStatusEnum extends \MyCLabs\Enum\Enum
{

    const ACTIVE = 'Active';
    const IN_ACTIVE = 'Inactive';
}