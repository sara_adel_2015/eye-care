<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/1/2020
 * Time: 3:41 AM
 */

namespace App\Enums;


class AdminStatusEnum extends \MyCLabs\Enum\Enum
{

    const ACTIVE = 1;
    const IN_ACTIVE = 0;
}