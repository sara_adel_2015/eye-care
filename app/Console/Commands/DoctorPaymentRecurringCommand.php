<?php

namespace App\Console\Commands;

use App\Entities\Doctor;
use App\Entities\DoctorPaymentCards;
use App\Entities\DoctorPaymentRecurringHistory;
use App\Repositories\DoctorPaymentCardsRepository;
use App\Repositories\DoctorRepository;
use Illuminate\Console\Command;
use Omnipay\Omnipay;
use LaravelDoctrine\ORM\Facades\EntityManager;

class DoctorPaymentRecurringCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doctor:payment-recurring {doctorId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Doctor Payment Recurring';
    /**
     * @var $repo DoctorRepository
     */
    protected $repo;
    /**
     * @var $paymentCardRepo DoctorPaymentCardsRepository
     */
    protected $paymentCardRepo;
    /**
     * @var
     */
    protected $doctorId;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->repo = EntityManager::getRepository(Doctor::class);
        $this->paymentCardRepo = EntityManager::getRepository(DoctorPaymentCards::class);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->doctorId = $this->argument('doctorId');
        /**
         * @var $doctor Doctor
         */
        if ($this->doctorId) {
            $doctor = $this->repo->find($this->doctorId);
            $paymentCardId = $doctor->getCurrentPaymentCardId();
            /**
             * @var $paymentCard DoctorPaymentCards
             */
            $paymentCard = $this->paymentCardRepo->find($paymentCardId);
            $amount = env('RECURRING_CHARGE_AMOUNT', 20);
            $gateway = Omnipay::create('Stripe');
            $gateway->setApiKey(env('STRIPE_API_KEY'));
            $paymentResponse = $gateway->authorize(['amount' => $amount, 'currency' => 'USD', 'cardReference' => $paymentCard->getCardReference(), 'customerReference' => $paymentCard->getCustomerReference()])->send();
            $doctorPaymentRecurringHistory = new DoctorPaymentRecurringHistory();
            $doctorPaymentRecurringHistory->setDoctorId($this->doctorId);
            $doctorPaymentRecurringHistory->setPaymentCardId($paymentCardId);
            $doctorPaymentRecurringHistory->setAmount($amount);
            if ($paymentResponse->isSuccessful()) {
                $doctorPaymentRecurringHistory->setIsSuccess(true);
                $doctorPaymentRecurringHistory->setPaymentTransactionId($paymentResponse->getTransactionReference());
                $doctorPaymentRecurringHistory->setMessage('Success');
            } else {
                //log as error
                $doctorPaymentRecurringHistory->setIsSuccess(false);
                $doctorPaymentRecurringHistory->setPaymentTransactionId($paymentResponse->getTransactionReference());
                $doctorPaymentRecurringHistory->setMessage($paymentResponse->getMessage());
            }
            EntityManager::persist($doctorPaymentRecurringHistory);
            EntityManager::flush();
            //$cardResponse = $gateway->createCard(['card' => $cardObj, 'customerReference' => $event->user->getId()])->send();
        } else {
            //doctor not exists
        }

    }
}