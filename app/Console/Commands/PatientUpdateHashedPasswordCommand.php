<?php

namespace App\Console\Commands;

use App\Entities\Patient;
use App\Repositories\PatientRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Illuminate\Support\Facades\Hash;

class PatientUpdateHashedPasswordCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patient:update-password';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Doctor Update Hashed Password';
    /**
     * @var $repo PatientRepository
     */
    protected $repo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->repo = EntityManager::getRepository(Patient::class);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $patients = $this->repo->findAll();
        if (!empty($patients)) {
            Log::info("start updating patient password");
            foreach ($patients as $patient) {
                $patient->setPassword(Hash::make($patient->getPassword()));
                Log::info("updateing patient" . $patient->getEmail() . ' with password ' . $patient->getPassword());
                EntityManager::flush();
            }
        }
    }
}