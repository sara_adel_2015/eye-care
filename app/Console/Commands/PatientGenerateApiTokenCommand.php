<?php

namespace App\Console\Commands;

use App\Entities\Patient;
use App\Repositories\PatientRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Illuminate\Support\Str;

class PatientGenerateApiTokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patient:generate-api-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Doctor Generate Api Token';
    /**
     * @var $repo PatientRepository
     */
    protected $repo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->repo = EntityManager::getRepository(Patient::class);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $patients = $this->repo->findBy(['api_token' => NULL]);
        if (!empty($patients)) {
            Log::info("start updating patient password");
            foreach ($patients as $patient) {
                /**
                 * @var $patient Patient
                 */
                $patient->setApiToken(Str::random(60));
                Log::info("updateing patient" . $patient->getEmail() . ' with api token ' . $patient->getApiToken());
                EntityManager::flush();
            }
        }
    }
}