<?php

namespace App\Console;

use App\Console\Commands\DoctorPaymentRecurringCommand;
use App\Console\Commands\PatientGenerateApiTokenCommand;
use App\Entities\Task;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use LaravelDoctrine\ORM\Facades\EntityManager;
use App\Console\Commands\PatientUpdateHashedPasswordCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        DoctorPaymentRecurringCommand::class,
        PatientUpdateHashedPasswordCommand::class,
        PatientGenerateApiTokenCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $repo = EntityManager::getRepository(Task::class);
        $tasks = $repo->findAll();
        if (!empty($tasks)) {
            /**
             * @var $task Task
             */
            foreach ($tasks as $task) {
                $parameters = $task->getParameters();
                $createdAt = ($task->getCreatedAt()) ? $task->getCreatedAt() : new \DateTime();
                $date = strtotime($createdAt->format('Y-m-d H:i:s'));
                $runHourOn = date('h:i', $date);
                $command = $task->getCommand() . ' ' . $parameters['doctorId'];
                $schedule->command($command)
                    ->monthlyOn($task->getRunAt(), $runHourOn)
                    ->withoutOverlapping()
                    ->appendOutputTo(storage_path() . '/logs/laravel_schedule_output.log')
                    ->after(function () use ($task, $runHourOn) {
                        // Task is complete...
                        $time = strtotime(date('Y-m-' . $task->getRunAt()));
                        $oneMonthLater = date("Y-m-d $runHourOn:s", strtotime("+1 month", $time));
                        $task->setNextRunTime(new \DateTime($oneMonthLater));
                        EntityManager::flush();
                    });
            }
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
