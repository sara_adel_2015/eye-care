<?php

namespace App\Imports;

use App\Exports\DoctorErrorsExporter;
use App\Services\Admin\DictionaryDoctorService;
use App\Services\Admin\DoctorService;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Validation\ValidationException;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Maatwebsite\Excel\Events\BeforeImport;

use Excel;

class DoctorsImport implements ToArray, WithValidation, WithHeadingRow
{
    use Importable, RegistersEventListeners;
    /**
     * @var bool
     */
    public $isExportFile = false;
    /**
     * @var string
     */
    public $importFileName;
    /**
     * @var string
     */
    public $importFolderName = 'doctor-imports';
    /**
     * @var string
     */
    public $exportFilePath;
    /**
     * @var string
     */
    public $importFileExtension;
    /**
     * @var bool
     */
    public $isHeaderValid = true;
    /**
     * @var bool
     */
    public $isFileEmpty = false;

    public static function beforeImport(BeforeImport $event)
    {
        $worksheet = $event->reader->getActiveSheet();
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        if ($highestRow < 2) {
            $error = \Illuminate\Validation\ValidationException::withMessages([]);
            $failure = new Failure(1, 'rows', [0 => 'Now enough rows!']);
            $failures = [0 => $failure];
            throw new ValidationException($error, $failures);
        }
    }

    public function rules(): array
    {
        return [
            '*.doctorname' => 'required',
            '*.speciality' => 'required',
            '*.address' => 'required',
            '*.city' => 'required',
            '*.state' => 'required',
            '*.zip' => 'required',
        ];
    }

    public function array(array $rows)
    {
        if (empty($rows)) {
            $this->isFileEmpty = true;
            return;
        }
        $headerError = [];
        if (!$this->validateHeader(array_keys($rows[0]), $headerError)) {
            return;//header not valid
        }
        $rowsError = [];
        $this->validateFileContent($rows, $rowsError);
        if (!empty($rows)) {
            $this->saveDoctors($rows);
        }
        if (!empty($rowsError)) {
            $this->isExportFile = true;
            $this->exportErrorsToFile($rowsError);
        }
    }

    private function validateFileContent(&$rows, &$rowsError)
    {
        try {
            Validator::make($rows, $this->rules())->validate();
        } catch (ValidationException $e) {
            if (!empty($e->errors())) {
                foreach ($e->errors() as $errorKey => $error) {
                    $errorKeyIndex = explode('.', $errorKey);
                    $index = $errorKeyIndex[0];
                    $rowError = $rows[$index];
                    $rowError['error_messages'] = implode(',', $error);
                    $rowsError[] = $rowError;
                    unset($rows[$index]);
                }
            }
        }
    }

    /**
     * @param $rows
     */
    private function saveDoctors($rows)
    {
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $doctor = [
                    'doctorName' => $row['doctorname'],
                    'speciality' => $row['speciality'],
                    'address' => $row['address'],
                    'city' => $row['city'],
                    'state' => $row['state'],
                    'zip' => $row['zip'],
                    'weblink' => $row['weblink'],
                    'phone' => $row['phone'],
                    'iphoneImage' => $row['iphoneimage']
                ];
                $service = app(DictionaryDoctorService::class);
                $service->addOrUpdateDoctor($doctor, 'add', true, false);
            }
            EntityManager::flush();
        }
    }

    /**
     * @param $rowsError
     * @return mixed
     */
    private function exportErrorsToFile($rowsError)
    {
        $header = $this->headings();
        $header[] = 'errorMessage';
        $export = new DoctorErrorsExporter(
            [$this->headings(), $rowsError]
        );
        $fileName = 'error_' . date('y-m-d h:i:s') . $this->importFileName;
        $this->exportFilePath = $this->importFolderName . '/errors/' . $fileName;
        Excel::store($export, $this->exportFilePath, 'uploads');
    }

    public function headings()
    {
        return [
            'DoctorName',
            'Speciality',
            'Address',
            'City',
            'State',
            'Zip',
            'Weblink',
            'Phone',
            'IphoneImage',
        ];
    }

    private function validateHeader($fileHeaders, &$headerErrors)
    {
        $originalSheetHeader = array_map(function ($header) {
            return strtolower($header);
        }, $this->headings());
        foreach ($originalSheetHeader as $originalHeader) {
            if (!in_array($originalHeader, $fileHeaders)) {
                $headerErrors[] = $originalHeader . ' header is missing';
                $this->isHeaderValid = false;
            }
        }
        return $this->isHeaderValid;
    }

}
