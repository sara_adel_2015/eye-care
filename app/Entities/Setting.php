<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use LaravelDoctrine\ORM\Facades\EntityManager;


/**
 * @ORM\Entity
 * @ORM\Table(name=" tblsettings")
 * @ORM\Entity(repositoryClass="\App\Repositories\SettingRepository")
 */
class Setting
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer" , name="ID")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer" , name="ALLOWED_COMPARE_RATIO")
     */
    protected $allowedCompareRatio;

    /**
     * @ORM\Column(type="integer" , name="ALLOWED_SIMILITRY_PERCENTAGE")
     */
    protected $allowedSimilitryPercentage;


    /**
     * @ORM\Column(type="string" , name="DIFFERENT_RESULT_RECIEPTION")
     */
    protected $differentResultReception;

    /**
     * @ORM\Column(type="string" , name="MATCHED_RESULT_RECIEPTION")
     */
    protected $matchedResultReception;

    /**
     * @ORM\Column(type="string" , name="DIFFERENT_RESULT_RECIEPTION_EMAIL")
     */
    protected $differentResultReceptionEmail;

    /**
     * @ORM\Column(type="string" , name="NOTIFICATION_EMAIL_NOT_MATCHED")
     */
    protected $notificationEmailNotMatched;

    /**
     * @ORM\Column(type="string" , name="NOTIFICATION_EMAIL_MATCHED")
     */
    protected $notificationEmailMatched;
    /**
     * @ORM\Column(type="string" , name="default_physician_id")
     */
    protected $defaultPhysician;
    /**
     * @ORM\Column(type="float" , name="doctor_charge_recurring_amount")
     */
    protected $doctorChargeRecurringAmount = 10;

    /**
     * @ORM\Column(type="string" , name="subscription_plan_id")
     */
    protected $subscriptionPlanId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAllowedCompareRatio()
    {
        return $this->allowedCompareRatio;
    }

    /**
     * @return mixed
     */
    public function getAllowedSimilitryPercentage()
    {
        return $this->allowedSimilitryPercentage;
    }

    /**
     * @return mixed
     */
    public function getDifferentResultReception()
    {
        return $this->differentResultReception;
    }

    /**
     * @return mixed
     */
    public function getDifferentResultReceptionEmail()
    {
        return $this->differentResultReceptionEmail;
    }

    /**
     * @return mixed
     */
    public function getNotificationEmailNotMatched()
    {
        return $this->notificationEmailNotMatched;
    }

    /**
     * @return mixed
     */
    public function getNotificationEmailMatched()
    {
        return $this->notificationEmailMatched;
    }


    /**
     * @param mixed $id
     * @return Setting
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param mixed $allowedCompareRatio
     * @return Setting
     */
    public function setAllowedCompareRatio($allowedCompareRatio)
    {
        $this->allowedCompareRatio = $allowedCompareRatio;
        return $this;
    }

    /**
     * @param mixed $allowedSimilitryPercentage
     * @return Setting
     */
    public function setAllowedSimilitryPercentage($allowedSimilitryPercentage)
    {
        $this->allowedSimilitryPercentage = $allowedSimilitryPercentage;
        return $this;
    }

    /**
     * @param mixed $differentResultReception
     * @return Setting
     */
    public function setDifferentResultReception($differentResultReception)
    {
        $this->differentResultReception = $differentResultReception;
        return $this;
    }

    /**
     * @param mixed $differentResultReceptionEmail
     * @return Setting
     */
    public function setDifferentResultReceptionEmail($differentResultReceptionEmail)
    {
        $this->differentResultReceptionEmail = $differentResultReceptionEmail;
        return $this;
    }

    /**
     * @param mixed $notificationEmailNotMatched
     * @return Setting
     */
    public function setNotificationEmailNotMatched($notificationEmailNotMatched)
    {
        $this->notificationEmailNotMatched = $notificationEmailNotMatched;
        return $this;
    }

    /**
     * @param mixed $notificationEmailMatched
     * @return Setting
     */
    public function setNotificationEmailMatched($notificationEmailMatched)
    {
        $this->notificationEmailMatched = $notificationEmailMatched;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefaultPhysician()
    {
        return $this->defaultPhysician;
    }

    /**
     * @param mixed $defaultPhysician
     * @return Setting
     */
    public function setDefaultPhysician($defaultPhysician)
    {
        $this->defaultPhysician = $defaultPhysician;
        return $this;
    }

    public function getDefaultPhysicianName()
    {
        if ($this->getDefaultPhysician()) {
            $doctorRepo = EntityManager::getRepository(Doctor::class);
            $doctor = $doctorRepo->find($this->getDefaultPhysician());
            return ($doctor) ? $doctor->getDoctorName() : '';
        }
        return '';
    }

    /**
     * @return mixed
     */
    public function getMatchedResultReception()
    {
        return $this->matchedResultReception;
    }

    /**
     * @param mixed $matchedResultReception
     * @return Setting
     */
    public function setMatchedResultReception($matchedResultReception)
    {
        $this->matchedResultReception = $matchedResultReception;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoctorChargeRecurringAmount()
    {
        return $this->doctorChargeRecurringAmount;
    }

    /**
     * @param mixed $doctorChargeRecurringAmount
     * @return Setting
     */
    public function setDoctorChargeRecurringAmount($doctorChargeRecurringAmount)
    {
        $this->doctorChargeRecurringAmount = $doctorChargeRecurringAmount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionPlanId()
    {
        return $this->subscriptionPlanId;
    }

    /**
     * @param mixed $subscriptionPlanId
     * @return Setting
     */
    public function setSubscriptionPlanId($subscriptionPlanId)
    {
        $this->subscriptionPlanId = $subscriptionPlanId;
        return $this;
    }
}