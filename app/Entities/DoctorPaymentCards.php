<?php

namespace App\Entities;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tbl_doctor_payment_cards")
 * @ORM\Entity(repositoryClass="\App\Repositories\DoctorPaymentCardsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class DoctorPaymentCards
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer" , name="id")
     */
    protected $id;

    /**
     * @ORM\Column(type="string" ,  name="card_reference")
     */
    protected $cardReference;

    /**
     * @ORM\Column(type="string" ,  name="customer_reference")
     */
    protected $customerReference;

    /**
     * @ORM\Column(type="string" , name="last_four_digits")
     */
    protected $lastFourDigits;


    /**
     * @ORM\Column(type="datetime" , name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime" , name="updated_at")
     */
    protected $updatedAt;

    /**
     * Unidirectional - Many-To-One
     *
     * @ORM\ManyToOne(targetEntity="Doctor")
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="intId")
     */
    protected $doctor;

    /**
     * DoctorList constructor.
     */
    public function __construct()
    {
    }

    /** @ORM\PrePersist */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PrePersist */
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return DoctorPaymentCards
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardReference()
    {
        return $this->cardReference;
    }

    /**
     * @param mixed $cardReference
     * @return DoctorPaymentCards
     */
    public function setCardReference($cardReference)
    {
        $this->cardReference = $cardReference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerReference()
    {
        return $this->customerReference;
    }

    /**
     * @param mixed $customerReference
     * @return DoctorPaymentCards
     */
    public function setCustomerReference($customerReference)
    {
        $this->customerReference = $customerReference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastFourDigits()
    {
        return $this->lastFourDigits;
    }

    /**
     * @param mixed $lastFourDigits
     * @return DoctorPaymentCards
     */
    public function setLastFourDigits($lastFourDigits)
    {
        $this->lastFourDigits = $lastFourDigits;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return DoctorPaymentCards
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return DoctorPaymentCards
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     * @return DoctorPaymentCards
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
        return $this;
    }

}