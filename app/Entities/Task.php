<?php

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping AS ORM;
use App\Enums\DoctorStatusEnum;

/**
 * @ORM\Entity
 * @ORM\Table(name="tasks")
 * @ORM\Entity(repositoryClass="\App\Repositories\TaskRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer" , name="id")
     */
    protected $id;

    /**
     * @ORM\Column(type="string" ,  name="task_name")
     */
    protected $taskName;

    /**
     * @ORM\Column(type="text" , name="command")
     */
    protected $command;
    /**
     * @ORM\Column(type="json" , name="parameters")
     */
    protected $parameters;
    /**
     * @ORM\Column(type="string" , name="run_every")
     */
    protected $runEvery;

    /**
     * @ORM\Column(type="string" , name="run_at")
     */
    protected $runAt;

    /**
     * @ORM\Column(type="datetime" , name="next_run_time")
     */
    protected $nextRunTime;
    /**
     * @ORM\Column(type="datetime" , name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime" , name="updated_at")
     */
    protected $updatedAt;


    /**
     * DoctorList constructor.
     */
    public function __construct()
    {
    }

    /** @ORM\PrePersist */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PrePersist */
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->setUpdatedAt(new \DateTime());
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Task
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaskName()
    {
        return $this->taskName;
    }

    /**
     * @param mixed $taskName
     * @return Task
     */
    public function setTaskName($taskName)
    {
        $this->taskName = $taskName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param mixed $command
     * @return Task
     */
    public function setCommand($command)
    {
        $this->command = $command;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRunEvery()
    {
        return $this->runEvery;
    }

    /**
     * @param mixed $runEvery
     * @return Task
     */
    public function setRunEvery($runEvery)
    {
        $this->runEvery = $runEvery;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRunAt()
    {
        return $this->runAt;
    }

    /**
     * @param mixed $runAt
     * @return Task
     */
    public function setRunAt($runAt)
    {
        $this->runAt = $runAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNextRunTime()
    {
        return $this->nextRunTime;
    }

    /**
     * @param mixed $nextRunTime
     * @return Task
     */
    public function setNextRunTime($nextRunTime)
    {
        $this->nextRunTime = $nextRunTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param mixed $parameters
     * @return Task
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Task
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return Task
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }


}