<?php

namespace App\Entities;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="doctor_payment_recurring_history")
 * @ORM\Entity(repositoryClass="\App\Repositories\DoctorPaymentRecurringHistoryRepository")
 * @ORM\HasLifecycleCallbacks
 */
class DoctorPaymentRecurringHistory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer" , name="id")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer" ,  name="doctor_id")
     */
    protected $doctorId;

    /**
     * @ORM\Column(type="integer" ,  name="payment_card_id")
     */
    protected $paymentCardId;

    /**
     * @ORM\Column(type="string" , name="charge_id")
     */
    protected $chargeId;

    /**
     * @ORM\Column(type="string" , name="event_id")
     */
    protected $eventId;

    /**
     * @ORM\Column(type="string" , name="invoice_id")
     */
    protected $invoiceId;
    /**
     * @ORM\Column(type="string" , name="invoice_pdf_url")
     */
    protected $invoicePdfUrl;
    /**
     * @ORM\Column(type="string" , name="customer_reference")
     */
    protected $customerReference;
    /**
     * @ORM\Column(type="string" , name="card_reference")
     */
    protected $cardReference;
    /**
     * @ORM\Column(type="decimal" , name="amount")
     */
    protected $amount;
    /**
     * @ORM\Column(type="boolean" , name="is_success")
     */
    protected $isSuccess;

    /**
     * @ORM\Column(type="text" , name="message")
     */
    protected $message;

    /**
     * @ORM\Column(type="string" , name="subscription_plan_id")
     */
    protected $subscriptionPlanId;


    /**
     * @ORM\Column(type="datetime" , name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime" , name="updated_at")
     */
    protected $updatedAt;

    /**
     * DoctorList constructor.
     */
    public function __construct()
    {
    }

    /** @ORM\PrePersist */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $this->setCreatedAt(new \DateTime());
    }

    /** @ORM\PrePersist */
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return DoctorPaymentRecurringHistory
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoctorId()
    {
        return $this->doctorId;
    }

    /**
     * @param mixed $doctorId
     * @return DoctorPaymentRecurringHistory
     */
    public function setDoctorId($doctorId)
    {
        $this->doctorId = $doctorId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentCardId()
    {
        return $this->paymentCardId;
    }

    /**
     * @param mixed $paymentCardId
     * @return DoctorPaymentRecurringHistory
     */
    public function setPaymentCardId($paymentCardId)
    {
        $this->paymentCardId = $paymentCardId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return DoctorPaymentRecurringHistory
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisSuccess()
    {
        return $this->isSuccess;
    }

    /**
     * @param mixed $isSuccess
     * @return DoctorPaymentRecurringHistory
     */
    public function setIsSuccess($isSuccess)
    {
        $this->isSuccess = $isSuccess;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return DoctorPaymentRecurringHistory
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return DoctorPaymentRecurringHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return DoctorPaymentRecurringHistory
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChargeId()
    {
        return $this->chargeId;
    }

    /**
     * @param mixed $chargeId
     * @return DoctorPaymentRecurringHistory
     */
    public function setChargeId($chargeId)
    {
        $this->chargeId = $chargeId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param mixed $eventId
     * @return DoctorPaymentRecurringHistory
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param mixed $invoiceId
     * @return DoctorPaymentRecurringHistory
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoicePdfUrl()
    {
        return $this->invoicePdfUrl;
    }

    /**
     * @param mixed $invoicePdfUrl
     * @return DoctorPaymentRecurringHistory
     */
    public function setInvoicePdfUrl($invoicePdfUrl)
    {
        $this->invoicePdfUrl = $invoicePdfUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerReference()
    {
        return $this->customerReference;
    }

    /**
     * @param mixed $customerReference
     * @return DoctorPaymentRecurringHistory
     */
    public function setCustomerReference($customerReference)
    {
        $this->customerReference = $customerReference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardReference()
    {
        return $this->cardReference;
    }

    /**
     * @param mixed $cardReference
     * @return DoctorPaymentRecurringHistory
     */
    public function setCardReference($cardReference)
    {
        $this->cardReference = $cardReference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionPlanId()
    {
        return $this->subscriptionPlanId;
    }

    /**
     * @param mixed $subscriptionPlanId
     * @return DoctorPaymentRecurringHistory
     */
    public function setSubscriptionPlanId($subscriptionPlanId)
    {
        $this->subscriptionPlanId = $subscriptionPlanId;
        return $this;
    }
}