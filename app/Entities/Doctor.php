<?php

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping AS ORM;
use App\Enums\DoctorStatusEnum;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @ORM\Entity
 * @ORM\Table(name="tbldoctorlist")
 * @ORM\Entity(repositoryClass="\App\Repositories\DoctorRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Doctor implements Authenticatable, \Illuminate\Contracts\Auth\CanResetPassword
{
    use \LaravelDoctrine\ORM\Auth\Authenticatable;
    use CanResetPassword;
    use Notifiable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer" , name="intId")
     */
    protected $id;

    /**
     * @ORM\Column(type="string" ,  name="doctorname")
     */
    protected $doctorName;

    /**
     * @ORM\Column(type="string" , name="email")
     */
    protected $email;

    /**
     * @ORM\Column(type="text" , name="speciality")
     */
    protected $speciality;

    /**
     * @ORM\Column(type="text" , name="address")
     */
    protected $address;

    /**
     * @ORM\Column(type="string" , name="city")
     */
    protected $city;

    /**
     * @ORM\Column(type="string" , name="state")
     */
    protected $state;

    /**
     * @ORM\Column(type="string" , name="zip")
     */
    protected $zip;
    /**
     * @ORM\Column(type="float" , name="latitude")
     */
    protected $latitude;
    /**
     * @ORM\Column(type="float" , name="longitude")
     */
    protected $longitude;
    /**
     * @ORM\Column(type="string" , name="weblink")
     */
    protected $weblink;
    /**
     * @ORM\Column(type="string" , name="iphoneimage")
     */
    protected $iphoneImage;
    /**
     * @ORM\Column(type="string" , name="status")
     */
    protected $status;

    /**
     * @ORM\Column(type="string" , name="dateadded")
     */
    protected $dateAdded;

    /**
     * @ORM\Column(type="integer" , name="strorder")
     */
    protected $strorder;

    /**
     * @ORM\Column(type="string" , name="phone")
     */
    protected $phone;


    /**
     * @ORM\Column(type="string" , name="api_token")
     */
    protected $api_token;

    /**
     * @ORM\Column(type="string" , name="doctor_code")
     */
    protected $doctorCode;
    /**
     * @ORM\Column(type="string" , name="password")
     */
    protected $password;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     *
     * @ORM\OneToMany(targetEntity="Patient", mappedBy="doctor", cascade={"persist"})
     */

    protected $patients;


    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     *
     * @ORM\OneToMany(targetEntity="EyeTest", mappedBy="doctor", cascade={"persist"})
     */

    protected $eyeTests;
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     *
     * @ORM\OneToMany(targetEntity="DoctorPaymentCards", mappedBy="doctor", cascade={"persist"})
     */

    protected $paymentCards = [];

    /**
     * @ORM\Column(type="integer" , name="current_payment_card_id")
     */
    protected $currentPaymentCardId;
    /**
     * @ORM\Column(type="datetime" , name="next_recurring_date")
     */
    protected $nextRecurringDate;

    /**
     * @ORM\Column(type="string" , name="subscription_id")
     */
    protected $subscriptionId;

    /**
     * @ORM\Column(type="string" , name="customer_reference")
     */
    protected $customerReference;
    /**
     * @ORM\Column(type="boolean" , name="is_subscription_cancelled")
     */
    protected $isSubscriptionCancelled;
    /**
     * @ORM\Column(type="boolean" , name="is_request_to_cancel_subscription")
     */
    protected $isRequestToCancelSubscription;

    /**
     * DoctorList constructor.
     */
    public function __construct()
    {
        $this->setStatus(DoctorStatusEnum::ACTIVE);
        $this->patients = new ArrayCollection();
        $this->paymentCards = new ArrayCollection();
    }

    /** @ORM\PrePersist */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $this->setDateAdded(date('Y-m-d H:i:s'));
        $this->setRememberToken(' ');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Doctor
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoctorName()
    {
        return $this->doctorName;
    }

    /**
     * @param mixed $doctorName
     * @return Doctor
     */
    public function setDoctorName($doctorName)
    {
        $this->doctorName = $doctorName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }

    /**
     * @param mixed $speciality
     * @return Doctor
     */
    public function setSpeciality($speciality)
    {
        $this->speciality = $speciality;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Doctor
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Doctor
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     * @return Doctor
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     * @return Doctor
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return !empty(trim($this->latitude)) ? $this->latitude : 0.00;
    }

    /**
     * @param mixed $latitude
     * @return Doctor
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return !empty(trim($this->longitude)) ? $this->longitude : 0.00;
    }

    /**
     * @param mixed $longitude
     * @return Doctor
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWeblink()
    {
        return $this->weblink;
    }

    /**
     * @param mixed $weblink
     * @return Doctor
     */
    public function setWeblink($weblink)
    {
        $this->weblink = $weblink;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIphoneImage()
    {
        return $this->iphoneImage;
    }

    /**
     * @param mixed $iphoneImage
     * @return Doctor
     */
    public function setIphoneImage($iphoneImage)
    {
        $this->iphoneImage = $iphoneImage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Doctor
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * @param mixed $dateAdded
     * @return Doctor
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrorder()
    {
        return $this->strorder;
    }

    /**
     * @param mixed $strorder
     * @return Doctor
     */
    public function setStrorder($strorder)
    {
        $this->strorder = $strorder;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Doctor
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Doctor
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApiToken()
    {
        return $this->api_token;
    }

    /**
     * @param mixed $api_token
     * @return Doctor
     */
    public function setApiToken($api_token)
    {
        $this->api_token = $api_token;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getPatients()
    {
        return $this->patients;
    }

    /**
     * @param mixed $patients
     * @return Doctor
     */
    public function setPatients($patients)
    {
        $this->patients = $patients;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentCards()
    {
        return $this->paymentCards;
    }

    /**
     * @param mixed $paymentCards
     * @return Doctor
     */
    public function setPaymentCards($paymentCards)
    {
        $this->paymentCards = $paymentCards;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentPaymentCardId()
    {
        return $this->currentPaymentCardId;
    }

    /**
     * @param mixed $currentPaymentCardId
     * @return Doctor
     */
    public function setCurrentPaymentCardId($currentPaymentCardId)
    {
        $this->currentPaymentCardId = $currentPaymentCardId;
        return $this;
    }

    public function addPaymentCard(DoctorPaymentCards $paymentCard)
    {
        $this->paymentCards[] = $paymentCard;
        $paymentCard->setDoctor($this);
    }

    /**
     * @return mixed
     */
    public function getNextRecurringDate()
    {
        return $this->nextRecurringDate;
    }

    /**
     * @param mixed $nextRecurringDate
     * @return DoctorPaymentRecurringHistory
     */
    public function setNextRecurringDate($nextRecurringDate)
    {
        $this->nextRecurringDate = $nextRecurringDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return Doctor
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEyeTests()
    {
        return $this->eyeTests;
    }

    /**
     * @param mixed $eyeTests
     * @return Doctor
     */
    public function setEyeTests($eyeTests)
    {
        $this->eyeTests = $eyeTests;
        return $this;
    }

    public static function formatDoctor(Doctor $doctorEntity)
    {
        return [
            'id' => $doctorEntity->getId(),
            'doctorName' => $doctorEntity->getDoctorName(),
            'email' => $doctorEntity->getEmail(),
            'apiToken' => $doctorEntity->getApiToken()
        ];
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param string $value
     *
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->rememberToken = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'rememberToken';
    }

    public function getAuthenticationType()
    {
        return 'doctor';
    }

    /**
     * @return mixed
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }

    /**
     * @param mixed $subscriptionId
     * @return Doctor
     */
    public function setSubscriptionId($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsSubscriptionCancelled()
    {
        return $this->isSubscriptionCancelled;
    }

    /**
     * @param mixed $isSubscriptionCancelled
     * @return Doctor
     */
    public function setIsSubscriptionCancelled($isSubscriptionCancelled)
    {
        $this->isSubscriptionCancelled = $isSubscriptionCancelled;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsRequestToCancelSubscription()
    {
        return $this->isRequestToCancelSubscription;
    }

    /**
     * @param mixed $isRequestToCancelSubscription
     * @return Doctor
     */
    public function setIsRequestToCancelSubscription($isRequestToCancelSubscription)
    {
        $this->isRequestToCancelSubscription = $isRequestToCancelSubscription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerReference()
    {
        return $this->customerReference;
    }

    /**
     * @param mixed $customerReference
     * @return Doctor
     */
    public function setCustomerReference($customerReference)
    {
        $this->customerReference = $customerReference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoctorCode()
    {
        return $this->doctorCode;
    }

    /**
     * @param mixed $doctorCode
     * @return Doctor
     */
    public function setDoctorCode($doctorCode)
    {
        $this->doctorCode = $doctorCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionCurrency()
    {
        return $this->subscriptionCurrency;
    }

    /**
     * @param mixed $subscriptionCurrency
     * @return Doctor
     */
    public function setSubscriptionCurrency($subscriptionCurrency)
    {
        $this->subscriptionCurrency = $subscriptionCurrency;
        return $this;
    }

}