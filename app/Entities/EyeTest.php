<?php

namespace App\Entities;

use App\Enums\EyeTestStatusEnum;
use Doctrine\ORM\Mapping AS ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * @ORM\Entity
 * @ORM\Table(name="tbleyetest")
 * @ORM\Entity(repositoryClass="\App\Repositories\EyeTestRepository")
 * @ORM\HasLifecycleCallbacks
 */
class EyeTest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer" , name="intId")
     */
    protected $id;
    /**
     * @ORM\Column(type="integer" ,  name="patientid")
     */
    protected $patientId;

    /**
     * @ORM\Column(type="text" ,  name="imagename")
     */
    protected $imageName;

    /**
     * @ORM\Column(type="string" ,  name="imagetype")
     */
    protected $imageType;

    /**
     * @ORM\Column(type="string" ,  name="devicetype")
     */
    protected $deviceType;

    /**
     * @ORM\Column(type="string" ,  name="comparepattern")
     */
    protected $comparePattern;

    /**
     * @ORM\Column(type="string" ,  name="status")
     */
    protected $status;
    /**
     * @ORM\Column(type="string" ,  name="sentmail")
     */
    protected $sentMail;
    /**
     * @ORM\Column(type="datetime" ,  name="dateadded")
     */
    protected $dateAdded;
    /**
     * Unidirectional - Many-To-One
     *
     * @ORM\ManyToOne(targetEntity="Doctor")
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="intId")
     */

    protected $doctor;

    /** @ORM\PrePersist */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $this->setDateAdded(new \DateTime());
        $this->setStatus(EyeTestStatusEnum::ACTIVE);
        $this->setSentMail('0');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return EyeTest
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param mixed $imageName
     * @return EyeTest
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageType()
    {
        return $this->imageType;
    }

    /**
     * @param mixed $imageType
     * @return EyeTest
     */
    public function setImageType($imageType)
    {
        $this->imageType = $imageType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * @param mixed $deviceType
     * @return EyeTest
     */
    public function setDeviceType($deviceType)
    {
        $this->deviceType = $deviceType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComparePattern()
    {
        return $this->comparePattern;
    }

    /**
     * @param mixed $comparePattern
     * @return EyeTest
     */
    public function setComparePattern($comparePattern)
    {
        $this->comparePattern = $comparePattern;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return EyeTest
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSentMail()
    {
        return $this->sentMail;
    }

    /**
     * @param mixed $sentMail
     * @return EyeTest
     */
    public function setSentMail($sentMail)
    {
        $this->sentMail = $sentMail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * @param mixed $dateAdded
     * @return EyeTest
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPatientId()
    {
        return $this->patientId;
    }

    /**
     * @param mixed $patientId
     * @return EyeTest
     */
    public function setPatientId($patientId)
    {
        $this->patientId = $patientId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     * @return EyeTest
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
        return $this;
    }


}