<?php

namespace App\Entities;

use App\Enums\PatientStatusEnum;
use Doctrine\ORM\Mapping AS ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @ORM\Entity
 * @ORM\Table(name="tblpatientlist")
 * @ORM\Entity(repositoryClass="\App\Repositories\PatientRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Patient implements Authenticatable, \Illuminate\Contracts\Auth\CanResetPassword
{

    use \LaravelDoctrine\ORM\Auth\Authenticatable;
    use CanResetPassword;
    use Notifiable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer" , name="intId")
     */
    protected $id;

    /**
     * @ORM\Column(type="string" , name="firstname")
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string" , name="lastname")
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string" , name="email")
     */
    protected $email;

    /**
     * @ORM\Column(type="string" , name="password")
     */
    protected $password;

    /**
     * @ORM\Column(type="text" ,  name="address")
     */
    protected $address;
    /**
     * @ORM\Column(type="string" , name="city")
     */
    protected $city;

    /**
     * @ORM\Column(type="string" , name="state")
     */
    protected $state;

    /**
     * @ORM\Column(type="string" , name="zip")
     */
    protected $zip;

    /**
     * @ORM\Column(type="string" , name="phone")
     */
    protected $phone;

    /**
     * @ORM\Column(type="string" , name="status")
     */
    protected $status;
    /**
     * @ORM\Column(type="integer" , name="strorder")
     */
    protected $strorder;

    /**
     * @ORM\Column(type="datetime" , name="dateadded",columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
     */
    protected $dateAdded;

    /**
     * @ORM\Column(type="string" , name="api_token")
     */
    protected $api_token;

    /**
     * Unidirectional - Many-To-One
     *
     * @ORM\ManyToOne(targetEntity="Doctor")
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="intId")
     */
    protected $doctor = null;

    public function __construct()
    {
        $this->setStatus(PatientStatusEnum::ACTIVE);
    }

    /** @ORM\PrePersist */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $this->setDateAdded(new \DateTime());
        $this->setRememberToken(' ');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Patient
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return Patient
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return Patient
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Patient
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return Patient
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Patient
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Patient
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     * @return Patient
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     * @return Patient
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Patient
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Patient
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }


    /**
     * @param mixed $dateAdded
     * @return Patient
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrorder()
    {
        return $this->strorder;
    }

    /**
     * @param mixed $strorder
     */
    public function setStrorder($strorder)
    {
        $this->strorder = $strorder;
        return $this;
    }

    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getApiToken()
    {
        return $this->api_token;
    }

    /**
     * @param mixed $apiToken
     * @return Patient
     */
    public function setApiToken($apiToken)
    {
        $this->api_token = $apiToken;
        return $this;
    }

    public static function formatPatient(Patient $patientEntity)
    {
        return [
            'id' => $patientEntity->getId(),
            'firstName' => $patientEntity->getFirstName(),
            'lastName' => $patientEntity->getLastName(),
            'email' => $patientEntity->getEmail(),
            'apiToken' => $patientEntity->getApiToken()
        ];
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * @return mixed
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param mixed $doctor
     * @return Patient
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
        return $this;
    }

    public function getAuthenticationType()
    {
        return 'patient';
    }
}