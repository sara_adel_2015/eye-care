<?php

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping AS ORM;
use App\Enums\DoctorStatusEnum;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @ORM\Entity
 * @ORM\Table(name="tbl_dictionary_doctor_list")
 * @ORM\Entity(repositoryClass="\App\Repositories\DictionaryDoctorRepository")
 * @ORM\HasLifecycleCallbacks
 */
class DictionaryDoctor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer" , name="intId")
     */
    protected $id;

    /**
     * @ORM\Column(type="string" ,  name="doctorname")
     */
    protected $doctorName;

    /**
     * @ORM\Column(type="text" , name="speciality")
     */
    protected $speciality;

    /**
     * @ORM\Column(type="text" , name="address")
     */
    protected $address;

    /**
     * @ORM\Column(type="string" , name="city")
     */
    protected $city;

    /**
     * @ORM\Column(type="string" , name="state")
     */
    protected $state;

    /**
     * @ORM\Column(type="string" , name="zip")
     */
    protected $zip;
    /**
     * @ORM\Column(type="float" , name="latitude")
     */
    protected $latitude;
    /**
     * @ORM\Column(type="float" , name="longitude")
     */
    protected $longitude;
    /**
     * @ORM\Column(type="string" , name="weblink")
     */
    protected $weblink;
    /**
     * @ORM\Column(type="string" , name="iphoneimage")
     */
    protected $iphoneImage;
    /**
     * @ORM\Column(type="string" , name="status")
     */
    protected $status;

    /**
     * @ORM\Column(type="string" , name="dateadded")
     */
    protected $dateAdded;

    /**
     * @ORM\Column(type="integer" , name="strorder")
     */
    protected $strorder;

    /**
     * @ORM\Column(type="string" , name="phone")
     */
    protected $phone;

    /**
     * DoctorList constructor.
     */
    public function __construct()
    {
        $this->setStatus(DoctorStatusEnum::ACTIVE);
    }

    /** @ORM\PrePersist */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $this->setDateAdded(date('Y-m-d H:i:s'));
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Doctor
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoctorName()
    {
        return $this->doctorName;
    }

    /**
     * @param mixed $doctorName
     * @return Doctor
     */
    public function setDoctorName($doctorName)
    {
        $this->doctorName = $doctorName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }

    /**
     * @param mixed $speciality
     * @return Doctor
     */
    public function setSpeciality($speciality)
    {
        $this->speciality = $speciality;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Doctor
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Doctor
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     * @return Doctor
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     * @return Doctor
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return !empty(trim($this->latitude)) ? $this->latitude : null;
    }

    /**
     * @param mixed $latitude
     * @return Doctor
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return !empty(trim($this->longitude)) ? $this->longitude : null;
    }

    /**
     * @param mixed $longitude
     * @return Doctor
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWeblink()
    {
        return $this->weblink;
    }

    /**
     * @param mixed $weblink
     * @return Doctor
     */
    public function setWeblink($weblink)
    {
        $this->weblink = $weblink;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIphoneImage()
    {
        return $this->iphoneImage;
    }

    /**
     * @param mixed $iphoneImage
     * @return Doctor
     */
    public function setIphoneImage($iphoneImage)
    {
        $this->iphoneImage = $iphoneImage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Doctor
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * @param mixed $dateAdded
     * @return Doctor
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrorder()
    {
        return $this->strorder;
    }

    /**
     * @param mixed $strorder
     * @return Doctor
     */
    public function setStrorder($strorder)
    {
        $this->strorder = $strorder;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Doctor
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

}