<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @ORM\Entity
 * @ORM\Table(name="tbladmin")
 * @ORM\Entity(repositoryClass="\App\Repositories\AdminRepository")
 */
class Admin implements Authenticatable, \Illuminate\Contracts\Auth\CanResetPassword
{
    use \LaravelDoctrine\ORM\Auth\Authenticatable;
    use CanResetPassword;
    use Notifiable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer" , name="intId")
     */
    protected $id;

    /**
     * @ORM\Column(type="string" ,  name="username")
     */
    protected $userName;

    /**
     * @ORM\Column(type="string" , name="password")
     */
    protected $password;

    /**
     * @ORM\Column(type="string" , name="varfname")
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string" , name="varlname")
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string" , name="vaemail")
     */
    protected $email;

    /**
     * @ORM\Column(type="integer" , name="intstatus")
     */
    protected $status;

    /**
     * @ORM\Column(type="datetime" , name="last_visited")
     */
    protected $lastVisited;

    /**
     * @ORM\Column(name="remember_token", type="string", nullable=true)
     */
    protected $rememberToken;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Admin
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     * @return Admin
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return Admin
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return Admin
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return Admin
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Admin
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Admin
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastVisited()
    {
        return $this->lastVisited;
    }

    /**
     * @param mixed $lastVisited
     * @return Admin
     */
    public function setLastVisited($lastVisited)
    {
        $this->lastVisited = $lastVisited;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRememberToken()
    {
        return $this->rememberToken;
    }

    /**
     * @param mixed $rememberToken
     * @return Admin
     */
    public function setRememberToken($rememberToken)
    {
        $this->rememberToken = $rememberToken;
        return $this;
    }

    public function getAuthenticationType()
    {
        return 'admin';
    }
}