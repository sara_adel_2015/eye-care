<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 5/11/2020
 * Time: 1:44 AM
 */

namespace App\Listeners;

use App\Events\DoctorRegisteredEvent;

class SendEmailRegisterNotification
{
    /**
     * handle the Event here
     * @param DoctorRegisteredEvent $event
     */
    public function handle(DoctorRegisteredEvent $event)
    {

    }
}