<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 5/11/2020
 * Time: 1:46 AM
 */

namespace App\Listeners;

use App\Entities\DoctorPaymentCards;
use App\Events\DoctorRegisteredEvent;
use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;
use LaravelDoctrine\ORM\Facades\EntityManager;

class DoctorCreatePaymentCard
{
    /**
     * handle the Event here
     * @param DoctorRegisteredEvent $event
     */
    public function handle(DoctorRegisteredEvent $event)
    {
        dd($event->user->getId());
    }
}