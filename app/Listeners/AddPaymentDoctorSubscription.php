<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 6/9/2020
 * Time: 1:52 AM
 */

namespace App\Listeners;


use App\Entities\Setting;
use App\Events\DoctorRegisteredEvent;
use App\Services\PaymentCommunicator;
use App\Entities\DoctorPaymentCards;
use LaravelDoctrine\ORM\Facades\EntityManager;
use App;
class AddPaymentDoctorSubscription
{
    /**
     * handle the Event here
     * @param DoctorRegisteredEvent $event
     */
    public function handle(DoctorRegisteredEvent $event)
    {
        $currentPaymentId = $event->user->getCurrentPaymentCardId();
        $repo = EntityManager::getRepository(DoctorPaymentCards::class);
        /**
         * @var $currentCard DoctorPaymentCards
         */
        $currentCard = $repo->find($currentPaymentId);
        $settingService = App::make(\App\Services\Admin\AdminService::class);
        /** @var  $settings Setting */
        $settings = $settingService->getSettings();
        $planId = $settings['settings']->getSubscriptionPlanId();
        $gateway = new PaymentCommunicator();
        $subscriptionResponse = $gateway->createSubscription(
            $planId,
            $currentCard->getCustomerReference()
        );
        if ($subscriptionResponse->isSuccessful()) {
            $subscriptionId = $subscriptionResponse->getData()['id'];
            $event->user->setSubscriptionId($subscriptionId);
            EntityManager::flush();
        } else {
            //handle this part later
        }
    }
}