<?php
/**
 * Created by PhpStorm.
 * User: sara.adel
 * Date: 4/24/2020
 * Time: 10:32 PM
 */

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromArray;

class DoctorErrorsExporter implements FromArray
{
    protected $doctors;

    public function __construct(array $doctors)
    {
        $this->doctors = $doctors;
    }

    public function array(): array
    {
        return $this->doctors;
    }
}