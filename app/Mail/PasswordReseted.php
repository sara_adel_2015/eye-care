<?php

namespace App\Mail;

use App\Entities\Patient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordReseted extends Mailable
{
    use Queueable, SerializesModels;

    public $patient;
    public $newPassword;

    /**
     * PasswordReseted constructor.
     * @param Patient $patient
     * @param $newPassword
     */
    public function __construct(Patient $patient, $newPassword)
    {
        $this->patient = $patient;
        $this->newPassword = $newPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Amsler Grid Eye Test | Reset password')
            ->replyTo(env('MAIL_REPLY_TO', 'noreply@amslerapp.com'))
            ->view('emails.patient.reset-password');
    }
}
