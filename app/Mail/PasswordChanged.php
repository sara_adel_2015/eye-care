<?php

namespace App\Mail;

use App\Entities\Patient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordChanged extends Mailable
{
    use Queueable, SerializesModels;

    public $patient;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Patient $patient)
    {
        $this->patient = $patient;
    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Amsler Grid Eye Test | Change password')
            ->replyTo(env('MAIL_REPLY_TO', 'noreply@amslerapp.com'))
            ->view('emails.patient.change-password');
    }
}
