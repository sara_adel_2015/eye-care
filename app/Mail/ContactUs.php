<?php

namespace App\Mail;

use App\Entities\Patient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;

    public $fullName;
    public $emailSubject;
    public $comment;
    public $email;

    /**
     * ContactUs constructor.
     * @param $fullName
     * @param $email
     * @param $emailSubject
     * @param $comment
     */
    public function __construct($fullName, $email, $emailSubject, $comment)
    {
        $this->fullName = $fullName;
        $this->emailSubject = $emailSubject;
        $this->comment = $comment;
        $this->email = $email;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Amsler eye test: Contact Details')
            ->replyTo(env('MAIL_REPLY_TO', 'noreply@amslerapp.com'))
            ->view('emails.contact-us');
    }
}
