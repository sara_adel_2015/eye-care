<?php

namespace App\Mail;

use App\Entities\Patient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EyeTestNotPassed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $patient;
    public $similarity;
    public $fileAttachments;
    public $imageTypeName;

    /**
     * EyeTestNotPassed constructor.
     * @param Patient $patient
     * @param $similarity
     * @param $attachments
     * @param $imageTypeName
     */
    public function __construct(Patient $patient, $similarity, $attachments, $imageTypeName)
    {
        $this->patient = $patient;
        $this->similarity = $similarity;
        $this->fileAttachments = $attachments;
        $this->imageTypeName = $imageTypeName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject("Amsler eye test")
            ->replyTo(env('MAIL_REPLY_TO', 'noreply@amslerapp.com'))
            ->view('emails.patient.eye-test-not-passed');
        //add attachments
        if (!empty($this->fileAttachments)) {
            foreach ($this->fileAttachments as $fileAttachment) {
                $this->attachFromStorageDisk('uploads', 'eyecareImages/' . $fileAttachment, null, [
                    'mime' => 'application/png'
                ]);
            }
        }
    }
}
