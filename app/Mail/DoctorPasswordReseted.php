<?php

namespace App\Mail;

use App\Entities\Doctor;
use App\Entities\Patient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DoctorPasswordReseted extends Mailable
{
    use Queueable, SerializesModels;

    public $doctor;
    public $newPassword;

    /**
     * DoctorPasswordReseted constructor.
     * @param Doctor $doctor
     * @param $newPassword
     */
    public function __construct(Doctor $doctor, $newPassword)
    {
        $this->doctor = $doctor;
        $this->newPassword = $newPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Amsler Grid Eye Test | Reset password')
            ->replyTo(env('MAIL_REPLY_TO', 'noreply@amslerapp.com'))
            ->view('emails.doctor.reset-password');
    }
}
