<?php

namespace App\Mail;

use App\Entities\Doctor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DoctorPassworodChanged extends Mailable
{
    use Queueable, SerializesModels;

    public $doctor;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Doctor $doctor)
    {
        $this->doctor = $doctor;
    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Amsler Grid Eye Test | Change password')
            ->replyTo(env('MAIL_REPLY_TO', 'noreply@amslerapp.com'))
            ->view('emails.doctor.change-password');
    }
}
