function handleAjaxResponse(status) {
    if (status == 200) {
        let queryParams = '?';
        let searchParams = new URLSearchParams(window.location.search);
        let index = 0;
        let searchParamsLength = Array.from(searchParams).length;
        searchParams.forEach(function (value, key) {
            index++;
            queryParams += key + '=' + value;
            if (index !== searchParamsLength) {
                queryParams += '&';
            }
        });
        window.location.href = BASE_URL + queryParams;
    }

}
