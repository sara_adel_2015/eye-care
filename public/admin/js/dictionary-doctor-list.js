const BASE_URL = '/admin/dictionary-doctor';

function deleteDoctor(id) {
    if (confirm("Are you sure want to delete this doctor and unsubscribe from recurring payment process?")) {
        $.ajax({
            method: "DELETE",
            url: BASE_URL + '/' + id,
            data: {"_token": csrf_token,}
        })
            .done(function (response) {
                handleAjaxResponse(response.status);
            })
            .fail(function (jqXHR, textStatus, error) {
                let acc = [];
                $.each(jqXHR, function (index, value) {
                    if(value.errors){
                        acc.push(value.errors[0]);
                    }


                });
                $('#ajax_msg').addClass('error_msg');
                $('#ajax_msg').html(JSON.stringify(acc));
            });
    }
}

function updateStatus(id, status) {
    $.ajax({
        method: "PUT",
        url: BASE_URL + "/update-status/" + id,
        data: {"_token": csrf_token, status: status}
    })
        .done(function (response) {
            handleAjaxResponse(response.status);
        });
}

function updateStrOrder(id, event) {
    if (event.keyCode == 13) {
        $.ajax({
            method: "PUT",
            url: BASE_URL + "/update-order/" + id,
            data: {"_token": csrf_token, strOrder: event.srcElement.value}
        })
            .done(function (response) {
                handleAjaxResponse(response.status);
            });
    }

}


function updateStrOrders() {
    let data = [];
    $('.row-data').each(function () {
        let doctorId = $(this).attr("id");
        let strOrder = $(this).find("#strorder" + doctorId).val()
        data.push({'id': doctorId, 'strOrder': strOrder});
    });
    $.ajax({
        method: "PUT",
        url: BASE_URL + "/update-orders/",
        data: {"_token": csrf_token, "doctorsOrders": data}
    })
        .done(function (response) {
            handleAjaxResponse(response.status);
        });
}

