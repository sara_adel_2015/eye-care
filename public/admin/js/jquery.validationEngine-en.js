(function ($) {
    $.fn.validationEngineLanguage = function () {
    };
    $.validationEngineLanguage = {
        newLang: function () {
            $.validationEngineLanguage.allRules = {
                "required": {    			// Add your regex rules here, you can take telephone as an example
                    "regex": "none",
                    "alertText": "* This field is required",
                    "alertTextCheckboxMultiple": "* Please select an option",
                    "alertTextCheckboxe": "* This checkbox is required"
                },
                "length": {
                    "regex": "none",
                    "alertText": "*Between ",
                    "alertText2": " and ",
                    "alertText3": " characters allowed"
                },
                "maxCheckbox": {
                    "regex": "none",
                    "alertText": "* Checks allowed Exceeded"
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": "* Please select ",
                    "alertText2": " options"
                },
                "confirm": {
                    "regex": "none",
                    "alertText": "* Your field is not matching"
                },
                "telephone": {
                    "regex": "/^[0-9\-\(\)\ ]+$/",
                    "alertText": "* Invalid phone number"
                },
                "email": {
                    //"regex": "/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
                    "regex": "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$/i",
                    "alertText": "* Invalid email address"
                },
                "date": {
                    "regex": "/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                    "alertText": "* Invalid date, must be in YYYY-MM-DD format"
                },
                "onlyNumber": {
                    "regex": "/^[0-9\ ]+$/",
                    "alertText": "* Numbers only"
                },
                "onlyNumberwithdot": {
                    "regex": "/^[0-9\. ]+$/",
                    "alertText": "* Numbers and Dot only"
                },
                "onlyNumberwithplus": {
                    "regex": "/^[0-9\-\+ ]+$/",
                    "alertText": "* Numbers and + , - only"
                },
                "noSpecialCaracters": {
                    "regex": "/^[0-9a-zA-Z\ -\']+$/",
                    "alertText": "* No special caracters allowed"
                },
                "ajaxUser": {
                    "file": "validateUser.php",
                    "extraData": "name=eric",
                    "alertTextOk": "* This user is available",
                    "alertTextLoad": "* Loading, please wait",
                    "alertText": "* This user is already taken"
                },
                "ajaxName": {
                    "file": "validateUser.php",
                    "alertText": "* This name is already taken",
                    "alertTextOk": "* This name is available",
                    "alertTextLoad": "* Loading, please wait"
                },
                "onlyLetter": {
                    "regex": "/^[a-zA-Z\ \']+$/",
                    "alertText": "* Letters only"
                },
                "url": {
                    "regex": "/^([(http:|https ]+[(/)]{2})+([(w)]{3})+[(.)]+([a-zA-Z0-9\-\])+[(.)]+([a-zA-Z]{2,4})+(.*)$/",
                    "alertText": "* Please enter valid URL including http://"
                },
                "validate2fields": {
                    "nname": "validate2fields",
                    "alertText": "* You must have a firstname and a lastname"
                },
                "validate2fields1": {
                    "nname": "validate2fields1",
                    "alertText": "* This field is required"
                },
                "pngOnly": {
                    //"regex":"/^([a-zA-Z0-9_-])+(.flv)$/",
                    "regex": "/png$/",
                    "alertText": "* Upload only png file"
                },
                "fileUpload": {
                    "file": "validatefile.php",
                    "alertText": "* This field is required"
                }
            }

        }
    }
})(jQuery);

$(document).ready(function () {
    $.validationEngineLanguage.newLang()
});

$(document).ready(function () {
    $("#" + pageForm).validationEngine()
});
