const BASE_URL = '/admin/doctor';

function deleteDoctor(id) {
    if (confirm("Are you sure want to delete this record?")) {
        $.ajax({
            method: "DELETE",
            url: BASE_URL + '/' + id,
            data: {"_token": csrf_token,}
        })
            .done(function (response) {
                handleAjaxResponse(response.status);
            });
    }
}

function updateStatus(id, status) {
    $.ajax({
        method: "PUT",
        url: BASE_URL + "/update-status/" + id,
        data: {"_token": csrf_token, status: status}
    })
        .done(function (response) {
            handleAjaxResponse(response.status);
        });
}

function updateStrOrder(id, event) {
    if (event.keyCode == 13) {
        $.ajax({
            method: "PUT",
            url: BASE_URL + "/update-order/" + id,
            data: {"_token": csrf_token, strOrder: event.srcElement.value}
        })
            .done(function (response) {
                handleAjaxResponse(response.status);
            });
    }

}

function handleAjaxResponse(status) {
    if (status == 200) {
        window.location.href = "/admin/doctor/";
    }
}
