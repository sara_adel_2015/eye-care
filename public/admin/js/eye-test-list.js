const BASE_URL = '/admin/eye-test';

function deleteEyeTest(id) {
    if (confirm("Are you sure want to delete this record?")) {
        $.ajax({
            method: "DELETE",
            url: BASE_URL + '/' + id,
            data: {"_token": csrf_token,}
        })
            .done(function (response) {
                handleAjaxResponse(response.status);
            });
    }
}

