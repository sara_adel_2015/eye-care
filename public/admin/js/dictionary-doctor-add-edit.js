const BASE_URL = '/admin/dictionary-doctor';

function deletePhoto(id) {
    if (confirm("Are you sure want to delete this record?")) {
        $.ajax({
            method: "DELETE",
            url: BASE_URL + '/delete-image/' + id,
            data: {"_token": csrf_token,}
        })
            .done(function (response) {
                if (response.status == 200) {
                    window.location.href = BASE_URL + "/edit/" + id;
                }
            });
    }
}

function checkAddress() {
    let address = $('#' + pageForm).find('#address').val();
    if (!address) {
        $('#' + pageForm).find('#address').trigger('focusout');
    }
    let city = $('#' + pageForm).find('#city').val();
    if (!city) {
        $('#' + pageForm).find('#city').trigger('focusout');
    }
    let state = $('#' + pageForm).find('#state').val();
    if (!state) {
        $('#' + pageForm).find('#state').trigger('focusout');
    }
    let zip = $('#' + pageForm).find('#zip').val();
    if (!zip) {
        $('#' + pageForm).find('#zip').trigger('focusout');
    }
    if (address && city && state && zip) {
        let fullAddress = address + '|' + city + '|' + state + '|' + zip;
        $.ajax({
            method: "GET",
            url: BASE_URL + '/check-address?address=' + fullAddress,
            data: {"_token": csrf_token,}
        })
            .done(function (response) {
                if (response.status == 200) {
                    $('#' + pageForm).find('#latitude').val(response.data.results.lat);
                    $('#' + pageForm).find('#longitude').val(response.data.results.lng);
                    $('#' + pageForm).find('#divMsg').html("");
                } else {
                    $('#' + pageForm).find('#divMsg').html("<font color='#FF00000'>" + response.errors[0] + "</font>");
                }
            }).fail(function (response) {
            $('#' + pageForm).find('#divMsg').html("<font color='#FF00000'>" + response.responseJSON.errors.join(',') + "</font>");
        });

    }
}
