const BASE_URL = '/admin/patient';

function deletePatient(id) {
    if (confirm("Are you sure want to delete this record?")) {
        $.ajax({
            method: "DELETE",
            url: BASE_URL + '/' + id,
            data: {"_token": csrf_token,}
        })
            .done(function (response) {
                handleAjaxResponse(response.status);
            });
    }
}

function updateStatus(id, status) {
    $.ajax({
        method: "PUT",
        url: BASE_URL + "/update-status/" + id,
        data: {"_token": csrf_token, status: status}
    })
        .done(function (response) {
            handleAjaxResponse(response.status);
        });
}
