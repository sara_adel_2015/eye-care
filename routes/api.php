<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1'], function () use ($router) {
    //payment hooks
    Route::post('/payment/hooks', function (\Illuminate\Http\Request $request) {
        Log::info(json_encode($request->all()));
        $handler = new \App\Services\PaymentWebhooksHandler($request->type);
        return $handler->execute($request);
    });

    Route::group(['prefix' => '/doctors'], function () use ($router) {
        $router->get('/list', 'Api\DictionaryDoctorController@listAll');
    });

    Route::group(['prefix' => '/subscribed-doctors'], function () use ($router) {
        $router->get('/list', 'Api\DoctorController@listAll');
    });

    Route::group(['prefix' => '/patient'], function () use ($router) {
        $router->post('/register', 'Api\PatientRegisterController@register');
        $router->post('/login', ['as' => 'login', 'uses' => 'Api\PatientLoginController@login']);
        $router->put('/change-password', ['as' => 'changePassword', 'uses' => 'Api\PatientChangePasswordController@execute']);
        $router->put('/reset-password', ['as' => 'resetPassword', 'uses' => 'Api\PatientResetPasswordController@execute']);
        $router->put('/update', ['as' => 'patient-update', 'uses' => 'Api\PatientUpdateController@execute']);
        $router->post('/upload-eye-test', ['as' => 'upload-eye-test', 'uses' => 'Api\PatientUploadTestController@execute']);
        $router->get('/', ['as' => 'get-patient-details', 'uses' => 'Api\PatientController@getDetails']);
    });
    Route::group(['prefix' => '/doctor'], function () use ($router) {
        $router->post('/register', 'Api\DoctorRegisterController@register');
    });
    $router->get('/settings', 'Api\SettingsController@getSettings');
});

