<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * Patient Admin Home
 */
Route::get('/', function () {
    return view('b2c/home');
});

Route::get('/specialist-info', function () {
    return view('b2c/specialist-info');
});

$router->post('/contact-us', ['as' => 'contact-us', 'uses' => 'B2c\AdminController@contactUs']);

$router->get('/learn', ['as' => 'cms-learn', 'uses' => 'B2c\AdminController@getLearnContent']);

$router->get('/privacy', ['as' => 'cms-privacy', 'uses' => 'B2c\AdminController@getPrivacyContent']);

$router->get('/terms', ['as' => 'cms-terms', 'uses' => 'B2c\AdminController@getTermsContent']);

$router->get('/doctor-terms', ['as' => 'cms-doctor-terms', 'uses' => 'B2c\AdminController@getDoctorTermsContent']);

$router->get('/home/doctors', ['as' => 'home-doctor-list', 'uses' => 'B2c\AdminController@getDoctors']);


Route::group(['prefix' => 'admin'], function () use ($router) {
    require_once(__DIR__ . '/admin/home.php');
    require_once(__DIR__ . '/admin/doctor.php');
    require_once(__DIR__ . '/admin/patient.php');
    require_once(__DIR__ . '/admin/settings.php');
    require_once(__DIR__ . '/admin/eye-test.php');
    require_once(__DIR__ . '/admin/dictionary-doctor.php');
});
/**
 * Patient B2c Home
 */
Route::group(['prefix' => 'patient'], function () use ($router) {
    require_once(__DIR__ . '/b2c/patient.php');
    require_once(__DIR__ . '/b2c/eye-test.php');
});
/**
 * Doctor B2c Home
 */
Route::group(['prefix' => 'doctor'], function () use ($router) {
    require_once(__DIR__ . '/b2c/doctor.php');
});

