<?php
Route::group(['prefix' => 'eye-test'], function () use ($router) {
    //patient
    $router->get('/', array('as' => 'patient-eye-test-list', 'uses' => 'B2c\EyeTestController@listAll'))->middleware('auth:patient');
    $router->get('/download/{id}', array('as' => 'patient-eye-test-download', 'uses' => 'B2c\EyeTestController@download'))->middleware('auth:patient');
    $router->delete('/{id}', 'B2c\EyeTestController@delete')->middleware('auth:patient');


});

