<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router->get('/login', ['as' => 'patient-login-form', 'uses' => 'B2c\Patient\PatientLoginController@showLoginForm']);
$router->post('/login', ['as' => 'patient-login', 'uses' => 'B2c\Patient\PatientLoginController@login']);
$router->get('/edit-profile', ['as' => 'patient-edit-profile-form', 'uses' => 'B2c\Patient\PatientController@edit'])->middleware('auth:patient');
$router->put('/update-profile', ['as' => 'patient-update-profile', 'uses' => 'B2c\Patient\PatientController@update'])->middleware('auth:patient');
$router->get('/logout', ['as' => 'patient-logout', 'uses' => 'B2c\Patient\PatientController@logout'])->middleware('auth:patient');
$router->get('/change-password', ['as' => 'patient-change-password-form', 'uses' => 'B2c\Patient\PatientChangePasswordController@showChangePasswordForm'])->middleware('auth:patient');
$router->put('/change-password', ['as' => 'patient-change-password', 'uses' => 'B2c\Patient\PatientChangePasswordController@changePassword'])->middleware('auth:patient');
$router->put('/reset-password', ['as' => 'patient-reset-password', 'uses' => 'B2c\Patient\PatientResetPasswordController@execute']);
$router->get('/reset-password', ['as' => 'patient-reset-password-form', 'uses' => 'B2c\Patient\PatientResetPasswordController@getResetPasswordForm']);

$router->get('/', function () {
    return view('b2c/index');
})->middleware('auth:patient');

