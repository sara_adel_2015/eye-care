<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// register
$router->get('/register', ['as' => 'doctor-register-form', 'uses' => 'B2c\Doctor\DoctorRegisterController@showRegisterForm']);
$router->post('/register', ['as' => 'doctor-register', 'uses' => 'B2c\Doctor\DoctorRegisterController@register']);
// login
$router->get('/login', ['as' => 'doctor-login-form', 'uses' => 'B2c\Doctor\DoctorLoginController@showLoginForm']);
$router->post('/login', ['as' => 'doctor-login', 'uses' => 'B2c\Doctor\DoctorLoginController@login']);
// edit profile
$router->get('/edit-profile', ['as' => 'doctor-edit-profile-form', 'uses' => 'B2c\Doctor\DoctorController@edit'])->middleware('auth-doctor:doctor');
$router->put('/update-profile', ['as' => 'doctor-update-profile', 'uses' => 'B2c\Doctor\DoctorController@update'])->middleware('auth-doctor:doctor');
$router->get('/logout', ['as' => 'doctor-logout', 'uses' => 'B2c\Doctor\DoctorController@logout'])->middleware('auth-doctor:doctor');
//change password
$router->get('/change-password', ['as' => 'doctor-change-password-form', 'uses' => 'B2c\Doctor\DoctorController@showChangePasswordForm'])->middleware('auth-doctor:doctor');
$router->put('/change-password', ['as' => 'doctor-change-password', 'uses' => 'B2c\Doctor\DoctorController@changePassword'])->middleware('auth-doctor:doctor');
//doctor
$router->get('/eye-test', array('as' => 'doctor-eye-test-list', 'uses' => 'B2c\EyeTestController@listAll'))->middleware('auth-doctor:doctor');
$router->get('/patient', array('as' => 'doctor-patients-list', 'uses' => 'B2c\Doctor\DoctorController@getPatients'))->middleware('auth-doctor:doctor');

$router->put('/reset-password', ['as' => 'doctor-reset-password', 'uses' => 'B2c\Doctor\DoctorResetPasswordController@execute']);
$router->get('/reset-password', ['as' => 'doctor-reset-password-form', 'uses' => 'B2c\Doctor\DoctorResetPasswordController@getResetPasswordForm']);

//payment cards
Route::group(['prefix' => 'payment-cards'], function () use ($router) {
    $router->get('/', ['as' => 'doctor-payment-cards', 'uses' => 'B2c\Doctor\DoctorPaymentCardsController@getPaymentCards'])->middleware('auth-doctor:doctor');
    $router->put('/update-current-payment', ['as' => 'doctor-update-current-payment', 'uses' => 'B2c\Doctor\DoctorController@updateDefaultPaymentCard'])->middleware('auth-doctor:doctor');
    $router->delete('/{id}', ['as' => 'doctor-delete-payment-card', 'uses' => 'B2c\Doctor\DoctorPaymentCardsController@deletePaymentCard'])->middleware('auth-doctor:doctor');
    $router->get('/add', ['as' => 'doctor-add-payment-card', 'uses' => 'B2c\Doctor\DoctorPaymentCardsController@showAddNewPaymentCardForm'])->middleware('auth-doctor:doctor');
    $router->post('/create', ['as' => 'doctor-save-payment-card', 'uses' => 'B2c\Doctor\DoctorPaymentCardsController@saveNewPaymentCard'])->middleware('auth-doctor:doctor');
    $router->get('/history/{id}', ['as' => 'doctor-payment-card-history', 'uses' => 'B2c\Doctor\DoctorPaymentCardsController@getPaymentHistory'])->middleware('auth-doctor:doctor');
    $router->get('/request-cancel-subscription', ['as' => 'request-cancel-subscription', 'uses' => 'B2c\Doctor\DoctorPaymentCardsController@requestCancelSubscription'])->middleware('auth-doctor:doctor');

});

$router->get('/', function () {
    //go to doctor name
    return view('b2c/doctor/index');
})->middleware('auth-doctor:doctor');

