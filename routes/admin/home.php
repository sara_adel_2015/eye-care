<?php
// login
$router->get('/login', ['as' => 'admin-login-form', 'uses' => 'Admin\AdminLoginController@showLoginForm']);
$router->post('/login', ['as' => 'admin-login', 'uses' => 'Admin\AdminLoginController@login']);

$router->get('/logout', ['as' => 'admin-logout', 'uses' => 'Admin\AdminController@logout'])->middleware('auth-admin:admin');
//change password
$router->get('/change-password', ['as' => 'admin-change-password-form', 'uses' => 'Admin\AdminController@showChangePasswordForm'])->middleware('auth-admin:admin');
$router->put('/change-password', ['as' => 'admin-change-password', 'uses' => 'Admin\AdminController@changePassword'])->middleware('auth-admin:admin');
//home
$router->get('/home', function () {
    return view('admin/home');
})->middleware('auth-admin:admin');