<?php
Route::group(['prefix' => 'settings', 'middleware' => 'auth-admin:admin'], function () use ($router) {
    $router->get('/', array('as' => 'admin-settings', 'uses' => 'Admin\AdminController@viewSettings'));
    $router->post('/', array('as' => 'settings-create', 'uses' => 'Admin\AdminController@createSettings'));
    $router->put('/update/{id}', array('as' => 'settings-update', 'uses' => 'Admin\AdminController@updateSettings'));
    $router->get('/subscription-settings', array('as' => 'view-subscription-settings', 'uses' => 'Admin\AdminController@viewSubscriptionSettings'));
    $router->put('/subscription-settings-update/{id}', array('as' => 'subscription-settings-update', 'uses' => 'Admin\AdminController@updateSubscriptionSettings'));
});

