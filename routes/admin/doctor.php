<?php
Route::group(['prefix' => 'doctor', 'middleware' => 'auth-admin:admin'], function () use ($router) {
    $router->get('/', array('as' => 'doctor-list', 'uses' => 'Admin\DoctorController@listAll'));
    $router->get('/check-address', array('as' => 'doctor-check-address', 'uses' => 'Admin\DoctorController@checkAddress'));
    $router->get('/add', array('as' => 'doctor-add', 'uses' => 'Admin\DoctorController@add'));
    $router->post('/', array('as' => 'doctor-create', 'uses' => 'Admin\DoctorController@create'));
    $router->put('/update-status/{id}', 'Admin\DoctorController@updateStatus');
    $router->put('/update-order/{id}', 'Admin\DoctorController@updateOrder');
    $router->delete('/{id}', 'Admin\DoctorController@delete');
    $router->get('edit/{id}', array('as' => 'doctor-edit', 'uses' => 'Admin\DoctorController@edit'));
    $router->put('/update/{id}', array('as' => 'doctor-update', 'uses' => 'Admin\DoctorController@update'));
    $router->delete('/delete-image/{id}', 'Admin\DoctorController@deleteImage');
    $router->put('/update-orders/', 'Admin\DoctorController@updateOrders');
    $router->get('/upload-sheet', array('as' => 'doctor-upload-sheet', 'uses' => 'Admin\DoctorController@uploadSheet'));
    $router->post('/import', ['as' => 'doctor-import-sheet', 'uses' => 'Admin\DoctorController@import']);
    $router->get('/autocomplete', array('as' => 'doctor-autocomplete', 'uses' => 'Admin\DoctorController@getDoctorsForAutocomplete'));
    $router->get('/payment/history', array('as' => 'doctor-payment-history', 'uses' => 'Admin\DoctorController@getAdminPaymentRecurringHistory'));
    $router->get('/cancel-subscription-requests', array('as' => 'cancel-subscription-requests', 'uses' => 'Admin\DoctorController@getCancelSubscriptionRequests'));
    $router->post('/confirm-cancel-subscription/{id}', array('as' => 'confirm-cancel-subscription', 'uses' => 'Admin\DoctorController@confirmCancelSubscription'));

});

