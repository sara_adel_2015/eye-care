<?php
Route::group(['prefix' => 'patient', 'middleware' => 'auth-admin:admin'], function () use ($router) {
    $router->get('/', array('as' => 'patient-list', 'uses' => 'Admin\PatientController@listAll'));
    $router->get('/autocomplete', array('as' => 'patient-autocomplete', 'uses' => 'Admin\PatientController@getPatientsForAutocomplete'));
    $router->get('/add', array('as' => 'patient-add', 'uses' => 'Admin\PatientController@add'));
    $router->post('/', array('as' => 'patient-create', 'uses' => 'Admin\PatientController@create'));
    $router->put('/update-status/{id}', 'Admin\PatientController@updateStatus');
    $router->delete('/{id}', 'Admin\PatientController@delete');
    $router->get('edit/{id}', array('as' => 'patient-edit', 'uses' => 'Admin\PatientController@edit'));
    $router->put('/update/{id}', array('as' => 'patient-update', 'uses' => 'Admin\PatientController@update'));
});

