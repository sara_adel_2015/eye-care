<?php
Route::group(['prefix' => 'dictionary-doctor', 'middleware' => 'auth-admin:admin'], function () use ($router) {
    $router->get('/', array('as' => 'dictionary-doctor-list', 'uses' => 'Admin\DictionaryDoctorController@listAll'));
    $router->get('/check-address', array('as' => 'dictionary-doctor-check-address', 'uses' => 'Admin\DictionaryDoctorController@checkAddress'));
    $router->get('/add', array('as' => 'dictionary-doctor-add', 'uses' => 'Admin\DictionaryDoctorController@add'));
    $router->post('/', array('as' => 'dictionary-doctor-create', 'uses' => 'Admin\DictionaryDoctorController@create'));
    $router->put('/update-status/{id}', 'Admin\DictionaryDoctorController@updateStatus');
    $router->put('/update-order/{id}', 'Admin\DictionaryDoctorController@updateOrder');
    $router->delete('/{id}', 'Admin\DictionaryDoctorController@delete');
    $router->get('edit/{id}', array('as' => 'dictionary-doctor-edit', 'uses' => 'Admin\DictionaryDoctorController@edit'));
    $router->put('/update/{id}', array('as' => 'dictionary-doctor-update', 'uses' => 'Admin\DictionaryDoctorController@update'));
    $router->delete('/delete-image/{id}', 'Admin\DictionaryDoctorController@deleteImage');
    $router->put('/update-orders/', 'Admin\DictionaryDoctorController@updateOrders');
    $router->get('/upload-sheet', array('as' => 'dictionary-doctor-upload-sheet', 'uses' => 'Admin\DictionaryDoctorController@uploadSheet'));
    $router->post('/import', ['as' => 'dictionary-doctor-import-sheet', 'uses' => 'Admin\DictionaryDoctorController@import']);

});

