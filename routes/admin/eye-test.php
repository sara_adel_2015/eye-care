<?php
Route::group(['prefix' => 'eye-test', 'middleware' => 'auth-admin:admin'], function () use ($router) {
    $router->get('/', array('as' => 'eye-test-list', 'uses' => 'Admin\EyeTestController@listAll'));
    $router->get('/add', array('as' => 'eye-test-add', 'uses' => 'Admin\EyeTestController@add'));
    $router->post('/', array('as' => 'eye-test-create', 'uses' => 'Admin\EyeTestController@create'));
    $router->get('/download/{id}', array('as' => 'eye-test-download', 'uses' => 'Admin\EyeTestController@download'));
    $router->delete('/{id}', 'Admin\EyeTestController@delete');
});

